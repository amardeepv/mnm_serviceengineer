package in.makenmake.generator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class Main {
    public static void main(String[] args) throws Exception {
        Schema schema=new Schema(1,"in.makenmake.serviceengineer.db");

        Entity order=schema.addEntity("Locations");
        order.addIdProperty();
        order.addStringProperty("localLocationId");
        order.addDoubleProperty("longitude");
        order.addDoubleProperty("latitude");
        order.addBooleanProperty("isSynced");
        order.addDateProperty("createdDate");
        order.addContentProvider();



        DaoGenerator daoGenerator=new DaoGenerator();
        daoGenerator.generateAll(schema,"./app/src/main/java");


    }
}
