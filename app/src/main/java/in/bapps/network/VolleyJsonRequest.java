package in.bapps.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.bapps.listener.UpdateJsonListener;
import in.makenmake.serviceengineer.BuildConfig;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * @author bucky
 */
public class VolleyJsonRequest extends JsonObjectRequest {

    public VolleyJsonRequest(int method, String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {

        super(url, new JSONObject(requestJson), updateListener, updateListener);
    }

    public static VolleyJsonRequest doPost(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
            Log.i("Request Jso-->", requestJson);
        }
        return new VolleyJsonRequest(Method.POST, url, updateListener, requestJson) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                // add headers <key,value>

                String auth = "bearer "
                        + EngineerPreference.getInstance().getAuthToken();
                headers.put("Authorization", auth);
                return headers;
            }

            /*@Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                //return super.parseNetworkResponse(response);
                int mStatusCode = response.statusCode;
                try {
                    Log.d(TAG, "[raw json]: " + (new String(response.data)));
                    Gson gson = new Gson();
                    String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                  *//*  Type type=new TypeToken<CommonJsonResponseNew<String>>(){}.getType();
                    return Response.success(gson.fromJson(json, type),
                            HttpHeaderParser.parseCacheHeaders(response));*//*

                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                    return Response.success(new JSONObject(jsonString),
                            HttpHeaderParser.parseCacheHeaders(response));

                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JsonSyntaxException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException e) {
                    e.printStackTrace();
                    return Response.error(new ParseError(e));
                }

            }
            */

        };
    }

    public static VolleyJsonRequest doget(String url, UpdateJsonListener updateListener) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.GET, url, updateListener, null){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                // add headers <key,value>

                String auth = "bearer "
                        + EngineerPreference.getInstance().getAuthToken();
                headers.put("Authorization", auth);
                return headers;
            }
        };
    }


}