package in.bapps.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import in.bapps.listener.UpdateListener;
import in.makenmake.serviceengineer.BuildConfig;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * @author bucky
 */
public class VolleyStringRequest extends StringRequest {

    private Map<String, String> mRequestparams;
    public static final String mNetworkTag = "Network";

    private VolleyStringRequest(int method, String url, UpdateListener updateListener, Map<String, String> params) {
        super(method, url, updateListener, updateListener);
        mRequestparams = params;
    }

    public static VolleyStringRequest doPost(String url, UpdateListener updateListener, Map<String, String> params) {
        if (BuildConfig.DEBUG) {
            Log.i(mNetworkTag, url);
            Log.i(mNetworkTag, params.toString());
        }
        return new VolleyStringRequest(Method.POST, url, updateListener, params) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                // add headers <key,value>

                String auth = "bearer "
                        + EngineerPreference.getInstance().getAuthToken();
                headers.put("Authorization", auth);
                return headers;
            }
        };
    }

    public static VolleyStringRequest doGet(String url, UpdateListener updateListener) {
        return new VolleyStringRequest(Method.GET, url, updateListener, null){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                // add headers <key,value>

                String auth = "bearer "
                        + EngineerPreference.getInstance().getAuthToken();
                headers.put("Authorization", auth);
                return headers;
            }
        };
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mRequestparams;
    }

}
