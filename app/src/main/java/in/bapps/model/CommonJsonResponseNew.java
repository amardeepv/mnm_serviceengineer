package in.bapps.model;

import java.util.List;

/**
 * Created by Bucky on 22/03/2017.
 */

public class CommonJsonResponseNew<T> extends CommonJsonResponse {
    public boolean hasError;
    public String messageCode;
    public String message;
    public List<String> messages;
    public T data;
}
