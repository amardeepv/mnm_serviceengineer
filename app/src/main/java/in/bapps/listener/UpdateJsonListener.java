package in.bapps.listener;


import android.app.Activity;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONObject;

import in.makenmake.serviceengineer.BuildConfig;


/**
 * @author bucky
 */
public class UpdateJsonListener implements ErrorListener, Listener<JSONObject> {

    private int reqType;
    private onUpdateViewJsonListener onUpdateViewJsonListener;
    private Activity mActivity;

    public interface onUpdateViewJsonListener {
        public void updateView(String responseString, boolean isSuccess, int reqType);
    }

    public UpdateJsonListener(Activity activity, onUpdateViewJsonListener onUpdateView, int reqType) {
        this.reqType = reqType;
        this.onUpdateViewJsonListener = onUpdateView;
        mActivity = activity;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String data="";
        try {
             data=new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers, "utf8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        onUpdateViewJsonListener.updateView(data, false, reqType);
    }

    @Override
    public void onResponse(JSONObject jsonResponse) {
        String responseStr = jsonResponse.toString();
        if (BuildConfig.DEBUG) {
            Log.i("Respone-------------->", responseStr);
        }
        onUpdateViewJsonListener.updateView(responseStr, true, reqType);
    }

}
