package in.bapps.listener;

public interface RightTextClickListner {
	public void onShareButtonClick();
	public void onDoneButtonClick();
}
