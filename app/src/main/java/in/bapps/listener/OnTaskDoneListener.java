package in.bapps.listener;

public interface OnTaskDoneListener {
	void onTaskDone();
}
