package in.bapps.listener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import in.bapps.model.CommonJsonResponse;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyStringRequest;
import in.makenmake.serviceengineer.BuildConfig;


public class UpdateListener<T> implements Listener<String>, ErrorListener {
    private int reqType;
    private onUpdateViewListener onUpdateViewListener;
    private Activity mActivity;
    private Class<T> classObject;

    public interface onUpdateViewListener {
        public void updateView(Object responseObject, boolean isSuccess, int reqType);
    }

    public UpdateListener(Activity activity, onUpdateViewListener onUpdateView, int reqType, Class<T> classObject) {
        this.reqType = reqType;
        this.onUpdateViewListener = onUpdateView;
        mActivity = activity;
        this.classObject = classObject;
    }

    @Override
    public void onErrorResponse(VolleyError error) { String data="";
        try {
            data=new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            final String finalData = data;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Object responseObject = new Gson().fromJson(finalData, classObject);
                    mActivity.runOnUiThread(new Runnable() {
                        @SuppressLint("InlinedApi")
                        @Override
                        public void run() {
                            if (responseObject instanceof CommonJsonResponseNew) {
                                CommonJsonResponseNew<String> responseModel = (CommonJsonResponseNew) responseObject;
                            }
                            onUpdateViewListener.updateView(responseObject, false, reqType);
                        }
                    });
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
            onUpdateViewListener.updateView(error, false, reqType);
        }

    }

    @Override
    public void onResponse(final String responseStr) {
        if (BuildConfig.DEBUG) {
            Log.i(VolleyStringRequest.mNetworkTag, responseStr);
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Object responseObject = new Gson().fromJson(responseStr, classObject);
                    mActivity.runOnUiThread(new Runnable() {
                        @SuppressLint("InlinedApi")
                        @Override
                        public void run() {
                            if (responseObject instanceof CommonJsonResponse) {
                                CommonJsonResponse responseModel = (CommonJsonResponse) responseObject;
                            }
                            onUpdateViewListener.updateView(responseObject, true, reqType);
                        }
                    });
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
            onUpdateViewListener.updateView(responseStr, false, reqType);
        }
    }

}
