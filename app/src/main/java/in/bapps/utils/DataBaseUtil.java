package in.bapps.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

public class DataBaseUtil {
	public static void exportDB(Context context) {

		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if (sd.canWrite()) {
				String currentDBPath = "//data//" + "com.nightadvisor" + "//databases//" + "night_advisor_db";
				String backupDBPath = "/BackupFolder/night_advisor_db";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, backupDBPath);
//				if (!backupDB.exists()) {
//					backupDB.mkdirs();
//				}
				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
				Toast.makeText(context, backupDB.toString(), Toast.LENGTH_LONG).show();

			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();

		}
	}
}
