package in.bapps.utils;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtils {

    public static String getRelativeDateTimeDiff(String taskDate) {
        if (taskDate == null) {
            return "";
        }
        long commentDateInMillis = DateUtils.getDateInMillis(taskDate, "yyyy-MM-dd HH:mm:ss");

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        long currentDateInMillis = DateUtils.getDateInMillis(sdf.format(new Date()), "yyyy-MM-dd HH:mm:ss");

        return TimeUtils.millisToLongDHMS(currentDateInMillis - commentDateInMillis);
    }

    public static String getRelativeDateTimeDiff(long dateInMilis) {
        if (dateInMilis == 0) {
            return "";
        }
        long commentDateInMillis = dateInMilis;

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        long currentDateInMillis = DateUtils.getDateInMillis(sdf.format(new Date()), "yyyy-MM-dd HH:mm:ss");

        return TimeUtils.millisToLongDHMS(currentDateInMillis - commentDateInMillis);
    }

    public static String getNextDate(String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static long getDateInMillis(String curDate, String dateFormat) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            return calendar.getTimeInMillis();
        } catch (Exception e) {
            e.printStackTrace();
            return System.currentTimeMillis();
        }
    }

    public static String getWeekDay(String dateFormat, String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
            Date date = sdf.parse(curDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
            String[] weekArray = {"Sun", "Mon", "Tue", "Wed", "Thur", "Fri",
                    "Sat"};
            return weekArray[day_of_week - 1];
        } catch (ParseException e) {
            e.printStackTrace();
            return "Sun";
        }
    }

    public static long getDateDiff(String date1, String date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date dateFirst = null;
        Date dateSecond = null;
        try {
            dateFirst = sdf.parse(date1);
            dateSecond = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TimeUnit timeUnit = TimeUnit.DAYS;
        long diffInMillies = dateSecond.getTime() - dateFirst.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static long getDateDiffInDays(String date1, String date2,String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        Date dateFirst = null;
        Date dateSecond = null;
        try {
            dateFirst = sdf.parse(date1);
            dateSecond = sdf.parse(date2);

             long diff=dateSecond.getTime()-dateFirst.getTime();
             long x=TimeUnit.DAYS.convert(diff,TimeUnit.MILLISECONDS);
            return x;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -99;

    }

    public static String getInitialDateofSelectedMonth(String selectedDate) {
        try {
            String firstDate = "01-" + selectedDate.split("-")[1] + "-" + selectedDate.split("-")[2];
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            Date date = sdf.parse(selectedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getNextMonthSelectedDate(String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(curDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, 1);
            return sdf.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int getDaysInMonth(String selectedDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = sdf.parse(selectedDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            // Get the number of days in that month
            int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            return daysInMonth;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 30;
        }
    }

    /**
     * ex: convert date from "dd-MM-yyyy" to MMM dd
     *
     * @param currentFormat
     * @param requiredFormat
     * @param curDate
     * @return
     */
    public static String getFormattedDate(String currentFormat, String requiredFormat, String curDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(currentFormat, Locale.US);
            SimpleDateFormat requiredSdf = new SimpleDateFormat(requiredFormat, Locale.US);
            Date date = sdf.parse(curDate);
            return requiredSdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return curDate;
        }
    }

    public static String getFormattedDate(Date dateTime,String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getFormattedDateFromMillis(String format, long millisTime) {
        try {
            Date date = new Date(millisTime);
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return millisTime + "";
        }
    }

    public static String getTimeFromMillis(long millisTime) {
        try {
            String timeFormatterEn = "hh:mm a";
            String timeFormatterDE = "HH:mm";
            String timeFormatter;
            String a = Locale.getDefault().getDisplayLanguage();
            if (a.equalsIgnoreCase("english")) {
                timeFormatter = timeFormatterEn;
            } else {
                timeFormatter = timeFormatterDE;
            }
            Date date = new Date(millisTime);
            SimpleDateFormat sdf = new SimpleDateFormat(timeFormatter, Locale.getDefault());
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return millisTime + "";
        }
    }

    private static int getDifferenceStartEndDate(String dateTimeOne, String dateTimeTwo) {
        try {

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
            Date d1 = f.parse(dateTimeOne);
            Date d2 = f.parse(dateTimeTwo);

            return d1.compareTo(d2);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return 0;
    }

    public static int getDifferenceStartEndDate(String dateTimeOne, String dateTimeTwo,String format) {
        try {

            SimpleDateFormat f = new SimpleDateFormat(format,Locale.US);
            Date d1 = f.parse(dateTimeOne);
            Date d2 = f.parse(dateTimeTwo);

            return d1.compareTo(d2);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return 0;
    }

    public static Date getDate(String dateTime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss");
        Date d1 = null;
        try {
            d1 = f.parse(dateTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return d1;
    }

    public static int getYear(String longDate) {
        if (longDate != null) {
            try {
                Date date = new Date(Long.parseLong(longDate));
                DateTime abstractDateTime = new DateTime(date);
                return abstractDateTime.getYear();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 0;
    }


    private static String getDay(String i) {
        switch (Integer.parseInt(i)) {
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thurday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
            case 7:
                return "Sunday";
            default:
                return "";
        }
    }

    public static String getMonth(String longDate) {
        if (longDate != null) {
            try {
                Date date = new Date(Long.parseLong(longDate));
                DateTime dateTime = new DateTime(date);
                int month = dateTime.getMonthOfYear();
                switch (month) {
                    case 0:
                        return "Jan";
                    case 1:
                        return "Feb";
                    case 2:
                        return "Mar";
                    case 3:
                        return "Apr";
                    case 4:
                        return "May";
                    case 5:
                        return "Jun";
                    case 6:
                        return "Jul";
                    case 7:
                        return "Aug";
                    case 8:
                        return "Sep";
                    case 9:
                        return "Oct";
                    case 10:
                        return "Nov";
                    case 11:
                        return "Dec";
                    default:
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        return "";
    }

	/*
     * private static int compareDateFromCurrent(Date date){ try { DateFormat
	 * dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); Date
	 * currentDate = new Date(); } catch (Exception e) { // TODO: handle
	 * exception }
	 * 
	 * 
	 * }
	 */
}
