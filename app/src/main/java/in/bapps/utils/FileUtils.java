package in.bapps.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Bucky on 02/02/2017.
 */

public class FileUtils {

    private static final String TAG = "FileUtils";

    public static File getAlbumStorageDir() {

       // if(isExternalStorageWritable())

        // Get the directory for the user's public pictures directory.
       /* File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);*/

        File root = Environment.getExternalStorageDirectory();

        File logDir = new File(root.getAbsolutePath() + "/" + "MnMEngineerAppFiles");
        if (!logDir.isDirectory()) {
            logDir.mkdir();
        }


      /*  if (!logDir.mkdirs()) {
            Log.e(TAG, "Directory not created");
        }*/
        return logDir;
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

}
