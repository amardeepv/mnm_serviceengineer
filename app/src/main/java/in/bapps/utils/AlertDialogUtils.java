package in.bapps.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AlertDialogUtils {

	public static final int	BUTTON_CLICK_FAILURE	= 0;
	public static final int	BUTTON_CLICK_SUCCESS	= 1;

	public static void showAlertDialogWithTwoButtons(Context context, String headerMessage, String messageInfo, String btnPositiveText, String btnNegativeText, final OnButtonClickListener onButtonClick) {
		new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setPositiveButton(btnPositiveText, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
			}
		}).setNegativeButton(btnNegativeText, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onButtonClick.onButtonClick(BUTTON_CLICK_FAILURE);
			}
		}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}

	public static void showAlertDialog(Context context, String headerMessage, String messageInfo, final OnButtonClickListener onButtonClick) {
		new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
			}
		}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}

	public interface OnButtonClickListener {

		void onButtonClick(int buttonId);

	}

}
