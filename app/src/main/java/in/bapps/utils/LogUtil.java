package in.bapps.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Bucky on 15/03/2017.
 */

public class LogUtil {

    static SimpleDateFormat smtForFileName = null;

    /**
     * This method writes the text to log file.
     *
     * @param log_text
     */
    public static synchronized void writeLog(String log_text) {
       /* BufferedWriter bw = null;
        try {
            File logFileName = getLogFile();
            if (!logFileName.exists()) {
                logFileName.createNewFile();
            }
            bw = new BufferedWriter(new FileWriter(logFileName, true));
            bw.append(now());
            bw.append(" : ");
            bw.append(log_text);
            bw.append("\r\n");
            bw.flush();
            bw.close();
            logFileName = null;
        } catch (Exception ex) {
        } finally {
            bw = null;
        }*/
    }

    /**
     * This method returns the current date in dd/MM/yyyy HH:mm:ss.SSS format .
     *
     * @return current date in formatted string
     */
    private static String now() {
        SimpleDateFormat smt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
        return (smt.format(new Date()));
    }

    private static File getLogFile() {
        try {
            if (isExternalStorageWritable()) {
                //  File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);

                /*File root1 = Environment.getExternalStorageDirectory();

                File logDir = new File(root1.getAbsolutePath() + "/" + "MnMEngineerAppFiles");
                if (!logDir.isDirectory()) {
                    logDir.mkdir();
                }*/


                File logDir=FileUtils.getAlbumStorageDir();
                File logFile = new File(logDir, "EngineerApp_LOG.log");
                return logFile;
            }
        } catch (Exception ex) {
        }
        return null;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), albumName);
        if (!file.mkdirs()) {
            Log.e("EngineerApp", "Directory not created");
        }
        return file;
    }
}
