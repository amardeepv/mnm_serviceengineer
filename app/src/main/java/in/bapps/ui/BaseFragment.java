package in.bapps.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.HashMap;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.listener.UpdateListener.onUpdateViewListener;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.utils.CommonMethods;

public abstract class BaseFragment extends Fragment implements OnClickListener,
        onUpdateViewListener {
    public static String HOME_FRAGMENT = "frg_home";
    private String frgamentName;
    public BaseActivity mActivity;
    private float density;

    public BaseFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        density = mActivity.getResources().getDisplayMetrics().density;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHeader();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setHeader();
        }
    }



    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    CommonMethods.hideKeyboard(getActivity());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }


    public abstract void setHeader();

    /*public void replaceChildFragment(Fragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragmentLocal = getChildFragmentManager().findFragmentById(
                R.id.innerFrame);
        if (fragmentLocal != null
                && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        ft.replace(R.id.innerFrame, fragment, tag);

        // fragment.setRetainInstance(true);
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
        ((BaseActivity) mActivity).removeProgressDialog();
    }

    public HashMap<String, String> getBaseParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public float getDensity() {
        return density;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (BaseActivity) activity;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragmentName(String frgamentName) {
        this.frgamentName = frgamentName;
    }

    public String getFragmentName() {
        return frgamentName;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(),
                    getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {

            default:
                url = "";
                className = null;
                break;
        }

        VolleyStringRequest request = VolleyStringRequest.doPost(url,
                new UpdateListener(mActivity, this, reqType, className) {
                }, getParams(reqType));
        ((BaseApplication) getActivity().getApplicationContext())
                .getVolleyManagerInstance().addToRequestQueue(request, url);
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = getBaseParams();


        return params;
    }

    /**
     * Override this method when refresh page is required in your fragment
     *
     * @param bundle
     */
    protected void refreshFragment(Bundle bundle) {

    }

    public void updateView(Object object) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            default:
                break;
        }

        Log.d("click", BaseFragment.class.getSimpleName().toString());

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(),
                        getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
