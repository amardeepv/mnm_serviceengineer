package in.bapps.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.listener.UpdateListener.onUpdateViewListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.LogUtil;
import in.bapps.utils.StringUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.ui.activity.LoginActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

//import com.bugsense.trace.BugSenseHandler;

public class BaseActivity extends AppCompatActivity implements onUpdateViewListener, OnClickListener,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "BaseActivity";

    private static AlertDialog alertDialog;
    private BaseApplication mApplication;
    private long delayTimeForExitToast = 2000;
    private long backPressedTime;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;

    private boolean isWindowFocused;
    private boolean isAppWentToBg;
    private boolean isBackPressed;
    private static int runningActivities = 0;

    private FirebaseAnalytics mFirebaseAnalytics;


    private static final long INTERVAL = 1000 * 60 * 2;
    private static final long FASTEST_INTERVAL = 1000 * 60 * 1;

    private boolean mRequestingLocationUpdates = false;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    public Location mLastLocation;
    String mLastUpdateTime;
    static boolean isGoogleServiceAvilble = false;

    public Location currentLocation = null;

    private static final int PERMISSION_CALLBACK_CONSTANT = 301;
    private static final int REQUEST_PERMISSION_SETTING = 302;
    private boolean sentToSettings = false;


    protected void createLocationRequest() {
        Log.d(TAG, "createLocationRequest() called");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle arg0) {
        Log.d(TAG, "onCreate() called with: arg0 = [" + arg0 + "]");
        super.onCreate(arg0);
        mApplication = (BaseApplication) getApplication();

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (isGooglePlayServicesAvailable()) {
            isGoogleServiceAvilble = true;
            buildGoogleApiClient();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient===");
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        //   createLocationRequest();
    }

    private boolean isGooglePlayServicesAvailable() {
        Log.d(TAG, "isGooglePlayServicesAvailable() called");
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    public String getDeviceId() {
/*        this.mDeviceId = EngineerPreference.getInstance().getDeviceId();
        if (this.mDeviceId == null)
        {
            if (this.gcm == null)
                this.gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        BaseActivity.this.mDeviceId = BaseActivity.this.gcm.register(new String[] { "741963953717" });
                        return;
                    }
                    catch (IOException localIOException)
                    {
                        localIOException.printStackTrace();
                    }
                }
            }).start();
        }
        if (this.mDeviceId == null)
            this.mDeviceId = "";
        return this.mDeviceId;*/

        return "";
    }


    public void logFireBaseSelectContent(String id, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

    }

    public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(R.id.content_frame, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(R.id.content_frame, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }


    public void removeFragmentFromBackStack(Fragment fragment) {

        String tag = fragment.getClass().getSimpleName();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        try {
            trans.remove(fragment);
            trans.commit();
            manager.popBackStack();

        } catch (Exception ex) {
            ex.printStackTrace();
            trans.commitAllowingStateLoss();
        }
    }


    public void showProgressDialog() {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage("Loading");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showProgressDialog(String message) {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(message);
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openLoginActivity() {
        clearSharedPerefrence();
        //  hitApiRequest(ApiConstants.REQUEST_APPLY_LEAVE);  //TODO : logout from server
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void clearSharedPerefrence() {
        EngineerPreference.getInstance().OnUserLogut();
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume() called");
        super.onResume();
        mApplication.activityResumed();
        if (mGoogleApiClient.isConnected()) {
            //  startLocationServices();
            Log.d(TAG, "Location update resumed .....................");
        }
    }


    @Override
    protected void onPause() {
        Log.d(TAG, "onPause() called");
        super.onPause();
        mApplication.activityPaused();
        stopLocationUpdates();
    }

    /**
     * hides the soft key pad
     */
    public void hideSoftKeypad(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(BaseActivity.class.getSimpleName(), "hideSoftKeypad()", e);
        }
    }

    public void loadUrlOnBrowser(String url) {
        try {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception ex) {
            ToastUtils.showToast(this, "Url is not proper!");
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                // ToastUtils.showToast(this,
                // getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {

                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


/*    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    CommonMethods.hideKeyboard(getBaseContext());
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }*/

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {


            default:
                url = "";
                className = null;
                break;
        }

        VolleyStringRequest request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
        }, getParams(reqType));
        ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();


        return params;
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.d(TAG, "Location update started ..............: ");
    }


    private void startLocationServices() {

        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Show Information about why you need the permission
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs access gps location.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CALLBACK_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else if (EngineerPreference.getPermissionCamera()) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs access gps location.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                            Toast.makeText(BaseActivity.this, "Go to Permissions to Grant Location permission", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_CALLBACK_CONSTANT);
                }
                EngineerPreference.setPermissionLocation(true);
            } else {
                PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                        mGoogleApiClient, mLocationRequest, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean getUserLastLocation() {
        if (mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BaseActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CALLBACK_CONSTANT);
                return false;
            }
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                return true;

            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    public void onStop() {
        Log.d(TAG, "onStop fired ..............");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        Log.d(TAG, "isConnected ...............: " + mGoogleApiClient.isConnected());
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        //startLocationServices();
        getUserLastLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called with: i = [" + i + "]");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called with: connectionResult = [" + connectionResult + "]");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]");
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
    }


    protected void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates() called");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            if(mRequestingLocationUpdates){
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }

        Log.d(TAG, "Location update stopped .......................");
    }

    public enum HEADER_MODE {
        MENU_ONLY, MENU_WITH_SEARCH, BACK_WITH_SEARCH, BACK_WITHOUT_SEARCH, BACK_WITH_FORWARD, ONLY_FORWARD, MENU_WITH_SETTING, BOTH_SIDE_TEXT, SEARCH;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        super.onActivityResult(requestCode, resultCode, data);

        ImageView imageView;
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    //Got Permission
                    // startLocationServices();
                }
            }

        }



    }



    public void updateLocationBaseActivity(){
        if(!getUserLastLocation()){
            ToastUtils.showToast(this,"Unable to update user location");
            return;
        }
        try{
            Log.d(TAG, "updateLocationBaseActivity() called");

            String url = ApiConstants.URL_ADD_ENG_LOCATION;

            JsonObjectRequest request = null;

            request = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);
                                LogUtil.writeLog("Server location added :"+mLastLocation.getLatitude()+" Longitude"+mLastLocation.getLongitude());

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    LogUtil.writeLog("VolleyError while updating to server :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());
                    LogUtil.writeLog("VolleyError is :"+error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            ((BaseApplication)getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.writeLog("Exception while updating to server :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());
            LogUtil.writeLog("Exception is :"+e.getMessage());
        }



    }

    private String getJSON() {

        String json = "";
        Gson gson = new Gson();
        Type type;


        EngineerLocation location = new EngineerLocation(
                EngineerPreference.getInstance().getUserId(),
                mLastLocation.getLongitude(),
                mLastLocation.getLatitude()

        );


        type = new TypeToken<EngineerLocation>() {
        }.getType();
        json = gson.toJson(location, type);


        return json;
    }
}
