package in.bapps.application;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;

import com.evernote.android.job.JobManager;

import java.io.File;
import java.io.IOException;

import in.bapps.network.VolleyManager;
import in.bapps.utils.FileUtils;
import in.makenmake.serviceengineer.db.DaoMaster;
import in.makenmake.serviceengineer.db.DaoMaster.DevOpenHelper;
import in.makenmake.serviceengineer.db.DaoSession;
import in.makenmake.serviceengineer.sync.MNMJobCreator;

/**
 * @author bucky
 */
public class BaseApplication extends Application {

    public static Context mContext;
    private boolean activityVisible;
    private String mFriendChatUserName;
    private boolean activityChatVisible;

    /** A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher. */
    public static final boolean ENCRYPTED = false;

    private static DaoSession daoSession;


    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();

        DevOpenHelper helper = new DevOpenHelper(this,"mnm_engineer",null);
        SQLiteDatabase db = helper.getWritableDatabase();
        daoSession = new DaoMaster(db).newSession();

       // saveAppLogCatToFile();

        JobManager.create(this).addJobCreator(new MNMJobCreator());

        JobManager.instance().getConfig().setAllowSmallerIntervalsForMarshmallow(true); // Don't use this in production

/*        startMyService();
        stopRecurringAlarm();*/



    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }



/*    private void startMyService() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, LocationUpdateService.class);
        PendingIntent pi = PendingIntent.getService(this,
                (int) System.currentTimeMillis(), intent, 0);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 34);
        calendar.set(Calendar.SECOND, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pi);
    }*/

/*    private void stopRecurringAlarm() {
        Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        updateTime.set(Calendar.HOUR_OF_DAY, 18);
        updateTime.set(Calendar.MINUTE, 0);
        Intent intent = new Intent(this, MyServiceTerminator.class);
        PendingIntent pintent = PendingIntent.getService(this, 1, intent,    PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.RTC_WAKEUP,updateTime.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pintent);
    }*/


    private void saveAppLogCatToFile() {

        //if ( isExternalStorageWritable() ) {

/*        File appDirectory = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),"EngineerFiles");
        File logDirectory = new File( appDirectory + "/log" );*/
        File mainDirectory = FileUtils.getAlbumStorageDir();
        File logDirectory = new File( mainDirectory + "/log" );
        File logFile = new File( logDirectory, "logcat" + System.currentTimeMillis() + ".txt" );

        // create app folder
       /* if ( !appDirectory.exists() ) {
            appDirectory.mkdir();
        }*/

        // create log folder
        if ( !logDirectory.exists() ) {
            logDirectory.mkdir();
        }

        // clear the previous logcat and then write the new one to the file
        try {
            Process process = Runtime.getRuntime().exec("logcat -c");
            process = Runtime.getRuntime().exec("logcat -f " + logFile);
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        /*}
        else if ( isExternalStorageReadable() ) {
            // only readable
        } else {
            // not accessible
        }*/
    }

    public boolean isActivityVisible() {
        return activityVisible;
    }

    public void activityResumed() {
        activityVisible = true;
    }

    public void activityPaused() {
        activityVisible = false;
    }

    public boolean isActivityChatVisible() {
        return activityChatVisible;
    }

    public void activityChatResumed(String pFriendChatUserName) {
        activityChatVisible = true;
        mFriendChatUserName = pFriendChatUserName;
    }

    public String getFocusedChatUser() {
        return mFriendChatUserName;
    }

    public void activityChatPaused() {
        activityChatVisible = false;
    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }

 /*   public  BaseSqliteOpenHelper getDbHelperInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(context);
        }
        return dbHelper;
    }
*/


/*    public  BaseSqliteOpenHelper getDbHelperInstance() {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(this);
        }
        return dbHelper;
    }*/



    @Override
    public void onTerminate() {

        super.onTerminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
        MultiDex.install(this);
    }
}
