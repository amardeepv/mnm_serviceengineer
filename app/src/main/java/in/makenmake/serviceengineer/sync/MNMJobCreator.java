package in.makenmake.serviceengineer.sync;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by Bucky on 19/06/2017.
 */

public class MNMJobCreator implements JobCreator {
    @Override
    public Job create(String tag) {
        try {
            switch (tag) {
                case LocationSyncJob.TAG:
                    return new LocationSyncJob();
                default:
                    return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
