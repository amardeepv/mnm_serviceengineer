package in.makenmake.serviceengineer.sync;

import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import in.bapps.application.BaseApplication;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.utils.LogUtil;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.db.Locations;
import in.makenmake.serviceengineer.db.LocationsDao;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.utils.EngineerPreference;

import static in.bapps.application.BaseApplication.getDaoSession;

/**
 * Created by Bucky on 19/06/2017.
 */

public class LocationSyncJob extends Job {
    public static final String TAG = "LocationSyncJob";
    private List<Locations> unSyncedLocationsList;

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        Log.d(TAG, "onRunJob() called with: params = [" + params + "]");

        if (getUnSyncedLocation().size() != 0) {
            LogUtil.writeLog(TAG + "No. of Order to sync :" + getUnSyncedLocation().size());
            syncLocationsToServer();
        } else {
            LogUtil.writeLog(TAG + "No Order found to sync");
        }


        return Result.SUCCESS;
    }


    public static void schedulePeriodic() {

        LogUtil.writeLog(TAG + "schedulePeriodic() called");

        try {
            new JobRequest.Builder(LocationSyncJob.TAG)
                    .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                    .setUpdateCurrent(true)
                    .setPersisted(true)
                    .build()
                    .schedule();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "schedulePeriodic: ", e);
        }
    }

    private String getJSON() {

        String json = "";
        Gson gson = new Gson();
        Type type;

        Locations locations = unSyncedLocationsList.get(0);


        EngineerLocation location = new EngineerLocation(
                EngineerPreference.getInstance().getUserId(),
                locations.getLongitude(),
                locations.getLatitude()

        );


        type = new TypeToken<EngineerLocation>() {
        }.getType();
        json = gson.toJson(location, type);


        return json;
    }

    private void syncLocationsToServer() {

        try {
            Log.d(TAG, "syncLocationsToServer() called");

            String url = ApiConstants.URL_ADD_ENG_LOCATION;

            JsonObjectRequest request = null;
            final long currentLocalLocation = unSyncedLocationsList.get(0).getId();

            request = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);
                                processUnSyncedLocationsList(currentLocalLocation);


                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();

                    LogUtil.writeLog("VolleyError is :" + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            ((BaseApplication) BaseApplication.mContext).getVolleyManagerInstance().addToRequestQueue(request, url);

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.writeLog("Exception is :" + e.getMessage());
        }
    }


    private List<Locations> getUnSyncedLocation() {
        unSyncedLocationsList = BaseApplication.getDaoSession().getLocationsDao().queryBuilder().where(LocationsDao.Properties.IsSynced.eq(false)).list();
        return unSyncedLocationsList;
    }

    private void updateLocationTable(long id) {

        LocationsDao locationsDao = getDaoSession().getLocationsDao();

        Locations locations = locationsDao.load(id);

        locations.setIsSynced(true);

        locationsDao.update(locations);
    }

    private void processUnSyncedLocationsList(long id) {
        updateLocationTable(id);
        updateUnSyncedLocationsList();
        if (unSyncedLocationsList.size() > 0) {
            syncLocationsToServer();
        }
    }

    private void updateUnSyncedLocationsList() {
        unSyncedLocationsList.remove(0);
    }


}

