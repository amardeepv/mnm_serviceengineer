package in.makenmake.serviceengineer.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import fr.quentinklein.slt.LocationTracker;
import in.bapps.application.BaseApplication;
import in.bapps.model.CommonJsonResponseNew;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;



/**
 * Created by Bucky on 14/06/2017.
 */

public class EngineerTrackerService extends Service {

    public boolean isRunning = false;
    Context mContext;
    private static final String TAG = "EngineerTrackerService";
    public double latitude;
    public double longitude;
    private LocationTracker tracker;
    private Runnable runnable;
    private Handler myHandler;

    Location currentBestLocation;
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        Log.i(TAG, "onCreate: " + "location service started");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isRunning) {
            Log.i(TAG, "onStartCommand: service is already running");
            return START_STICKY;
        }



        //FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                //Toast.makeText(mContext, "Loca :" + provider.getClass().getName(), Toast.LENGTH_LONG).show();

                Log.i(TAG, "onStatusChanged: location provider status is changed");
            }

            public void onProviderEnabled(String provider) {
                Toast.makeText(mContext, "Location provider us enabled :" + provider.getClass().getName(), Toast.LENGTH_LONG).show();
            }

            public void onProviderDisabled(String provider) {
                Toast.makeText(mContext, "Location provider is disabled :" + provider.getClass().getName(), Toast.LENGTH_LONG).show();
            }
        };


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return START_STICKY;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void makeUseOfNewLocation(Location location) {
        //Toast.makeText(this,location.getLatitude()+" and "+location.getLongitude(),Toast.LENGTH_LONG).show();

        Log.i(TAG, "makeUseOfNewLocation: "+location.getLatitude()+" and "+location.getLongitude());

        latitude=location.getLatitude();
        longitude=location.getLongitude();

        updateLocation();

        if(isBetterLocation(location)){
            currentBestLocation=location;
        }

    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate

     */

    protected boolean isBetterLocation(Location location) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            currentBestLocation=location;
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private void updateLocation(){

        String url = ApiConstants.URL_ADD_ENG_LOCATION;;

        JsonObjectRequest request = null;
        try {
            request = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ((MainActivity)mContext).removeProgressDialog();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ((BaseApplication)getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

    }

    private String getJSON() {
        String json = "";
        Gson gson = new Gson();
        Type type;


        EngineerLocation location = new EngineerLocation(
                EngineerPreference.getInstance().getUserId(),
                longitude,
                latitude
        );


        type = new TypeToken<EngineerLocation>() {
        }.getType();
        json = gson.toJson(location, type);


        return json;
    }
}
