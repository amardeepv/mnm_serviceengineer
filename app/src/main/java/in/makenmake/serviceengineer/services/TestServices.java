package in.makenmake.serviceengineer.services;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.ProviderError;
import fr.quentinklein.slt.TrackerSettings;
import in.bapps.application.BaseApplication;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * Created by r.yadav on 06/02/2017.
 */

public class TestServices extends Service {
    public boolean isRunning = false;
    Context mContext;
    private static final String TAG = "TestServices";
    public double latitude;
    public double longitude;
    private LocationTracker tracker;
    private Runnable runnable;
    private Handler myHandler;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mContext=this;
        ToastUtils.showToast(this,"service is created", Toast.LENGTH_LONG);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(isRunning){
            Log.i(TAG, "onStartCommand: service is already running");
            return START_STICKY;
        }

        Log.i(TAG, "onStartCommand: "+"service is started");
         /*myHandler=new Handler();



         runnable=new Runnable() {
            @Override
            public void run() {

            }
        };*/
    //    myHandler.post(runnable);

        try {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // You need to ask the user to enable the permissions
            } else {
                tracker = new LocationTracker(mContext,
                        new TrackerSettings()
                                .setUseGPS(true)
                                .setUseNetwork(false)
                                .setUsePassive(false)
                                .setTimeout(60 * 1000)
                                .setTimeBetweenUpdates(30 * 1000)
                                .setMetersBetweenUpdates(100)) {


                    public double localLongitude;
                    public double localLatitude;

                    @Override
                    public void onLocationFound(Location location) {
                        // Toast.makeText(MainActivity.this,"hey your location : latitude is :"+location.getLatitude()+" and longitude is : "+location.getLongitude(),Toast.LENGTH_SHORT).show();
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        if (localLongitude != location.getLongitude() || localLatitude != location.getLatitude()) {
                            this.localLatitude = location.getLatitude();
                            this.localLongitude = location.getLongitude();
                            updateLocation();
                        }
                    }

                    @Override
                    public void onTimeout() {
                        Log.i(TAG, "onTimeout: ");
                    }

                    @Override
                    public void onProviderDisabled(@NonNull String provider) {
                        super.onProviderDisabled(provider);
                        Log.i(TAG, "onProviderDisabled: "+provider.getClass().getName());
                    }

                    @Override
                    public void onProviderError(@NonNull ProviderError providerError) {
                        super.onProviderError(providerError);
                        Log.i(TAG, "onProviderError: "+providerError.getMessage());
                    }

                    @Override
                    public void onStatusChanged(@NonNull String provider, int status, Bundle extras) {
                        super.onStatusChanged(provider, status, extras);
                        Log.i(TAG, "onStatusChanged: "+provider.getClass().getName());
                    }
                };
                tracker.startListening();
                isRunning = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "run: "+e.getMessage());
            Log.e(TAG, "run: ",e );
            isRunning = false;
        }


        return START_STICKY;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        tracker.stopListening();
       // myHandler.removeCallbacks(runnable);
    }

    private void updateLocation(){

        String url = ApiConstants.URL_ADD_ENG_LOCATION;;

        JsonObjectRequest request = null;
        try {
            request = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                               String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ((MainActivity)mContext).removeProgressDialog();
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ((BaseApplication)getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

    }

    private String getJSON() {
        String json = "";
        Gson gson = new Gson();
        Type type;


                EngineerLocation location = new EngineerLocation(
                        EngineerPreference.getInstance().getUserId(),
                        longitude,
                        latitude
                );


                type = new TypeToken<EngineerLocation>() {
                }.getType();
                json = gson.toJson(location, type);


        return json;
    }

/*    private class DoBackgroundTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            String dataToSend = params[0];
            Log.i("FROM STATS SERVICE DoBackgroundTask", dataToSend);
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Utilities.AGENT_URL);

            try {
                httpPost.setEntity(new StringEntity(dataToSend, "UTF-8"));

                // Set up the header types needed to properly transfer JSON
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Accept-Encoding", "application/json");
                httpPost.setHeader("Accept-Language", "en-US");

                // Execute POST
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity responseEntity = httpResponse.getEntity();
                if (responseEntity != null) {
                    response = EntityUtils.toString(responseEntity);
                } else {
                    response = "{\"NO DATA:\"NO DATA\"}";
                }
            } catch (ClientProtocolException e) {
                response = "{\"ERROR\":" + e.getMessage().toString() + "}";
            } catch (IOException e) {
                response = "{\"ERROR\":" + e.getMessage().toString() + "}";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            Utilities.STATUS = result;
            Log.i("FROM STATUS SERVICE: STATUS IS:", Utilities.STATUS);
            super.onPostExecute(result);
        }
    }*/

}
