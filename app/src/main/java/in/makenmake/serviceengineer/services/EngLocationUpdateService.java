package in.makenmake.serviceengineer.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import in.bapps.application.BaseApplication;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.utils.LogUtil;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * Created by Bucky on 14/06/2017.
 */

public class EngLocationUpdateService  extends Service
{
    public static final int TWO_MINUTES =  1000*60*3; // 120 seconds
    public static final long TIME_BW_UPDATE =  1000*60*3; // 120 seconds
    public static final float DIS_METER_BW_UPDATE =  50.00f; // 120 seconds

    public static Boolean isRunning = false;

    public LocationManager mLocationManager;
    public LocationUpdaterListener mLocationListener;

    public Location previousBestLocation = null;

    private static final String TAG = "EngLocUpdateService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate() called");
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationUpdaterListener();
        super.onCreate();
    }

    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable(){
        @Override
        public void run() {
            if (!isRunning) {
                startListening();
            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() called with: intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId + "]");
      //  mHandlerTask.run();
        mHandler.postDelayed(mHandlerTask, TIME_BW_UPDATE);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy() called");
        stopListening();
        mHandler.removeCallbacks(mHandlerTask);
        super.onDestroy();
    }

    private void startListening() {
        Log.d(TAG, "startListening() called");
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_BW_UPDATE, DIS_METER_BW_UPDATE, mLocationListener);

            if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_BW_UPDATE, DIS_METER_BW_UPDATE, mLocationListener);
        }
        isRunning = true;
    }

    private void stopListening() {
        Log.d(TAG, "stopListening() called");
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(mLocationListener);
        }
        isRunning = false;
    }

    public class LocationUpdaterListener implements LocationListener
    {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]");
            if (isBetterLocation(location, previousBestLocation)) {
                previousBestLocation = location;
                try {
                   // updateLocation();
                    LogUtil.writeLog("updated location latitude :"+previousBestLocation.getLatitude()+" Longitude"+previousBestLocation.getLongitude());
                }
                catch (Exception e) {
                    e.printStackTrace();
                    stopListening();

                }
                finally {
                    //stopListening();
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "onProviderDisabled() called with: provider = [" + provider + "]");
            stopListening();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "onProviderEnabled() called with: provider = [" + provider + "]");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "onStatusChanged() called with: provider = [" + provider + "], status = [" + status + "], extras = [" + extras + "]");
        }
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        Log.d(TAG, "isBetterLocation() called with: location = [" + location + "], currentBestLocation = [" + currentBestLocation + "]");
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TIME_BW_UPDATE;
        boolean isSignificantlyOlder = timeDelta < -TIME_BW_UPDATE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        Log.d(TAG, "isSameProvider() called with: provider1 = [" + provider1 + "], provider2 = [" + provider2 + "]");
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }



    private void updateLocation(){
        Log.d(TAG, "updateLocation() called");

        String url = ApiConstants.URL_ADD_ENG_LOCATION;;

        JsonObjectRequest request = null;
        try {
            request = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ((BaseApplication)getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

    }

    private String getJSON() {
        String json = "";
        Gson gson = new Gson();
        Type type;


        EngineerLocation location = new EngineerLocation(
                EngineerPreference.getInstance().getUserId(),
                previousBestLocation.getLatitude(),
                previousBestLocation.getLongitude()
        );


        type = new TypeToken<EngineerLocation>() {
        }.getType();
        json = gson.toJson(location, type);


        return json;
    }
}