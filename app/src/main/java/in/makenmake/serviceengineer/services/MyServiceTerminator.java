package in.makenmake.serviceengineer.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Bucky on 04/04/2017.
 */

public class MyServiceTerminator extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }


    public void onCreate()
    {
        Intent service = new Intent(this, LocationUpdateService.class);
        stopService(service);   //stop MyService
        stopSelf();     //stop MyServiceTerminator so that it doesn't keep running uselessly
    }

    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }
}
