package in.makenmake.serviceengineer.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import in.bapps.application.BaseApplication;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.utils.LogUtil;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.db.Locations;
import in.makenmake.serviceengineer.db.LocationsDao;
import in.makenmake.serviceengineer.model.LocationVo;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.Const;
import in.makenmake.serviceengineer.utils.EngineerPreference;

import static in.bapps.application.BaseApplication.getDaoSession;


/**
 * Created by Bucky on 04/04/2017.
 */

public class LocationUpdateService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    protected static final String TAG = "LocationUpdateService";
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000*60*5;

    public static final long TIME_BW_UPDATE =  1000*60*3; // 120 seconds
    public static final float DIS_METER_BW_UPDATE =  50.00f; // 120 seconds

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    public static Boolean mRequestingLocationUpdates;
    /**
     * Time when the location was updated represented as a String.
     */
    protected String mLastUpdateTime;
    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    /**
     * Represents a geographical location.
     */

    public static Boolean isRunning = false;

    protected Location mCurrentLocation;
    public static boolean isEnded = false;
    private ArrayList<LocationVo> mLocationData;

    @Override
    public void onCreate() {
        super.onCreate();
        // Kick off the process of building a GoogleApiClient and requesting the LocationServices
        // API.
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable(){
        @Override
        public void run() {
            if (!isRunning) {
                isEnded = false;
                mRequestingLocationUpdates = false;
                mLastUpdateTime = "";
                buildGoogleApiClient();
                if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                    startLocationUpdates();
                }
            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
        Log.d("LOC", "Service init...");


        showNotification();
        mHandlerTask.run();


        //mHandler.postDelayed(mHandlerTask, TIME_BW_UPDATE);
        return START_STICKY;
    }


    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended==");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

        if(isBetterLocation(location,mCurrentLocation)){
            mCurrentLocation = location;
            mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
            updateUI();
        /*Toast.makeText(this, getResources().getString(R.string.location_updated_message),
                Toast.LENGTH_SHORT).show();*/
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient===");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
    }

    public void setLocationData() {

        mLocationData = new ArrayList<>();
        LocationVo mLocVo = new LocationVo();
        mLocVo.setmLongitude(mCurrentLocation.getLongitude());
        mLocVo.setmLatitude(mCurrentLocation.getLatitude());
        mLocVo.setmLocAddress(Const.getCompleteAddressString(this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
        mLocationData.add(mLocVo);

    }

    /**
     * Updates the latitude, the longitude, and the last location time in the UI.
     */
    private void updateUI() {
      /*  setLocationData();
        Toast.makeText(this, "Latitude: =" + mCurrentLocation.getLatitude() + " Longitude:=" + mCurrentLocation
                .getLongitude(), Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Latitude:==" + mCurrentLocation.getLatitude() + "\n Longitude:==" + mCurrentLocation.getLongitude
                ());
        */
        LogUtil.writeLog("updated location latitude :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());

      //  updateLocation();

        new MyAsyncTask().execute(getJSON());



    }

    private void updateServer() {

    }


    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    protected void createLocationRequest() {
        mGoogleApiClient.connect();
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);



        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected void startLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;

            // The final argument to {@code requestLocationUpdates()} is a LocationListener
            // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            Log.i(TAG, " startLocationUpdates===");
            isEnded = true;

            isRunning=true;
        }


    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected void stopLocationUpdates() {
        if (mRequestingLocationUpdates) {
            mRequestingLocationUpdates = false;
            // It is a good practice to remove location requests when the activity is in a paused or
            // stopped state. Doing so helps battery performance and is especially
            // recommended in applications that request frequent location updates.

            Log.d(TAG, "stopLocationUpdates();==");
            // The final argument to {@code requestLocationUpdates()} is a LocationListener
            // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

            isRunning = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent("in.makenmake.serviceengineer.services.START_LOCATION_SERVICE");
        sendBroadcast(broadcastIntent);
        stopLocationUpdates();
        mHandler.removeCallbacks(mHandlerTask);
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        Log.d(TAG, "isBetterLocation() called with: location = [" + location + "], currentBestLocation = [" + currentBestLocation + "]");
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TIME_BW_UPDATE;
        boolean isSignificantlyOlder = timeDelta < -TIME_BW_UPDATE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        Log.d(TAG, "isSameProvider() called with: provider1 = [" + provider1 + "], provider2 = [" + provider2 + "]");
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    private void updateLocation(){
        try{
        Log.d(TAG, "updateLocation() called");

        String url = ApiConstants.URL_ADD_ENG_LOCATION;

        JsonObjectRequest request = null;

            request = new JsonObjectRequest(Request.Method.POST, url,new JSONObject(getJSON()),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            String resultResponse = response.toString();
                            // parse success output


                            Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                            }.getType();
                            CommonJsonResponseNew<String> locationRes = new Gson().fromJson(resultResponse, listType3);
                            if (!locationRes.message.equals("")) {
                                // ToastUtils.showToast(this,locationRes.message);
                                Log.i(TAG, "location added: " + locationRes.message);
                                LogUtil.writeLog("Server location added :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    LogUtil.writeLog("VolleyError while updating to server :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());
                    LogUtil.writeLog("VolleyError is :"+error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();//Constants.getHeaders(context);
                    // add headers <key,value>

                    String auth = "bearer "
                            + EngineerPreference.getInstance().getAuthToken();
                    headers.put("Authorization", auth);
                    return headers;
                }
            };

            ((BaseApplication)getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.writeLog("Exception while updating to server :"+mCurrentLocation.getLatitude()+" Longitude"+mCurrentLocation.getLongitude());
            LogUtil.writeLog("Exception is :"+e.getMessage());
        }



    }

    private String getJSON() {

        String json = "";
        Gson gson = new Gson();
        Type type;


        EngineerLocation location = new EngineerLocation(
                EngineerPreference.getInstance().getUserId(),
                mCurrentLocation.getLongitude(),
                mCurrentLocation.getLatitude()

        );


        type = new TypeToken<EngineerLocation>() {
        }.getType();
        json = gson.toJson(location, type);


        return json;
    }
/*
    private class MyAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }


        @Override
        protected String doInBackground(String... params) {
            String sendJsonData = params[0];
            String jsonResponse = null;
            try {
                if (ConnectivityUtils.isNetworkEnabled(LocationUpdateService.this)) {
                    JSONObject jsonObjRecv;
                    String line;
                    StringBuffer jsonString = new StringBuffer();

                    URL url = new URL("https://makenmake.in/mnmapi/AddCurrentEngineerLocation");
                    HttpURLConnection uc = (HttpURLConnection) url.openConnection();
                    uc.setRequestMethod("POST");
                    uc.connect();

                    uc.setDoInput(true);
                    uc.setDoOutput(true);
                    uc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

                    if (sendJsonData != null) {
                        OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");
                        writer.write(sendJsonData.toString());
                        writer.flush();
                    }

                    int statusCode = uc.getResponseCode();

                    if (statusCode == 200) {
                       // InputStream inputStream = new BufferedInputStream(uc.getInputStream());


                        BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                        while ((line = br.readLine()) != null) {
                            jsonString.append(line);
                        }
                        br.close();

                        uc.disconnect();
                        return jsonString.toString();

                    }
                }
            } catch (MalformedURLException e) {
                Log.e(TAG, "doInBackground: ",e );
                e.printStackTrace();
            }
            catch (IOException e) {
                Log.e(TAG, "doInBackground: ",e );
                e.printStackTrace();
            }
            catch (Exception e) {
                Log.e(TAG, "doInBackground: ",e );
                e.printStackTrace();
            }


            return "";
        }






        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            // dialog.dismiss();
            if(jsonResponse!=null)
            {
                try {
                    Log.i(TAG, "onPostExecute: "+jsonResponse);
                    JSONObject objectResponse = new JSONObject(jsonResponse);


                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }
        }
    }

    */

    private class MyAsyncTask extends AsyncTask<String, String, String> {
        long currentLocationId;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            currentLocationId=insertNewLocation(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());

        }


        @Override
        protected String doInBackground(String... params) {

            String url="https://makenmake.in/mnmapi/AddCurrentEngineerLocation";
            String jsonPayload=getJSON();
            try {
                return makePostRequest(url,jsonPayload);
            } catch (IOException e) {
                e.printStackTrace();
                return "false: "+e.getMessage();
            }

        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            // dialog.dismiss();
            if(jsonResponse!=null)
            {
                JSONObject objectResponse=null;
                try {
                    Log.i(TAG, "onPostExecute: "+jsonResponse);
                 //   Toast.makeText(LocationUpdateService.this, "Response is : "+jsonResponse, Toast.LENGTH_SHORT).show();
                    try {
                        objectResponse = new JSONObject(jsonResponse);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(objectResponse !=null){
                        Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                        }.getType();
                        CommonJsonResponseNew<String> locationRes = new Gson().fromJson(jsonResponse, listType3);
                        if (!locationRes.message.equals("")) {
                           updateLocationTable(currentLocationId);
                        }
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }
    }


    public static String makePostRequest(String stringUrl, String payload) throws IOException {
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            String line;
            StringBuffer jsonString = new StringBuffer();

            uc.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            uc.addRequestProperty("Authorization", "bearer " + EngineerPreference.getInstance().getAuthToken());
            uc.setRequestMethod("POST");
            uc.setReadTimeout(15000 /* milliseconds */);
            uc.setConnectTimeout(15000 /* milliseconds */);
            uc.setDoInput(true);
            uc.setDoOutput(true);
            // uc.setInstanceFollowRedirects(false);
            // uc.connect();
            //OutputStreamWriter writer = new OutputStreamWriter(uc.getOutputStream(), "UTF-8");

            OutputStream os = uc.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.write(payload);
            writer.close();
            int responseCode = uc.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        jsonString.append(line);
                    }
                    br.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                uc.disconnect();
                return jsonString.toString();
            } else {
                return new String("false : " + responseCode);
            }
        } catch (Exception e) {
            return new String("Exception: " + e.getMessage());
        }
    }

    private void updateLocationTable(long id){

        LocationsDao locationsDao=getDaoSession().getLocationsDao();

        Locations locations=locationsDao.load(id);

        locations.setIsSynced(true);

        locationsDao.update(locations);
    }

    private Long insertNewLocation(double latitude,double longitude){
        Locations locations=new Locations();
        locations.setLatitude(latitude);
        locations.setLongitude(longitude);
        locations.setLocalLocationId(Long.toString(DateTime.now().getMillis()));
        locations.setIsSynced(false);
        locations.setCreatedDate(new Date());

        Long locationId=BaseApplication.getDaoSession().getLocationsDao().insert(locations);
        return locationId;
    }

    private Locations getLocationsFromDatabase(long id){
        return BaseApplication.getDaoSession().getLocationsDao().load(id);
    }


/*    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                ToastUtils.showToast(this, responseNew.message);
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_UPDATE_ENG_LOC:

                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                    ToastUtils.showToast(this, responseNew.message);

                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }*/



    private void showNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction("MAIN_ACTION");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Intent previousIntent = new Intent(this, LocationUpdateService.class);
        previousIntent.setAction("LAST");
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(this, LocationUpdateService.class);
        playIntent.setAction("PLAY");
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, 0);

        Intent nextIntent = new Intent(this, LocationUpdateService.class);
        nextIntent.setAction("NEXT");
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Service Engineer")
                .setTicker("Updating location")
                .setContentText("")
                .setSmallIcon(R.drawable.engineer)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                /*.addAction(android.R.drawable.ic_media_previous, "Previous",
                        ppreviousIntent)
                .addAction(android.R.drawable.ic_media_play, "Play",
                        pplayIntent)
                .addAction(android.R.drawable.ic_media_next, "Next",
                        pnextIntent)*/
                .build();
        startForeground(101,notification);

    }
}