package in.makenmake.serviceengineer.location;

public abstract interface GeocoderHandler
{
  public abstract void getAddress(GeocoderResponse paramGeocoderResponse);
}

