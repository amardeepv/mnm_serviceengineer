package in.makenmake.serviceengineer.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import in.makenmake.serviceengineer.utils.EngineerPreference;

public class LocationBroadcastReceiver extends BroadcastReceiver {
    LocationListener mLocationListener;

    public void onReceive(Context paramContext, Intent paramIntent) {
        //TODO:  defining new paramContext to
        Location locatioContext;
        if ((paramIntent != null) && (paramIntent.getParcelableExtra("location_key") != null)) {
            locatioContext = (Location) paramIntent.getParcelableExtra("location_key");
            this.mLocationListener.onLocationUpdate(locatioContext);
            Log.d("Location : ", "Latitude : " + locatioContext.getLatitude() + "Logitude : " + locatioContext.getLongitude());
            EngineerPreference.getInstance().setLatitude(String.valueOf(locatioContext.getLatitude()));
            EngineerPreference.getInstance().setLongitude(String.valueOf(locatioContext.getLongitude()));
        }
    }

    public void setLocationListener(LocationListener paramLocationListener) {
        this.mLocationListener = paramLocationListener;
    }
}
