package in.makenmake.serviceengineer.location;

import android.location.Address;

public class GeocoderResponse
{
  private String errorMessage;
  private double latitude;
  private double longitude;
  private Address mAddress;

  public String getErrorMessage()
  {
    return this.errorMessage;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public Address getmAddress()
  {
    return this.mAddress;
  }

  public void setErrorMessage(String paramString)
  {
    this.errorMessage = paramString;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setmAddress(Address paramAddress)
  {
    this.mAddress = paramAddress;
  }
}

