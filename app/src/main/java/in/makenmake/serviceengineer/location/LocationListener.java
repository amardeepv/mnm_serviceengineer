package in.makenmake.serviceengineer.location;

import android.location.Location;

public abstract interface LocationListener
{
  public abstract void onLocationUpdate(Location paramLocation);
}

