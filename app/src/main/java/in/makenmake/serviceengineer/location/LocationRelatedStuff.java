package in.makenmake.serviceengineer.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationRelatedStuff
        implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final long FASTEST_INTERVAL = 5000L;
    private static final int FASTEST_INTERVAL_IN_SECONDS = 5;
    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final String TAG = "LocationRelatedStuff";
    private static final long UPDATE_INTERVAL = 30000L;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 30;
    private int callBackIdentifier;
    private CurrentLocationListener currentLocationListener;
    private GoogleApiClient googleApiClient;
    private Context mContext;
    LocationRequest mLocationRequest;

    public LocationRelatedStuff(Context paramContext) {
        this.mContext = paramContext;
        this.googleApiClient = new Builder(this.mContext).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        this.currentLocationListener = new CurrentLocationListener();
        intializeLocationRequest();
    }

    private void broadcastLocation(Location paramLocation) {
        Intent localIntent = new Intent("lcoation_intent_action");
        localIntent.putExtra("location_key", paramLocation);
        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(localIntent);
    }

    private void intializeLocationRequest() {
        this.mLocationRequest = LocationRequest.create();
        this.mLocationRequest.setPriority(100);
        this.mLocationRequest.setInterval(30000L);
        this.mLocationRequest.setFastestInterval(5000L);
    }

    public boolean checkIfLocationIsEnabled() {
/*        LocationManager localLocationManager = (LocationManager) this.mContext.getSystemService("location");
        boolean i = false;
        int j = 0;
        try {
            boolean bool = localLocationManager.isProviderEnabled("gps");
            i = bool;
            try {
                label28:
                bool = localLocationManager.isProviderEnabled("network");
                j = bool;
                label38:
                if ((i != 0) && (j != 0)) {
                    Log.d("LocationRelatedStuff", "location is enabled");
                    return true;
                }
                Log.d("LocationRelatedStuff", "location is disabled");
                return false;
            } catch (Exception localException1) {
                break label38;
            }
        } catch (Exception localException2) {
            break label28;
        }*/

        return false;
    }

    public void disconnectClient() {
        if (this.googleApiClient != null)
            this.googleApiClient.disconnect();
    }

    public void disconnectLocationChangeUpdates() {
        if ((this.currentLocationListener != null) && (this.googleApiClient.isConnected()))
            this.googleApiClient.disconnect();
    }

    public Location getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        return LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);
    }

    public boolean isClientConnected() {
        return this.googleApiClient.isConnected();
    }

    public void onConnected(Bundle paramBundle) {
        Log.d("LocationRelatedStuff", "location client connected!");
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(this.googleApiClient, this.mLocationRequest, this.currentLocationListener);

        //TODO: commented
/*        paramBundle = LocationServices.FusedLocationApi.getLastLocation(this.googleApiClient);
        if (paramBundle != null)
            broadcastLocation(paramBundle);*/
    }

    public void onConnectionFailed(ConnectionResult paramConnectionResult) {
        Log.d("LocationRelatedStuff", "connection falied!");
        try {
            paramConnectionResult.startResolutionForResult((Activity) this.mContext, 9000);
            if (!paramConnectionResult.hasResolution()) ;
        } catch (IntentSender.SendIntentException localSendIntentException2) {
            try {
                paramConnectionResult.startResolutionForResult((Activity) this.mContext, 9000);
                if (!paramConnectionResult.hasResolution()) ;
            } catch (IntentSender.SendIntentException localSendIntentException1) {
                try {
                    //TODO : commented

                    /*while (true) {
                        paramConnectionResult.startResolutionForResult((Activity) this.mContext, 9000);

                        localSendIntentException1 = localSendIntentException1;
                        localSendIntentException1.printStackTrace();
                        return;
                    }*/

                    localSendIntentException2 = localSendIntentException2;
                    localSendIntentException2.printStackTrace();
                } /*catch (IntentSender.SendIntentException paramConnectionResult1) {
                    paramConnectionResult1.printStackTrace();
                }*/ catch (Exception paramConnectionResult1) {
                    paramConnectionResult1.printStackTrace();
                }
            }
        }
    }

    public void onConnectionSuspended(int paramInt) {
    }

    public void startListeningForLocationUpdates() {
        this.googleApiClient.connect();
    }

    class CurrentLocationListener implements LocationListener {
        CurrentLocationListener() {
        }

        public void onLocationChanged(Location paramLocation) {
            Log.d("LocationRelatedStuff", "location changed- lat:" + paramLocation.getLatitude() + " long:" + paramLocation.getLongitude());
            LocationRelatedStuff.this.broadcastLocation(paramLocation);
        }
    }
}

