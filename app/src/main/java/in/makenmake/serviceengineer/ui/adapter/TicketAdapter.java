package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import in.bapps.utils.DateUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.enums.TicketStatusString;
import in.makenmake.serviceengineer.model.Ticket;

/**
 * Created by Bucky on 29/01/2017.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.MyViewHolder> {
    private Context mContext;
    private List<Ticket> ticketList;
    public IMyViewHolderClicks mListener;

    public void setTicketList(List<Ticket> ticketList) {
        this.ticketList = ticketList;
    }



    public  class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView serviceIcon;

        public TextView ticketId=null,userName, userAddress,timeSlot,status,txt_expire_timeout;
        TextView inprocess;
        TextView txtTicketDate;
        ImageView acceptTicket,rejectTicket;
        CardView cardView;

        public MyViewHolder(View view,IMyViewHolderClicks listener) {
            super(view);
            mListener=listener;

            ticketId = (TextView) view.findViewById(R.id.txt_ticket_id);
            userName = (TextView) view.findViewById(R.id.txt_customer_name);
            serviceIcon= (ImageView) view.findViewById(R.id.img_service_icon);
            status = (TextView) view.findViewById(R.id.txt_ticket_status);
            userAddress = (TextView) view.findViewById(R.id.txt_customer_address);
            timeSlot= (TextView) view.findViewById(R.id.txt_ticket_time_slot);
            txtTicketDate= (TextView) view.findViewById(R.id.txt_ticket_date);
            //inprocess= (TextView) view.findViewById(R.id.txt_inprocess_ticket);
            acceptTicket= (ImageView) view.findViewById(R.id.img_ticket_accept);
            rejectTicket= (ImageView) view.findViewById(R.id.img_ticket_reject);

            txt_expire_timeout= (TextView) view.findViewById(R.id.txt_expire_timeout);

            cardView= (CardView) view.findViewById(R.id.card_view);

          /*  acceptTicket.setOnClickListener(this);
            rejectTicket.setOnClickListener(this);
            cardView.setOnClickListener(this);*/


        }

/*        @Override
        public void onClick(View v) {
            int _ticketId=Integer.parseInt(ticketId.getText().toString());
            if(v.getId()==acceptTicket.getId()){
                mListener.onTicketAcceptButtonClick(_ticketId);
            }
            else if(v.getId()==rejectTicket.getId()){
                mListener.onTicketRejectButtonClick(_ticketId);
            }
            if(v.getId()==R.id.card_view){
                mListener.onTicketViewClick(_ticketId);
            }

        }*/

    }
    public  interface IMyViewHolderClicks {
         void onTicketAcceptButtonClick(int ticketId);
         void onTicketRejectButtonClick(int ticketId);
         void onTicketViewClick(int ticketId);

    }

    public TicketAdapter(Context mContext, List<Ticket> ticketList,IMyViewHolderClicks listener) {
        this.mContext = mContext;
        this.ticketList = ticketList;
        this.mListener=listener;
    }

    @Override
    public TicketAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ticket_card, parent, false);

        return new TicketAdapter.MyViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final TicketAdapter.MyViewHolder holder, final int position) {
/*        Ticket ticketModel = ticketList.get(position);
        holder.title.setText(ticketModel.getName());
        holder.count.setText(ticketModel.getNumOfSongs() + " songs");

        // loading album cover using Glide library
        Glide.with(mContext).load(ticketModel.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/


        Ticket ticket=ticketList.get(position);

        String status=ticket.getTicketStatus();

        String ticketDate=ticket.getTicketOpenTime().substring(0,10);
            //2017-05-26T13:00:38.93

        Date date=null;
        try {
             date=new SimpleDateFormat("yyyy-MM-dd").parse(ticketDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String formattedDate="";

        if(date !=null) {
            formattedDate = DateUtils.getFormattedDate(date, "dd-MM-yy");
        }

        Glide.with(mContext).load("http://icons.iconarchive.com/icons/icons8/windows-8/128/Household-Electrical-icon.png").into(holder.serviceIcon);
        holder.timeSlot.setText(ticket.getTimeSlot());
        holder.ticketId.setText(ticket.getTicketId()+"");
        holder.userAddress.setText(ticket.getConsumerDetails().getConsumerAddress().getAddress());
        holder.userName.setText(ticket.getConsumerDetails().getCustomerName());
        holder.status.setText(ticket.getTicketStatus().toString());
        holder.txtTicketDate.setText(formattedDate);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTicketViewClick(position);
            }
        });

        if(status.equalsIgnoreCase(TicketStatusString.assigned)){
            holder.rejectTicket.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    mListener.onTicketRejectButtonClick(position);
                }
            });
            holder.acceptTicket.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    mListener.onTicketAcceptButtonClick(position);
                }
            });
        }
        else if(status.equalsIgnoreCase(TicketStatusString.accepted)){
            holder.rejectTicket.setVisibility(View.GONE);
            holder.txt_expire_timeout.setVisibility(View.GONE);
            holder.acceptTicket.setVisibility(View.GONE);
           // holder.acceptTicket.setText("Accepted");

        }
        else if(status.equalsIgnoreCase(TicketStatusString.in_progress)){
            holder.rejectTicket.setVisibility(View.GONE);
            holder.txt_expire_timeout.setVisibility(View.GONE);
            holder.acceptTicket.setVisibility(View.GONE);

           // holder.acceptTicket.setText("In Progress");

        }


    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }



    /*public void swap(ArrayList<Data> datas){
        data.clear();
        data.addAll(datas);
        notifyDataSetChanged();
    }*/

    // to handle empty data set

    /*public void swap(List list){
        if (mFeedsList != null) {
            mFeedsList.clear();
            mFeedsList.addAll(list);
        }
        else {
            mFeedsList = list;
        }
        notifyDataSetChanged();
    }*/
}
