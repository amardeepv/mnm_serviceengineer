package in.makenmake.serviceengineer.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.LeaveStatusModel;
import in.makenmake.serviceengineer.model.response.LeaveStatusResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.LeaveStatusAdapter;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveStatusFragment extends Fragment implements UpdateListener.onUpdateViewListener, LeaveStatusAdapter.IMyViewHolderClicks {

    private static final String TAG = "LeaveStatusFragment";
    private Context mContext;

    private RecyclerView recyclerView;
    private LeaveStatusAdapter adapter;
    private List<LeaveStatusModel> leaveStatusList;


    public LeaveStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_leave_status, container, false);



        recyclerView = (RecyclerView) view.findViewById(R.id.rview_leave_status_list);

        leaveStatusList = new ArrayList<>();
        adapter = new LeaveStatusAdapter(mContext, leaveStatusList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new AlbumActivity.GridSpacingItemDecoration(2, dpToPx(10), true));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.setAdapter(adapter);

        loadLeaveStatus();

        return view;
    }

    private void loadLeaveStatus() {
        hitApiRequest(ApiConstants.REQUEST_LEAVES_STATUS);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LEAVES_STATUS:
                ((MainActivity)mContext).showProgressDialog();
                url = ApiConstants.URL_LEAVES_STATUS+ EngineerPreference.getInstance().getUserId();
                className=CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {});
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response =( CommonJsonResponseNew<String>)responseObject ;
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }

                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_LEAVES_STATUS:

                    Type listType2 = new TypeToken<CommonJsonResponseNew<LeaveStatusResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<LeaveStatusResponse> response = new Gson().fromJson(new Gson().toJson(responseObject), listType2);

                    leaveStatusList=response.data.getServiceEngineerByID();

                    if(leaveStatusList.size()>0){
                        updateLeaveStatusAdapter(leaveStatusList);
                    }

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateLeaveStatusAdapter(List<LeaveStatusModel> leaveStatusList) {
        adapter.setLeaveStatusList(leaveStatusList);
        adapter.notifyDataSetChanged();
    }
}
