package in.makenmake.serviceengineer.ui.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.DateUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.ApplyLeaveRequest;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ApplyLeaveFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ApplyLeaveFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ApplyLeaveFragment extends BaseFragment implements UpdateJsonListener.onUpdateViewJsonListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private DatePickerDialog datePickerDialog;

    Context mContext;

    String myFormat = "dd/MM/yyyy"; //In which you need put here

    @BindView(R.id.edt_leaveStartDate)  EditText edtLeaveStartDate;
    @BindView(R.id.edt_leaveEndDate)    EditText edtLeaveEndDate;
    @BindView(R.id.edt_weeklyOffDate)    EditText edt_weeklyOffDate;
    @BindView(R.id.edtLeaveReason)    EditText edtLeaveReason;
    @BindView(R.id.btn_submit_leave)    Button btnSubmit;
    @BindView(R.id.btn_cancle_leave)    Button btnCancle;
    @BindView(R.id.radioButtonLeave)    RadioButton radioButtonLeave;
    @BindView(R.id.radioButtonWeeklyOff)    RadioButton radioButtonWeeklyOff;
    @BindView(R.id.radioButtonHalfDay)    RadioButton radioButtonHalfDay;
    @BindView(R.id.radioButtonFullDay)    RadioButton radioButtonFullDay;
    @BindView(R.id.radioButtonFirstHalf)    RadioButton radioButtonFirstHalf;
    @BindView(R.id.radioButtonSecondHalf)    RadioButton radioButtonSecondHalf;
    @BindView(R.id.layout_leaveStart)    TextInputLayout layout_leaveStart;
    @BindView(R.id.layout_leaveEnd)    TextInputLayout layout_leaveEnd;
    @BindView(R.id.layout_weeklyOffDate)    TextInputLayout layout_weeklyOffDate;

    @BindView(R.id.llLeavePeriod)    LinearLayout llLeavePeriod;
    @BindView(R.id.llLeavePeriodPart)    LinearLayout llLeavePeriodPart;
    @BindView(R.id.layout_halfDayLeaveDate)    TextInputLayout layout_halfDayLeaveDate;
    @BindView(R.id.edt_halfDayLeaveDate)    EditText edt_halfDayLeaveDate;

    String type ="L";
    String peroid="F";
    String leaveStartDate=null;
    String leaveEndDate=null;
    int noOfDays=0;




    public ApplyLeaveFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ApplyLeaveFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ApplyLeaveFragment newInstance(String param1, String param2) {
        ApplyLeaveFragment fragment = new ApplyLeaveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void setHeader() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_apply_leave, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @OnCheckedChanged(R.id.radioButtonLeave)
    public void onRadioButtonLeaveChanged(boolean checked){
        if(checked) {
            //ToastUtils.showToast(mContext, "leave checked :" + checked);
            type="L";

            layout_leaveStart.setVisibility(View.VISIBLE);
            layout_leaveEnd.setVisibility(View.VISIBLE);
            llLeavePeriod.setVisibility(View.VISIBLE);
            layout_weeklyOffDate.setVisibility(View.GONE);

            llLeavePeriodPart.setVisibility(View.GONE);

            radioButtonFullDay.setChecked(true);
            radioButtonFirstHalf.setChecked(true);

        }

    }
    @OnCheckedChanged(R.id.radioButtonWeeklyOff)
    public void onRadioButtonWeeklyOffChanged(boolean checked){
        if(checked) {
//            ToastUtils.showToast(mContext, "weeklyOff checked :" + checked);
            type="W";
            layout_leaveStart.setVisibility(View.GONE);
            layout_leaveEnd.setVisibility(View.GONE);
            llLeavePeriod.setVisibility(View.GONE);
            llLeavePeriodPart.setVisibility(View.GONE);
            layout_halfDayLeaveDate.setVisibility(View.GONE);
            layout_weeklyOffDate.setVisibility(View.VISIBLE);

        }
    }
    @OnCheckedChanged(R.id.radioButtonHalfDay)
    public void onRadioButtonHalfDayChange(boolean checked){
        if(checked) {
//            ToastUtils.showToast(mContext, "halfday checked :" + checked);
            peroid="H";

            layout_leaveStart.setVisibility(View.GONE);
            layout_leaveEnd.setVisibility(View.GONE);
            layout_weeklyOffDate.setVisibility(View.GONE);

            //half day
            llLeavePeriodPart.setVisibility(View.VISIBLE);
            layout_halfDayLeaveDate.setVisibility(View.VISIBLE);

        }
    }
    @OnCheckedChanged(R.id.radioButtonFullDay)
    public void onRadioButtonFullDayChanged(boolean checked){
        if(checked) {
//            ToastUtils.showToast(mContext, "Fullday checked :" + checked);
            peroid="F";

            //start date
            layout_leaveStart.setVisibility(View.VISIBLE);
            //end date
            layout_leaveEnd.setVisibility(View.VISIBLE);


            //half day
            llLeavePeriodPart.setVisibility(View.GONE);
            layout_halfDayLeaveDate.setVisibility(View.GONE);

        }
    }


    @OnClick(R.id.edt_leaveStartDate)
    public void OnStartDateEditTextClick(){
        selectDate(edtLeaveStartDate);
        edtLeaveEndDate.setText("");
    }

    @OnClick(R.id.edt_leaveEndDate)
    public void OnEndDateEditTextClick(){
        if(edtLeaveStartDate.getText().toString().equalsIgnoreCase("")){
            ToastUtils.showToast(mContext,"Please select start date first.");
            return;
        }
        selectDate(edtLeaveEndDate);
    }

    @OnClick(R.id.edt_weeklyOffDate)
    public void OnWeeklyOffEditTextClick(){
        selectDate(edt_weeklyOffDate);
    }

    @OnClick(R.id.edt_halfDayLeaveDate)
    public void OnHalfDayLeaveEditTextClick(){
        selectDate(edt_halfDayLeaveDate);
    }

/*    @OnClick(R.id.imgStartLeave)
    public void onStartLeaveIconClick(){
        selectDate(edtLeaveStartDate);
    }
    @OnClick(R.id.imgStartLeave)
    public void onEndLeaveIconClick(){
        selectDate(edtLeaveEndDate);
    }*/

    @OnClick(R.id.btn_cancle_leave)
    public void OnCancleButtonClick(){
        //TODO : pop fragment

        ((MainActivity)mContext).removeFragmentFromBackStack(this);
    }

    @OnClick(R.id.btn_submit_leave)
    public void OnSubmitButtonClick(){

        if(validateData()) {
            hitApiRequest(ApiConstants.REQUEST_APPLY_LEAVE);
        }
    }

    private boolean validateData() {
        removeAllErrorMessage();

        if(edtLeaveReason.getText().toString()==null ||edtLeaveReason.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter reason for leave.");
            requestFocus(edtLeaveReason);
            return false;
        }

        if(radioButtonWeeklyOff.isChecked()){
             return validateWeeklyOffData();
        }
        else {

            if(radioButtonHalfDay.isChecked()){
                return validateHalfDayOffData();
            }
            else {
                return validateLeaveOffData();
            }


        }
    }

    private void removeAllErrorMessage(){
        layout_halfDayLeaveDate.setError(null);
        layout_weeklyOffDate.setError(null);
        layout_leaveStart.setError(null);
        layout_leaveEnd.setError(null);


    }

    private boolean validateWeeklyOffData(){

        if(edt_weeklyOffDate.getText().toString()==null ||edt_weeklyOffDate.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter weekly off date.");
            layout_weeklyOffDate.setError("Please enter weekly off date.");
            requestFocus(edtLeaveEndDate);
            return false;
        }

        return true;
    }


    private boolean validateHalfDayOffData(){

        if(edt_halfDayLeaveDate.getText().toString()==null ||edt_halfDayLeaveDate.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter half day leave date.");
            layout_halfDayLeaveDate.setError("Please enter half day leave date.");
            requestFocus(edtLeaveEndDate);
            return false;
        }

        //TODO : check when user is applying which half is it


        return true;
    }

    private boolean validateLeaveOffData() {
        if (edtLeaveStartDate.getText().toString() == null || edtLeaveStartDate.getText().toString().equals("")) {
            ToastUtils.showToast(mContext, "Please enter leave start date.");
            layout_leaveStart.setError("Please enter leave start date.");
            requestFocus(edtLeaveStartDate);

            return false;
        }

        if (edtLeaveEndDate.getText().toString() == null || edtLeaveEndDate.getText().toString().equals("")) {
            ToastUtils.showToast(mContext, "Please enter leave end date.");
            layout_leaveEnd.setError("Please enter leave end date.");
            requestFocus(edtLeaveEndDate);
            return false;
        }

        if(!checkDateDiffernce()){
            return false;
        }
        return true;
    }



    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((MainActivity)mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_APPLY_LEAVE:
                ((MainActivity)mContext).showProgressDialog();
                url = ApiConstants.URL_APPLY_LEAVE;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getLeaveData());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getLeaveData(){
        //int engineerID, String reason, String leavestartdate,
        // String leaveEndDate, int noofdays, String leaveType, String created,
        // String weeklyOff, int status, String type, String halfDayType,String available

        /*{
              "engineerID": 7, Done
                "reason": "test", Done
                "leavestartdate": "2017-03-16", Done
                "leaveEndDate": "2017-03-17", Done
                "noofdays": 1,      Done
                "available": "True", Done
                "leaveType": "F",   Done // "F" full day or "H"  half day
                "created": "2017-03-16", Done
                "weeklyOff": "", DOne
                "status": 0,
                "type": "L",    //"L" leave  or "W" weekly off
                "halfDayType": "FH"  // if select half day leave then "FH" first half or "SH" 2nd half
        }*/
        ApplyLeaveRequest leave=new ApplyLeaveRequest();

        leave.setEngineerID(EngineerPreference.getInstance().getUserId());
        leave.setAvailable("True");
        leave.setReason(edtLeaveReason.getText().toString());
        leave.setCreated(DateUtils.getFormattedDate(new Date(), "yyyy-MM-dd"));
        leave.setStatus(0);  // hard coded value as discussed with monika for admin use only value

        if(radioButtonWeeklyOff.isChecked()){
            leave.setLeavestartdate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edt_weeklyOffDate.getText().toString()));
            leave.setLeaveEndDate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edt_weeklyOffDate.getText().toString()));
            leave.setNoofdays(1);
            leave.setLeaveType("F");
            leave.setWeeklyOff(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edt_weeklyOffDate.getText().toString()));
            leave.setType("W");
            leave.setHalfDayType("");
        }
        else{
            if(radioButtonHalfDay.isChecked()){
                leave.setLeavestartdate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edt_halfDayLeaveDate.getText().toString()));
                leave.setLeaveEndDate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edt_halfDayLeaveDate.getText().toString()));
                leave.setNoofdays(1);
                leave.setLeaveType("H");
                leave.setWeeklyOff("");
                leave.setType("L");
                leave.setHalfDayType(radioButtonFirstHalf.isChecked()?"FH":"SH");
            }
            else{
                leave.setLeavestartdate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edtLeaveStartDate.getText().toString()));
                leave.setLeaveEndDate(DateUtils.getFormattedDate("dd/MM/yyy","yyyy-MM-dd",edtLeaveEndDate.getText().toString()));
                leave.setNoofdays(getNumberOfLeaveDays());
                leave.setLeaveType("F");
                leave.setWeeklyOff("");
                leave.setType("L");
                leave.setHalfDayType("");
            }
        }




/*             leave = new ApplyLeaveRequest(
                    EngineerPreference.getInstance().getUserId(),
                    edtLeaveReason.getText().toString(),
                    DateUtils.getFormattedDate("dd/MM/yyy", "yyyy-MM-dd", edtLeaveStartDate.getText().toString()),
                    DateUtils.getFormattedDate("dd/MM/yyy", "yyyy-MM-dd", edtLeaveEndDate.getText().toString()),
                    noOfDays,
                    type,
                    DateUtils.getFormattedDate(DateTime.now(), "yyyy-MM-dd"),
                    "",
                    0,
                    "L",
                    "FH",
                    "True"*/





        Gson gson = new Gson();
        Type type = new TypeToken<ApplyLeaveRequest>() {
        }.getType();
        String json = gson.toJson(leave, type);

        return json;

    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_APPLY_LEAVE:


                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);

                    if(response.message.equals("Data Added Successfully")){
                        ToastUtils.showToast(mContext,"Leave applied successfully");
                        clearAllFiled();
                        ((MainActivity)mContext).removeFragmentFromBackStack(this);
                    }

                   /* if(responseString=="Account  created"){
                        ToastUtils.showToast(mContext,"Please verify your mobile number");
                       // startActivityForResult(new Intent(mContext,OtpVerificationActivity.class),VERIFY_MOBILE_NUMBER_REQUEST);
                    }
                    else{
                        ToastUtils.showToast(mContext,"Some error occurred");
                    }*/
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void clearAllFiled() {
        edtLeaveEndDate.setText("");
        edtLeaveStartDate.setText("");
        edt_halfDayLeaveDate.setText("");
        edt_weeklyOffDate.setText("");
        edtLeaveReason.setText("");


    }


    public void selectDate(final EditText edtView){

        final Calendar myCalendar =  Calendar.getInstance();

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);



                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                edtView.setText(sdf.format(myCalendar.getTime()));


            }

        };


        DatePickerDialog dialog=new DatePickerDialog(mContext, onDateSetListener,
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar maxD = Calendar.getInstance();
        maxD.setTime(new Date());
        maxD.add(Calendar.DATE, 90);
        // user is required atleast tell 2 day before leave : issue mm-6
        long twoDay=1000*60*60*24*2;
        if(edtView==edtLeaveStartDate) {
            maxD.add(Calendar.DATE, 90);

            dialog.getDatePicker().setMinDate(System.currentTimeMillis()+twoDay);
            dialog.getDatePicker().setMaxDate(maxD.getTime().getTime());
        }
        else if(edtView==edtLeaveEndDate){
            maxD.add(Calendar.DATE, 90);
            dialog.getDatePicker().setMinDate(DateUtils.getDateInMillis(edtLeaveStartDate.getText().toString(),"dd/MM/yyy")- TimeUnit.DAYS.toMillis(1));
           // dialog.getDatePicker().setMinDate(System.currentTimeMillis()+twoDay);
            dialog.getDatePicker().setMaxDate(maxD.getTime().getTime());
        }
        else if(edtView==edt_weeklyOffDate){
            maxD.add(Calendar.DATE, 30);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis()+twoDay);
            dialog.getDatePicker().setMaxDate(maxD.getTime().getTime());
        }
        else if(edtView==edt_halfDayLeaveDate){
            maxD.add(Calendar.DATE, 90);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis());
            dialog.getDatePicker().setMaxDate(maxD.getTime().getTime());
        }




        dialog.show();

    }

    private boolean checkDateDiffernce() {
        try {
            String startDate = edtLeaveStartDate.getText().toString();
            String endDate = edtLeaveEndDate.getText().toString();

            if(startDate.equals("") || endDate.equals("")){
                return false;
            }


            int validDate= DateUtils.getDifferenceStartEndDate(startDate,endDate,"dd/MM/yy"); //+1 if argument date before 1st date
            if(validDate >0){
                ToastUtils.showToast(mContext,"Start date should be greater than end date.");
                return false;
            }
            else{
                return true;
            }


             // diff in days in int

        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private int getNumberOfLeaveDays(){
        String startDate = edtLeaveStartDate.getText().toString();
        String endDate = edtLeaveEndDate.getText().toString();
        return Integer.parseInt(DateUtils.getDateDiffInDays(startDate,endDate,"dd/MM/yyyy")+"")+1;
    }
}
