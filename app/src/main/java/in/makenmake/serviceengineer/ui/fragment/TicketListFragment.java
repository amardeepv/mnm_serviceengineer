package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.constants.enums.TicketStatus;
import in.makenmake.serviceengineer.constants.enums.TicketStatusString;
import in.makenmake.serviceengineer.model.Ticket;
import in.makenmake.serviceengineer.model.response.TicketListResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.TicketAdapter;
import in.makenmake.serviceengineer.utils.EngineerPreference;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TicketListFragment extends BaseFragment implements TicketAdapter.IMyViewHolderClicks, UpdateJsonListener.onUpdateViewJsonListener {


    private static final String TAG = "TicketListFragment";

    private Context mContext;
    private RecyclerView recyclerView;
    private TicketAdapter adapter;
    private List<Ticket> mTicketList;
    Timer t;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private OnFragmentInteractionListener mListener;
    private int ticketStatus;
    private int selectedTicket;

    public TicketListFragment() {
        // Required empty public constructor
    }

    public static TicketListFragment newInstance(String param1, String param2) {
        TicketListFragment fragment = new TicketListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setHeader() {
        getActivity().setTitle("Home");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view= inflater.inflate(R.layout.fragment_ticket_list, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.rview_ticket_list);

        mTicketList = new ArrayList<>();
        adapter = new TicketAdapter(mContext, mTicketList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
       // recyclerView.addItemDecoration(new AlbumActivity.GridSpacingItemDecoration(2, dpToPx(10), true));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.setAdapter(adapter);

        mSwipeRefreshLayout= (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                onLoadTicketList();
            }
        });

       // getTicketList();

        t = new Timer();
        t.schedule(new TimerTask() {

            public void run() {
                onLoadTicketList();
            }
        }, 1000);

        return view;
    }

    public void onLoadTicketList() {
        hitApiRequest(ApiConstants.REQUEST_GET_TICKET);
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void getTicketList(){
        try {
            InputStream is = mContext.getAssets().open("data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");

            Gson gson=new Gson();

            Type listType = new TypeToken<List<Ticket>>() {
            }.getType();

            JSONArray obj = new JSONArray(json);

            //  String newJson=new Gson().toJson(json);

            mTicketList =new Gson().fromJson(json, listType);

            adapter.setTicketList(mTicketList);
            adapter.notifyDataSetChanged();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void updateTicketAdapter(List<Ticket> ticketListNew){
        adapter.setTicketList(ticketListNew);
        adapter.notifyDataSetChanged();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTicketAcceptButtonClick(int position) {

        ticketStatus=TicketStatus.accepted;
        selectedTicket=mTicketList.get(position).getTicketId();
        hitApiRequest(ApiConstants.REQUEST_UPDATE_TICKET_STATUS);
       /* TicketMasterFragment ticketMasterFragment=new TicketMasterFragment();
        Bundle data=new Bundle();
        data.putParcelable("ticket",mTicketList.get(position));
        data.putInt("ticketStatus", TicketStatus.accepted);
        ticketMasterFragment.setArguments(data);
        ((MainActivity)mContext).replaceFragment(TicketListFragment.class.getSimpleName(),ticketMasterFragment,null,true);*/
    }

    @Override
    public void onTicketRejectButtonClick(int ticketId) {
        ticketStatus=TicketStatus.reject;
        selectedTicket=mTicketList.get(ticketId).getTicketId();
        hitApiRequest(ApiConstants.REQUEST_UPDATE_TICKET_STATUS);
        //ToastUtils.showToast(mContext,"From Ticket reject  click",Toast.LENGTH_LONG);
    }

    @Override
    public void onTicketViewClick(int position) {


        EngineerPreference.getInstance().setSelectedTicket(mTicketList.get(position));
        EngineerPreference.getInstance().setTicketInProgress(mTicketList.get(position));

        if(mTicketList.get(position).getTicketStatus().equalsIgnoreCase(TicketStatusString.completed)){
            ((MainActivity)mContext).replaceFragment(TicketListFragment.class.getSimpleName(),new JobCardFragment(),null,true);
            return;
        }

        TicketMasterFragment ticketMasterFragment=new TicketMasterFragment();
        Bundle data=new Bundle();
        //data.putParcelable("ticket",mTicketList.get(position));
        //data.putInt("ticketStatus", TicketStatus.assigned);
        ticketMasterFragment.setArguments(data);
        ((MainActivity)mContext).replaceFragment(TicketListFragment.class.getSimpleName(),new TicketDetailsFragment(),null,true);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_TICKET:
                ((MainActivity)mContext).showProgressDialog();
                url = ApiConstants.URL_GET_TICKET_LIST+ EngineerPreference.getInstance().getUserId();
                className=CommonJsonResponseNew.class;
                    request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {});

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_UPDATE_TICKET_STATUS:
                ((MainActivity)mContext).showProgressDialog("Updating Ticket Status...");
                url = ApiConstants.URL_UPDATE_TICKET_STATUS + selectedTicket + "&status=" + ticketStatus;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, "{}");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;


            default:
                url = "";
                className = null;
                break;
        }
    }



/*    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TICKET:


                    Type listType2 = new TypeToken<TicketListResponse>() {
                    }.getType();
                    TicketListResponse response = new Gson().fromJson(responseString, listType2);

                     List<Ticket> ticketList=response.data.EngineerTicketList;



                        if(ticketList.size()>0){
                            updateTicketAdapter(ticketList);
                        }

                        mSwipeRefreshLayout.setRefreshing(false);


                   *//* if(responseString=="Account  created"){
                        ToastUtils.showToast(mContext,"Please verify your mobile number");
                       // startActivityForResult(new Intent(mContext,OtpVerificationActivity.class),VERIFY_MOBILE_NUMBER_REQUEST);
                    }
                    else{
                        ToastUtils.showToast(mContext,"Some error occurred");
                    }*//*
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }*/


/*
    private void processTicketList(List<Ticket> ticketListLocal){
        for (Ticket ticket:ticketListLocal) {
            if(ticket.getTicketStatus().equals("In Progress")){
                hitApiRequest();
            }

        }

    }
*/


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response =( CommonJsonResponseNew<String>)responseObject ;
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }

                mSwipeRefreshLayout.setRefreshing(false);
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_TICKET:

                    Type listType2 = new TypeToken<TicketListResponse>() {
                    }.getType();
                    TicketListResponse response = new Gson().fromJson(new Gson().toJson(responseObject), listType2);

                    mTicketList=response.data.EngineerTicketList;

                    if(mTicketList.size()>0){
                        updateTicketAdapter(mTicketList);
                        updateTicketInProgress(mTicketList);
                    }

                    mSwipeRefreshLayout.setRefreshing(false);

                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateTicketInProgress(List<Ticket> mTicketList) {
        for (Ticket ticket:mTicketList) {
            if (ticket.getTicketStatus().equalsIgnoreCase(TicketStatusString.in_progress) && ticket.isCheckedOut()==false) {
                EngineerPreference.setTicketInProgress(ticket);
                return;
            }
        }
        EngineerPreference.setTicketInProgress(null);
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_UPDATE_TICKET_STATUS:


                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                    ToastUtils.showToast(mContext,response.message);
                    onLoadTicketList();
                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
