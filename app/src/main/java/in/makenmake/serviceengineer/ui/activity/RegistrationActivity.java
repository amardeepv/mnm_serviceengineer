package in.makenmake.serviceengineer.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.StringUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.Register;
import in.makenmake.serviceengineer.model.response.SkillListResponse;
import in.makenmake.serviceengineer.model.response.Skills;

public class RegistrationActivity extends BaseActivity implements UpdateJsonListener.onUpdateViewJsonListener {

    //<editor-fold desc="view bind">
    @BindView(R.id.edt_full_name)
    EditText edtFullName;
    @BindView(R.id.edt_mobile_number)
    EditText edtMobileNumber;
    @BindView(R.id.edt_email_id)
    EditText edtEmailId;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
  /*  @BindView(R.id.edtPasswordConfirm_registration)
    EditText edtPasswordConfirm;*/
    @BindView(R.id.edtOptionalAgencyCode)
    EditText edtAgencyCode;
    @BindView(R.id.txt_agency)
    TextView txtAgency;
    @BindView(R.id.sp_skill_list)
    Spinner spSkill;
    @BindView(R.id.linearLayout_agencyCode)
    LinearLayout linearLayout_agencyCode;
    //</editor-fold>

    String url;

    Context mContext;

    private static final String TAG = "RegistrationActivity";

    private List<Skills> skillListModel;
    private List<String> skillList;
    private int selectSkill = -99;

    static final int VERIFY_MOBILE_NUMBER_REQUEST = 1;
    private String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        mContext = this;

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        //TODO : get skill list from server
        //TODO : get city list from server

        hitApiRequest(ApiConstants.REQUEST_SKILL_LIST);

    }


    @OnClick(R.id.btnRegisterNext)
    public void register() {
        if (((CheckBox) findViewById(R.id.checkboxAgree)).isChecked()) {
            if (validateDate()) {
                hitApiRequest(ApiConstants.REQUEST_REGISTER);
            }
        } else {
            ToastUtils.showToast(this, "Please check terms and condition");
        }
    }

    @OnClick(R.id.txt_agency)
    public void onAgencyClick() {
        txtAgency.setVisibility(View.GONE);
        linearLayout_agencyCode.setVisibility(View.VISIBLE);
    }

    @OnItemSelected(R.id.sp_skill_list)
    public void skillSelection(Spinner spinner, int position) {
        selectSkill = skillListModel.get(position).skillId-1;
    }

    private boolean validateDate() {
        if (StringUtils.isNullOrEmpty(edtFullName.getText().toString())) {
            ToastUtils.showToast(this, "Please enter name.");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtMobileNumber.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile number");
            return false;
        } else if (!StringUtils.isValidEmail(edtEmailId.getText().toString(), false)) {
            ToastUtils.showToast(this, "Please enter valid Email.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (edtPassword.getText().toString().length() < 6) {
            ToastUtils.showToast(this, "Please enter Password at least 6 char long.");
            return false;
        }
        /*else if (StringUtils.isNullOrEmpty(edtPasswordConfirm.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Confirm Password");
            return false;
        } else if (!edtPassword.getText().toString().equals(edtPasswordConfirm.getText().toString())) {
            ToastUtils.showToast(this, "Confirm password do not match");
            return false;
        }*/
        else if (spSkill.getSelectedItem().toString().equals("Select Skill")) {
            ToastUtils.showToast(this, "Please select a skill.");
            return false;
        }



        return true;
    }


    @Override
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest = null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHECK_NUMBER_REGISTER:
                url = ApiConstants.URL_CHECK_NUMBER_REGISTER + edtMobileNumber.getText().toString();
                className = String.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            /**
             * not able to make VolleyJsonRequest as response coming from server is not json a string response "Account created"
             */
            case ApiConstants.REQUEST_REGISTER:
                showProgressDialog();
                url = ApiConstants.URL_REGISTER;
                className = Object.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_SKILL_LIST:
                url = ApiConstants.URL_GET_SKILL_LIST;
                className = SkillListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
            default:
                url = "";
                className = null;
                break;
        }
    }


    public String getAddBillJson() {
//String engineerFirstName, String emailID, String mobileNumber, String password

        mobileNumber = edtMobileNumber.getText().toString();

        String name = edtFullName.getText().toString();
        String[] names = name.split(" ");

        String firstName, lastName;

        if (names.length > 1) {
            firstName = names[0];
            lastName = names[1];
        } else {
            firstName = names[0];
            lastName = "";
        }

        Register register = new Register(
                firstName,
                lastName,
                edtEmailId.getText().toString(),
                edtMobileNumber.getText().toString(),
                edtPassword.getText().toString(),
                selectSkill
        );

        Gson gson = new Gson();
        Type type = new TypeToken<Register>() {
        }.getType();
        String json = gson.toJson(register, type);

        return json;

    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_REGISTER) {


            mobileNumber = edtMobileNumber.getText().toString();

            String name = edtFullName.getText().toString();
            String[] names = name.split(" ");

            String firstName, lastName;

            if (names.length > 1) {
                firstName = names[0];
                lastName = names[1];
            } else {
                firstName = names[0];
                lastName = " ";
            }

            params.put(ApiConstants.PARAMS.FIRSTNAME, firstName);
            params.put(ApiConstants.PARAMS.LASTNAME, lastName);
            params.put(ApiConstants.PARAMS.MOBILE, edtMobileNumber.getText().toString());
            params.put(ApiConstants.PARAMS.EMAILID, edtEmailId.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD, edtPassword.getText().toString());


            params.put(ApiConstants.PARAMS.currentAddress, "");
            params.put(ApiConstants.PARAMS.currentCountry, "0");
            params.put(ApiConstants.PARAMS.currentState, "0");
            params.put(ApiConstants.PARAMS.currentDistrict, "0");
            params.put(ApiConstants.PARAMS.currentCity, "0");

            /**
             * commented as discussed with Mohit, right now agency code is not required
             */
            //params.put(ApiConstants.PARAMS.agencyCode, edtAgencyCode.getText().toString());
            params.put(ApiConstants.PARAMS.agencyCode, "test");
            params.put(ApiConstants.PARAMS.skillID, selectSkill + "");


        }
        return params;
    }

    public String getRegisterJson(){
        String json="";

        mobileNumber = edtMobileNumber.getText().toString();

        String name = edtFullName.getText().toString();
        String[] names = name.split(" ");

        String firstName, lastName;

        if (names.length > 1) {
            firstName = names[0];
            lastName = names[1];
        } else {
            firstName = names[0];
            lastName = " ";
        }

        Register register=new Register()
                .setEngineerFirstName(firstName)
                .setEngineerLastName(lastName)
                .setCurrentAddress("")
                .setCurrentCity(0)
                .setCurrentDistrict(0)
                .setCurrentState(0)
                .setCurrentCountry(0)
                .setEmailID(edtEmailId.getText().toString())
                .setMobileNumber(edtMobileNumber.getText().toString())
                .setPassword(edtPassword.getText().toString())
                .setSkillID(selectSkill)
                .setAgencyCode("");

        Gson gson = new Gson();
        Type type = new TypeToken<Register>() {
        }.getType();
         json = gson.toJson(register, type);


        return json;
    }


    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

        removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                ToastUtils.showToast(this, responseNew.message);
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER:

                    if (responseString.equalsIgnoreCase("Account  created")) {
                        ToastUtils.showToast(mContext, "Please verify your mobile number");

                        Intent intent = new Intent(mContext, OtpVerificationActivity.class);

                        intent.putExtra("mobileNumber", mobileNumber);
                        //startActivityForResult(intent,VERIFY_MOBILE_NUMBER_REQUEST);
                        startActivity(intent);
                        finish();
                    } else {
                        ToastUtils.showToast(mContext, getString(R.string.some_error_occurred));
                    }
                    break;

                case ApiConstants.REQUEST_SKILL_LIST:

                    //         responseObject = new Gson().fromJson(responseString, SkillListResponse.class);

                    Type listType2 = new TypeToken<SkillListResponse>() {
                    }.getType();
                    SkillListResponse skillListResponse = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                    //  hitApiRequest(ApiConstants.ADD_BILL);


                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setSkillListAdapter() {
        skillList = new ArrayList<>();

        skillList.add("Select Skill");

        if (skillListModel != null && skillListModel.size() > 0) {

            for (int i = 0; i < skillListModel.size(); i++) {
                skillList.add(skillListModel.get(i).skillName);
            }
            ArrayAdapter adapter = new ArrayAdapter(this, R.layout.single_row_dropdown, R.id.txt_single_row_item, skillList);

            spSkill.setAdapter(adapter);
        }

    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();

                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                if (!responseNew.message.equals("")) {
                    ToastUtils.showToast(this, responseNew.message);
                } else {
                    ToastUtils.showToast(this, "Some error occured.");
                }
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_SKILL_LIST:

                    //         responseObject = new Gson().fromJson(responseString, SkillListResponse.class);

                    Type listType2 = new TypeToken<SkillListResponse>() {
                    }.getType();
                    SkillListResponse skillListResponse = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                    //  hitApiRequest(ApiConstants.ADD_BILL);
                    skillListModel = skillListResponse.data.Skills;

                    setSkillListAdapter();

                    break;

                case ApiConstants.REQUEST_REGISTER:

                    Type str = new TypeToken<String>() {
                    }.getType();
                    String msg = new Gson().fromJson(new Gson().toJson(responseObject), str);

                    if (msg.equals("Account  created")) {
                        ToastUtils.showToast(mContext, "Please verify your mobile number");

                        Intent intent = new Intent(mContext, OtpVerificationActivity.class);

                        intent.putExtra("mobileNumber", mobileNumber);
                        startActivity(intent);
                        finish();
                    } else {
                        ToastUtils.showToast(mContext, "Some error occurred");
                    }
                    break;


                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            if (resultCode != RESULT_CANCELED) {
                if (resultCode == RESULT_OK && requestCode == VERIFY_MOBILE_NUMBER_REQUEST) {
                    if (data.hasExtra("isVerified")) {
                        String d=data.getExtras().getString("isVerified");
                        Toast.makeText(this,"Your account is verified,Please login to continue." ,
                                Toast.LENGTH_SHORT).show();


                        finish();
                    }
                }
            }
        }
        catch (Exception ex){
            //Logger.error(TAG, "onActivityResult", ex);
            Log.e(TAG, "onActivityResult: ",ex);
            ex.printStackTrace();
        }

    }*/
}
