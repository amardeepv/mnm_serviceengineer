package in.makenmake.serviceengineer.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.network.VolleyMultipartRequest;
import in.bapps.utils.ImagePickerHelper;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.ImagesTypeList;
import in.makenmake.serviceengineer.model.response.ImageUploadResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.CommonMethods;
import in.makenmake.serviceengineer.utils.EngineerPreference;

public class ImageUploadFragment extends Fragment {

    @BindView(R.id.img_checkincheckout_one) ImageView imageOne;
    @BindView(R.id.img_checkincheckout_two) ImageView imageTwo;
    @BindView(R.id.img_checkincheckout_three) ImageView imageThree;

    @BindView(R.id.btn_upload_image) Button btnSubmit;

    private static final String TAG = "ImageUploadFragment";

    private static final int REQUEST_IMAGE_ONE   = 100;
    private static final int REQUEST_IMAGE_TWO   = 200;
    private static final int REQUEST_IMAGE_THREE = 300;

    List<Bitmap> bitmapList;

    Context mContext;
    Boolean anyImage=false;

    Bitmap myBitmap;
    Uri picUri;
    private ImageUploadResponse responseNew;
    String requestBy;
    ImagesTypeList imagesTypes;


    private static final int PERMISSION_CALLBACK_CONSTANT = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;
    private static  int imageCode = 100;
    private boolean sentToSettings = false;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_image_upload, container, false);
        bitmapList=new ArrayList<>();

        requestBy=this.getArguments().getString("requestBy");


        ButterKnife.bind(this,view);

        return view;
    }

    @OnClick(R.id.btn_upload_image)
    public void onImageUploadClick(){
        if(!anyImage){
            //((MainActivity)mContext).replaceFragment(TicketMasterFragment.class.getSimpleName(),new CheckInFragment(),null,true);
            openNextView();
        }
        else{
            try {
                uploadImage();
            }catch (Exception ex){
                ex.printStackTrace();
            }
          //  ((MainActivity)mContext).replaceFragment(TicketMasterFragment.class.getSimpleName(),new CheckInFragment(),null,true);
        }
    }

    @OnClick(R.id.img_checkincheckout_one)
    public void setImageOneClick(){
        imageCode=REQUEST_IMAGE_ONE;
        getImage();
    }

    @OnClick(R.id.img_checkincheckout_two)
    public void setImageTwoClick(){
        imageCode=REQUEST_IMAGE_TWO;
        getImage();
    }

    @OnClick(R.id.img_checkincheckout_three)
    public void setImageThreeClick(){
        imageCode=REQUEST_IMAGE_THREE;
        getImage();
    }


    private void getImage(){

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Permission");
                builder.setMessage("This app needs Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (EngineerPreference.getPermissionCamera()) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Permission");
                builder.setMessage("This app needs Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity(), "Go to Permissions to Grant Camera permission", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CALLBACK_CONSTANT);
            }
            EngineerPreference.setPermissionCamera(true);

        } else {
            //You already have the permission, just go ahead.
            openImageCaptureView();
        }



    }


    private void openImageCaptureView(){
        startActivityForResult(getPickImageChooserIntent(),imageCode);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext=null;
    }




    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = mContext.getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mContext.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap bitmap;
        ImageView  imageView;
        if (resultCode == Activity.RESULT_OK) {

           if(requestCode==REQUEST_IMAGE_ONE){
                imageView=imageOne;
           }
            else if(requestCode==REQUEST_IMAGE_TWO){
               imageView=imageTwo;
           }
            else {
               imageView=imageThree;
           }

            if (getPickImageResultUri(data) != null) {
                picUri = getPickImageResultUri(data);

                try {
                    myBitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), picUri);
                    myBitmap = ImagePickerHelper.rotateImageIfRequired(myBitmap, picUri);
                    myBitmap = ImagePickerHelper.getResizedBitmap(myBitmap, 500);
                    imageView.setImageBitmap(myBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else {

                bitmap = (Bitmap) data.getExtras().get("data");
                myBitmap = bitmap;
/*                CircleImageView croppedImageView = (CircleImageView) findViewById(R.id.img_profile);
                if (croppedImageView != null) {
                    croppedImageView.setImageBitmap(myBitmap);
                }*/
                imageView.setImageBitmap(myBitmap);
            }

            bitmapList.add(myBitmap);
            anyImage=true;
            btnSubmit.setText("Upload Image");

        }


        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                openImageCaptureView();
            }
        }

    }


    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }


        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }


    private void uploadImage(){
        String url = ApiConstants.URL_ServiceTimeImageUpload;

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        ((MainActivity)mContext).removeProgressDialog();
                        String resultResponse = new String(response.data);
                        // parse success output

                        Type listType2 = new TypeToken<ImageUploadResponse>() {
                        }.getType();

                        Type listType3 = new TypeToken<ImagesTypeList>() {
                        }.getType();
                        responseNew = new Gson().fromJson(resultResponse, listType2);

                        imagesTypes=new Gson().fromJson(resultResponse, listType3);

                      /*  Toast.makeText(getActivity(), resultResponse, Toast.LENGTH_SHORT).show();*/

                        openNextView();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity)mContext).removeProgressDialog();
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
             /*   params.put("api_token", "gh659gjhvdyudo973823tt9gvjf7i6ric75r76");
                params.put("name", "Angga");
                params.put("location", "Indonesia");
                params.put("about", "UI/UX Designer");
                params.put("contact", "angga@email.com");*/
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                //params.put("file", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mAvatarImage.getDrawable()), "image/jpeg")); Map<String, DataPart> params = new HashMap<>();

                for(int i=0;i<bitmapList.size();i++){
                    byte[] featuredData = CommonMethods.getFileDataFromBitmap(bitmapList.get(i));
                    params.put("file"+i, new DataPart("file_featured.jpg", featuredData, "image/jpeg"));
                }
                return params;
            }
        };

        ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(multipartRequest, url);
        ((MainActivity)mContext).showProgressDialog("Uploading Image");
    }

    private void openNextView() {
        Bundle data=new Bundle();
        data.putParcelable("images",imagesTypes);
        data.putString("requestBy",requestBy);
        if(requestBy !=null){
            CheckInFragment checkInFragment=new CheckInFragment();
            checkInFragment.setArguments(data);
            ((MainActivity)mContext).replaceFragment(ImageUploadFragment.class.getSimpleName(),checkInFragment,null,true);
        }

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                openImageCaptureView();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Permission");
                    builder.setMessage("This app needs Camera permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, PERMISSION_CALLBACK_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getActivity(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }



    @Override
    public void onResume() {
        super.onResume();

        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                openImageCaptureView();
            }
        }
    }
}
