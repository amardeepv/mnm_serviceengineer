package in.makenmake.serviceengineer.ui.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import in.makenmake.serviceengineer.model.Ticket;
import in.makenmake.serviceengineer.ui.fragment.TicketDetailsFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketHistoryFragment;

/**
 * Created by Bucky on 11/02/2017.
 */

public class TicketMasterAdapter extends FragmentStatePagerAdapter {

    Ticket mTicket;
    int ticketStatus;

    public TicketMasterAdapter(FragmentManager fm,Ticket ticket,int ticketStatus) {
        super(fm);
        this.mTicket=ticket;
        this.ticketStatus=ticketStatus;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle data=new Bundle();
        data.putParcelable("ticket",mTicket);
        data.putInt("ticketStatus", ticketStatus);

        if (position == 0) {
            fragment = new TicketDetailsFragment();
        } else if (position == 1) {
            fragment = new TicketHistoryFragment();
        }

        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
