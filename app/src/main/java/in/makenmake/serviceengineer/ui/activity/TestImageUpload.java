package in.makenmake.serviceengineer.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

import in.bapps.application.BaseApplication;
import in.bapps.network.VolleyMultipartRequest;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.utils.CommonMethods;

public class TestImageUpload extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_image_upload);

        String url = "http://103.25.131.238/mnmapi/ServiceTimeImageUpload";

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        String resultResponse = new String(response.data);
                        // parse success output
                        Toast.makeText(TestImageUpload.this, resultResponse, Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
             /*   params.put("api_token", "gh659gjhvdyudo973823tt9gvjf7i6ric75r76");
                params.put("name", "Angga");
                params.put("location", "Indonesia");
                params.put("about", "UI/UX Designer");
                params.put("contact", "angga@email.com");*/
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                //params.put("file", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mAvatarImage.getDrawable()), "image/jpeg")); Map<String, DataPart> params = new HashMap<>();


                    byte[] featuredData = CommonMethods.getFileDataFromResource(TestImageUpload.this,R.drawable.user);
                    params.put("file", new DataPart("file_featured.jpg", featuredData, "image/jpeg"));
                return params;
            }
        };

        ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(multipartRequest, url);
    }
}
