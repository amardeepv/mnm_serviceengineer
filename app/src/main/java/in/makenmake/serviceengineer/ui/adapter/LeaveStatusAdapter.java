package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.bapps.utils.DateUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.enums.LeaveStatusString;
import in.makenmake.serviceengineer.model.LeaveStatusModel;

/**
 * Created by Bucky on 03/06/2017.
 */

public class LeaveStatusAdapter extends RecyclerView.Adapter<LeaveStatusAdapter.MyViewHolder> {
    private Context mContext;
    private List<LeaveStatusModel> leaveStatusModelList;
    public LeaveStatusAdapter.IMyViewHolderClicks mListener;

    public void setLeaveStatusList(List<LeaveStatusModel> leaveStatusModelList) {
        this.leaveStatusModelList = leaveStatusModelList;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView serviceIcon;

        public TextView type, leaveType, leave_start_date, leave_end_date, leave_status, ticket_id;
        TextView inprocess;
        ImageView img_leave_status;
        CardView cardView;

        public MyViewHolder(View view, LeaveStatusAdapter.IMyViewHolderClicks listener) {
            super(view);
            mListener = listener;

            type = (TextView) view.findViewById(R.id.txt_type);
            leaveType = (TextView) view.findViewById(R.id.txt_leave_type);
            leave_start_date = (TextView) view.findViewById(R.id.txt_start_date);
            leave_end_date = (TextView) view.findViewById(R.id.txt_end_date);
            leave_status = (TextView) view.findViewById(R.id.txt_leave_status);
            img_leave_status = (ImageView) view.findViewById(R.id.img_leave_status);


        }
    }

        public interface IMyViewHolderClicks {
        }

        public LeaveStatusAdapter(Context mContext, List<LeaveStatusModel> leaveStatusModels, LeaveStatusAdapter.IMyViewHolderClicks listener) {
            this.mContext = mContext;
            this.leaveStatusModelList = leaveStatusModels;
            this.mListener = listener;
        }

        @Override
        public LeaveStatusAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_leave_status_row, parent, false);

            return new LeaveStatusAdapter.MyViewHolder(itemView, mListener);
        }

        @Override
        public void onBindViewHolder(final LeaveStatusAdapter.MyViewHolder holder, final int position) {


            LeaveStatusModel leaveStatusModel = leaveStatusModelList.get(position);

            String status = leaveStatusModel.getStatus();
            String leaveType=leaveStatusModel.getLeaveType();
            String type=leaveStatusModel.getType();

            if(leaveType.equalsIgnoreCase("F")){
                leaveType="Full Day";
            }
            else{
                if(leaveStatusModel.getHalfLeaveType().trim().equalsIgnoreCase("FH")){
                    leaveType="1st Half";
                }
                else if(leaveStatusModel.getHalfLeaveType().trim().equalsIgnoreCase("SH")){
                    leaveType="2nd Half";
                }
                else{
                    leaveType="Half Day";
                }

            }

            if(type.equalsIgnoreCase("L")){
                type="Leave";
            }
            else{
                type="Weekly Off";
            }

            String startDate= DateUtils.getFormattedDate("yyyy-MM-dd","dd-MM-yyyy",leaveStatusModel.getLeavestartdate().substring(0,10));

            String endDate=DateUtils.getFormattedDate("yyyy-MM-dd","dd-MM-yyyy",leaveStatusModel.getLeaveEndDate().substring(0,10));


            holder.type.setText(type);
            holder.leaveType.setText(leaveType);
            holder.leave_start_date.setText(startDate);
            holder.leave_end_date.setText(endDate);
            holder.leave_status.setText(leaveStatusModel.getStatus());



            if (status.equalsIgnoreCase(LeaveStatusString.ACCEPTED)) {
                Glide.with(mContext).load(R.drawable.checked_green_sixty_four_px).into(holder.img_leave_status);
            } else if (status.equalsIgnoreCase(LeaveStatusString.REJECTED)) {
                Glide.with(mContext).load(R.drawable.cancel_red_sixty_four_px).into(holder.img_leave_status);
            } else {
                Glide.with(mContext).load(R.drawable.pending_processing_sixty_four_px).into(holder.img_leave_status);

            }


        }

        @Override
        public int getItemCount() {
            return leaveStatusModelList.size();
        }


}