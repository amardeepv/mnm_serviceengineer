package in.makenmake.serviceengineer.ui.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;

import butterknife.ButterKnife;
import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.DateUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.constants.enums.EngineerStatus;
import in.makenmake.serviceengineer.model.request.EngineerLocation;
import in.makenmake.serviceengineer.model.request.MarkAttendance;
import in.makenmake.serviceengineer.services.LocationUpdateService;
import in.makenmake.serviceengineer.ui.fragment.AccountFragment;
import in.makenmake.serviceengineer.ui.fragment.ApplyLeaveFragment;
import in.makenmake.serviceengineer.ui.fragment.CheckInFragment;
import in.makenmake.serviceengineer.ui.fragment.CompletedTicketFragment;
import in.makenmake.serviceengineer.ui.fragment.JobCardFragment;
import in.makenmake.serviceengineer.ui.fragment.LeaveStatusFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketDetailsFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketFeedbackFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketHistoryFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketListFragment;
import in.makenmake.serviceengineer.ui.fragment.TicketMasterFragment;
import in.makenmake.serviceengineer.utils.EngineerPreference;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TicketListFragment.OnFragmentInteractionListener,
        TicketMasterFragment.OnFragmentInteractionListener,
        UpdateJsonListener.onUpdateViewJsonListener,
        CheckInFragment.OnTicketStatusChange,
        TicketDetailsFragment.OnTicketStatusChange,
        JobCardFragment.OnTicketStatusChange{

    private static final String TAG = "MainActivity";

    DrawerLayout drawer;
    Toolbar toolbar;
    public double latitude;
    public double longitude;


    private TicketListFragment fragment = null;
    private FragmentManager manager = null;
    private FragmentTransaction ft;
    private boolean isAvailable;
    private NavigationView navView;
    private View headerView;
    TextView availabilityStatus=null;
    SwitchCompat toggleAvailbility;


    private static final int PERMISSION_CALLBACK_CONSTANT = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;
    private boolean sentToSettings=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



/*        manager=getSupportFragmentManager();

        fragment = new TicketListFragment();
        ft= manager.beginTransaction();
        ft.add(R.id.content_frame,fragment).commit();*/


       navView = (NavigationView) findViewById(R.id.nav_view);
       headerView = navView.getHeaderView(0);

        if (headerView != null) {

            availabilityStatus = (TextView) headerView.findViewById(R.id.txt_availability);

             toggleAvailbility = (SwitchCompat) headerView.findViewById(R.id.switchButton);
            if(EngineerPreference.getInstance().getORIGIN().equals("I")){
                toggleAvailbility.setVisibility(View.GONE);
                        availabilityStatus.setVisibility(View.GONE);
            }


            toggleAvailbility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    updateAvailabilityStatus(isChecked);
                }
            });

            ImageView userImage= (ImageView) headerView.findViewById(R.id.img_user_image);
         //   Glide.with(this).load(ApiConstants.URL_BASE+EngineerPreference.getImagePath()).into(userImage);
            Picasso.with(this).load(ApiConstants.URL_BASE+EngineerPreference.getImagePath())
                    .placeholder(R.drawable.logo)
                    .error(R.drawable.logo)
                    .into(userImage);

            TextView userName= (TextView) headerView.findViewById(R.id.txt_user_name);
            TextView mobileNumber= (TextView) headerView.findViewById(R.id.txt_mobile_number);
            userName.setText(EngineerPreference.getFullName());
            mobileNumber.setText(EngineerPreference.getMobileNumber());

        }



        replaceFragment(null, new TicketListFragment(), null, false);
      //  replaceFragment(null, new JobCardFragment(), null, false);


      //  startLocationListner();

        if(!isServiceRunning(LocationUpdateService.class)){
            startLocationServices();

        }else {

        }
        updateDrawerMenuItems();
    }

    private void startLocationServices() {

        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs access gps location.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_CALLBACK_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else if (EngineerPreference.getPermissionCamera()) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs access gps location.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                            Toast.makeText(MainActivity.this, "Go to Permissions to Grant Location permission", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA}, PERMISSION_CALLBACK_CONSTANT);
                }
                EngineerPreference.setPermissionLocation(true);
            } else {
                Intent intent = new Intent(this, LocationUpdateService.class);
                intent.setAction(ApiConstants.ACTION.STARTFOREGROUND_ACTION);



                startService(intent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateAvailabilityStatus(boolean isChecked) {
        if (isChecked) {

            availabilityStatus.setText("Available");
            availabilityStatus.setTextColor(Color.WHITE);

            isAvailable = true;
            updateEngineerAvailabilityStatus(EngineerStatus.AVAILABILE);

        } else {
            if(EngineerPreference.getTicketInProgress()!=null){
                toggleAvailbility.setChecked(true);
                ToastUtils.showToast(this,"You can't change status while working on a ticket.");
                return;
            }
            isAvailable = false;
            availabilityStatus.setText("Unavailable");
            availabilityStatus.setTextColor(Color.RED);
            updateEngineerAvailabilityStatus(EngineerStatus.ON_LEAVE);
        }
    }

    private void updateDrawerMenuItems() {
        Menu nav_Menu = navView.getMenu();
        if(EngineerPreference.getORIGIN().equalsIgnoreCase("I")){
            nav_Menu.findItem(R.id.nav_earning).setVisible(false);
        }
        else {
            nav_Menu.findItem(R.id.nav_leaveApply).setVisible(false);
        }
    }

    private void startLocationListner() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationTracker tracker = new LocationTracker(this,
                new TrackerSettings()
                        .setUseGPS(true)
                        .setUseNetwork(true)
                        .setUsePassive(true)
                        .setTimeBetweenUpdates(10 * 1000)
                        .setMetersBetweenUpdates(10)) {


            public double localLongitude;
            public double localLatitude;

            @Override
            public void onLocationFound(Location location) {
                // Toast.makeText(MainActivity.this,"hey your location : latitude is :"+location.getLatitude()+" and longitude is : "+location.getLongitude(),Toast.LENGTH_SHORT).show();
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                this.localLatitude = location.getLatitude();
                this.localLongitude = location.getLongitude();
                if (localLongitude != location.getLongitude() || localLatitude != location.getLatitude()) {
                    hitApiRequest(ApiConstants.REQUEST_UPDATE_ENG_LOC);
                }
            }

            @Override
            public void onTimeout() {

            }
        };
        tracker.startListening();


    }



    // Custom method to determine whether a service is running
    private boolean isServiceRunning(Class<?> serviceClass){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        // Loop through the running services
        for(ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                // If the service is running then return true
                return true;
            }
        }
        return false;
    }


    private void updateEngineerAvailabilityStatus(EngineerStatus userStatus) {
        //TODO : update status locally and server
        //TODO : start or stop location services
        //TODO : start or stop ticket services

        EngineerPreference.getInstance().setEngineerStatus(userStatus.toString());
        hitApiRequest(ApiConstants.REQUEST_EX_ENG_AVBL);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void openTicketListView(FragmentManager fragmentManager){

        int count=fragmentManager.getBackStackEntryCount();

        while(count>0){
            fragmentManager.popBackStack();
            count--;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            // Handle the Account Details
            openAccountSettingView();

        } /*else if (id == R.id.nav_availability) {
            //Handle availability action
            updateAvailability();

        } */else if (id == R.id.nav_leaveApply) {
            //Handle Apply Leave option
            openLeaveApplyView();

        } else if (id == R.id.nav_earning) {
            //show earning details
            openEarningView();

        } else if (id == R.id.nav_feedback) {
            //show ticket feedback
            openFeedbackView();

        }  else if (id == R.id.nav_history) {
            //show ticket feedback
            openHistoryView();

        }else if (id == R.id.nav_complete_ticket) {
            //show ticket feedback
            openCompleteTicketView();

        }
        else if (id == R.id.nav_leaveStatus) {
            //show ticket feedback
            openLeaveStatusView();

        }else if (id == R.id.nav_notification) {
            //view notification
            openNotificationView();

        } else if (id == R.id.nav_terms_conditions) {
            //view terms and conditions
            openTermsAndConditionsView();

        } else if (id == R.id.nav_rateApp) {
            //open Rate App
            rateApp();
        }
        else if (id == R.id.nav_home) {
            openHomeView();
        }else if (id == R.id.nav_logout) {
            //open Rate App
            onLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openLeaveStatusView() {
        replaceFragment(null, new LeaveStatusFragment(), null, true);
    }

    private void openCompleteTicketView() {
        replaceFragment(null, new CompletedTicketFragment(), null, true);
    }

    private void openHomeView() {
        openTicketListView(getSupportFragmentManager());
    }

    private void openHistoryView() {
        replaceFragment(null, new TicketHistoryFragment(), null, true);
    }

    public void onLogout() {
        EngineerPreference.getInstance().OnUserLogut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
        startActivity(intent);
        stopLocationService();
        finish();
    }

    private void stopLocationService(){
        stopService(new Intent(this,LocationUpdateService.class));
    }


    private void openAccountSettingView() {
        replaceFragment(null, new AccountFragment(), null, true);
    }

    private void updateAvailability() {
        ToastUtils.showToast(this, "Coming Soon updateAvailbility view", Toast.LENGTH_LONG);
    }

    private void openLeaveApplyView() {
        //  ToastUtils.showToast(this,"Coming Soon Leave Apply view", Toast.LENGTH_LONG);

        replaceFragment(null, new ApplyLeaveFragment(), null, true);
    }

    private void openEarningView() {
        ToastUtils.showToast(this, "Coming Soon Earning view", Toast.LENGTH_LONG);
    }

    private void openFeedbackView() {
       // ToastUtils.showToast(this, "Coming Soon Feedback view", Toast.LENGTH_LONG);
        replaceFragment(null, new TicketFeedbackFragment(), null, true);
    }

    private void openNotificationView() {
        ToastUtils.showToast(this, "Coming Soon Notification view", Toast.LENGTH_LONG);

    }

    private void openTermsAndConditionsView() {
        ToastUtils.showToast(this, "Coming Soon Terms and Conditions view", Toast.LENGTH_LONG);
    }

    private void rateApp() {
        ToastUtils.showToast(this, "Coming Soon", Toast.LENGTH_LONG);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyJsonRequest jsonRequest = null;
        VolleyStringRequest request = null;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_EX_ENG_AVBL:
                String data=EngineerPreference.getInstance().getUserId()+"&Status="+(isAvailable==false?0:1);
                url = ApiConstants.URL_EX_ENG_AVBL+data;
                className=CommonJsonResponseNew.class;
                  request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType,className) {
                    }, new HashMap<String, String>());
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

                break;

            case ApiConstants.REQUEST_UPDATE_ENG_LOC:
                url = ApiConstants.URL_ADD_ENG_LOCATION;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType) {
                    }, getJSON(reqType));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getJSON(int reqType) {
        String json = "";
        Gson gson = new Gson();
        Type type;
        switch (reqType) {
            case ApiConstants.REQUEST_MARK_ATTENDANCE:

                MarkAttendance markAttendance = new MarkAttendance(
                        DateUtils.getFormattedDate(new Date(), "yyyy-MM-dd hh:mm:ss"),                   //2017-03-16T07:53:27.548Z
                        EngineerPreference.getInstance().getUserId()
                );
                type = new TypeToken<MarkAttendance>() {
                }.getType();
                json = gson.toJson(markAttendance, type);
                break;

            case ApiConstants.REQUEST_UPDATE_ENG_LOC:

                EngineerLocation location = new EngineerLocation(
                        EngineerPreference.getInstance().getUserId(),
                        longitude,
                        latitude
                );


                type = new TypeToken<EngineerLocation>() {
                }.getType();
                json = gson.toJson(location, type);
                break;
            default:

        }

        return json;
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

        removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_MARK_ATTENDANCE:
                    //   responseObject = new Gson().fromJson(responseString, AddFolderFeeResponse.class);

                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);
                    if (!responseNew.message.equals("")) {
                        ToastUtils.showToast(this, responseNew.message);
                    }
                    break;

                case ApiConstants.REQUEST_UPDATE_ENG_LOC:
                    //   responseObject = new Gson().fromJson(responseString, AddFolderFeeResponse.class);

                    Type listType3 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> locationRes = new Gson().fromJson(responseString, listType3);
                    if (!locationRes.message.equals("")) {
                        // ToastUtils.showToast(this,locationRes.message);
                        Log.i(TAG, "updateView: " + locationRes.message);
                    }
                    break;

                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();

                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                if(!responseNew.message.equals("")) {
                    ToastUtils.showToast(this, responseNew.message);
                }
                else {
                    ToastUtils.showToast(this, "Some error occured.");
                }
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_EX_ENG_AVBL:

                    //         responseObject = new Gson().fromJson(responseString, SkillListResponse.class);

                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);

                    Toast.makeText(this,responseNew.message,Toast.LENGTH_LONG).show();

                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy() called");
        stopLocationService();
        super.onDestroy();
    }

    @Override
    public void ticketStatusChange() {

        try {
            FragmentManager manager=getSupportFragmentManager();
            TicketListFragment fragment= (TicketListFragment) manager.findFragmentByTag(TicketListFragment.class.getSimpleName());
            fragment.onLoadTicketList();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
