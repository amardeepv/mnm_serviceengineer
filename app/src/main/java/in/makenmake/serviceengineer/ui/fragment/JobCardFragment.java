package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.SparePartCartModel;
import in.makenmake.serviceengineer.model.request.AddJobCardRequest;
import in.makenmake.serviceengineer.model.response.SparePartTypeModel;
import in.makenmake.serviceengineer.model.response.SparePartsNameModel;
import in.makenmake.serviceengineer.model.response.SparePartsNameResponse;
import in.makenmake.serviceengineer.model.response.SparePartsTypeResponse;
import in.makenmake.serviceengineer.model.response.TicketAmountResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.SparePartCartAdapter;
import in.makenmake.serviceengineer.ui.dialog.AddSparePartDialog;
import in.makenmake.serviceengineer.utils.EngineerPreference;


public class JobCardFragment extends BaseFragment implements
        UpdateJsonListener.onUpdateViewJsonListener,
        SparePartCartAdapter.IMyViewHolderClicks,
        AddSparePartDialog.AddSparePartListener{

    private static final String TAG = "JobCardFragment";
    Context mContext;

    @BindView(R.id.spSparePartType) Spinner spSparePartType;
    @BindView(R.id.spSparePartName) Spinner spSparePartName;
    @BindView(R.id.txtTicketAmount) TextView txtTicketAmount;
    @BindView(R.id.txtAdvancePayment) TextView txtAdvancePayment;
    @BindView(R.id.txtDueAmount) TextView txtDueAmount;
    @BindView(R.id.txtTicketId) TextView txtTicketId;
    @BindView(R.id.txtSparePartAmount) TextView txtSparePartAmount;

    @BindView(R.id.edtConveyanceDistance)   EditText edtConveyanceDistance;
    @BindView(R.id.edtConvenienceCharges) EditText edtConvenienceCharges;
    @BindView(R.id.txtTotalDueAmount) TextView txtTotalDueAmount;
    @BindView(R.id.txtConveyanceCharge) TextView txtConveyanceCharge;
    @BindView(R.id.edtTotalAmountPaid) EditText edtTotalAmountPaid;
    @BindView(R.id.edtRemark) EditText edtRemark;

    @BindView(R.id.btnSubmit)  Button btnSubmit;
    @BindView(R.id.btnAddSparePartView)  Button btnAddSparePartView;
    @BindView(R.id.btnAddSparePart)  Button btnAddSparePart;
    @BindView(R.id.txtAddManually)  TextView txtAddManually;
    @BindView(R.id.card_sparePart)CardView card_sparePart;
    @BindView(R.id.rview_sparePart_list)RecyclerView rview_sparePart_list;

    private int selectedSpareType;

    private List<String> spareTypeNames=new ArrayList<>();
    private List<SparePartTypeModel> sparePartTypeModelList=new ArrayList<>();
    int selectedSparePartTypePosition =1;

    private List<String> spareNames=new ArrayList<>();
    private List<SparePartsNameModel> sparePartsNameModelList=new ArrayList<>();
    int selectedSparePartsNamePosition =0;

    private List<SparePartCartModel> sparePartCartModelList=new ArrayList<>();
    private SparePartCartAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setHeader() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_job_card, container, false);

        ButterKnife.bind(this,view);
        hitApiRequest(ApiConstants.REQUEST_JOB_CARD);

        ((MainActivity)mContext).setTitle("Job Card");



        adapter = new SparePartCartAdapter(mContext, sparePartCartModelList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rview_sparePart_list.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new AlbumActivity.GridSpacingItemDecoration(2, dpToPx(10), true));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        rview_sparePart_list.setItemAnimator(itemAnimator);
        rview_sparePart_list.setAdapter(adapter);


        edtConvenienceCharges.addTextChangedListener(convenienceWatcher);
        edtConveyanceDistance.addTextChangedListener(conveyanceWatcher);


        txtTicketId.setText(Integer.toString(EngineerPreference.getTicketInProgress().getTicketId()));

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext=null;
    }

    @OnClick(R.id.btnSubmit)
    public void onConfirmPayment(){
        if(validateData()){
            hitApiRequest(ApiConstants.REQUEST_ADD_JOB_CARD);
        }
    }

    @OnClick(R.id.btnAddSparePartView)
    public void btnAddSparePartView(){
        btnAddSparePartView.setVisibility(View.GONE);
        card_sparePart.setVisibility(View.VISIBLE);
        hitApiRequest(ApiConstants.REQUEST_SPAREPART_TYPE);

    }
    @OnClick(R.id.btnAddSparePart)
    public void onAddSparePartClick(){
        if(sparePartsNameModelList.size()>0) {
            SparePartsNameModel sparePartsNameModel=sparePartsNameModelList.get(spSparePartName.getSelectedItemPosition());
            if(sparePartsNameModel == null){
                return;
            }
            SparePartCartModel sparePartCartModel =new SparePartCartModel();

            sparePartCartModel.setSparePartTypeModel(sparePartTypeModelList.get(selectedSparePartTypePosition));
            sparePartCartModel.setSparePartName(sparePartsNameModel.getSparePartName());
            sparePartCartModel.setSparePartAmount(sparePartsNameModel.getSellingPrice());
            sparePartCartModel.setQty(1);
            sparePartCartModel.setSellingPrice(sparePartsNameModel.getSellingPrice());
            sparePartCartModel.setMrpPrice(sparePartsNameModel.getMRPPrice());
            sparePartCartModel.setManuallyAdded(false);

            addSparePartToCart(sparePartCartModel);
        }
    }

    @OnClick(R.id.txtAddManually)
    public void onAddManuallyClick(){
       ToastUtils.showToast(mContext,"Add Manually clicked");
        AddSparePartDialog addSparePartDialog=new AddSparePartDialog();
        addSparePartDialog.setData(spareTypeNames,sparePartTypeModelList,this);
        addSparePartDialog.show(getFragmentManager(),"AddSparePartDialog");
    }

    @OnItemSelected(R.id.spSparePartType)
    public void onSparePartTypeSelection(Spinner spinner, int position) {
        selectedSparePartTypePosition=position;
        hitApiRequest(ApiConstants.REQUEST_SPAREPART_NAME);
    }

    private boolean validateData() {
        /*if(edtConvenienceCharges.getText().toString().equals("")){
            ToastUtils.showToast(mContext,"Please enter convenience charges.");
            return false;
        }
        if(edtConveyanceDistance.getText().toString().equals("")){
            ToastUtils.showToast(mContext,"Please enter conveyance charges.");
            return false;
        }*/
        if(edtTotalAmountPaid.getText().toString().equals("")){
            ToastUtils.showToast(mContext,"Please enter total amount paid.");
            return false;
        }

        return true;
    }

    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest = null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_JOB_CARD:
                ((MainActivity) mContext).showProgressDialog("Get Ticket Amount ...");
                url = ApiConstants.URL_GET_TICKET_AMOUNT+ EngineerPreference.getInstance().getSelectedTicket().getTicketId();
                className= CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {
                });
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            case ApiConstants.REQUEST_ADD_JOB_CARD:
                ((MainActivity) mContext).showProgressDialog("Adding Ticket Amount ...");
                url = ApiConstants.URL_ADD_JOB_CARD;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getJobCardJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            case ApiConstants.REQUEST_SPAREPART_TYPE:
                ((MainActivity) mContext).showProgressDialog("Getting spare part types...");
                className= CommonJsonResponseNew.class;
                url = ApiConstants.URL_GET_SPAREPART_TYPE;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {
                });

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            case ApiConstants.REQUEST_SPAREPART_NAME:
                 ((MainActivity) mContext).showProgressDialog("Getting spare parts names...");
                className= CommonJsonResponseNew.class;
                url = ApiConstants.URL_GET_SPAREPART_NAME+ selectedSparePartTypePosition;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {
                });

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getJobCardJson() {

        AddJobCardRequest request=new AddJobCardRequest();

        String paymentStatus="";
        String totalAmountPaidText=edtTotalAmountPaid.getText().toString();
        float totalAmountPaid=totalAmountPaidText.equals("")?0:Float.parseFloat(totalAmountPaidText);

        if(totalAmountPaid==getTotalDueAmount()){
            paymentStatus="Paid";
        }
        else if(totalAmountPaid<=0){
            paymentStatus="UnPaid";
        }
        else{
            paymentStatus="UnderPaid";
        }


        request.setTickeId(EngineerPreference.getInstance().getSelectedTicket().getTicketId());
        request.setEngineerId(EngineerPreference.getInstance().getUserId());
        request.setAmountRecived(Float.parseFloat(edtTotalAmountPaid.getText().toString()));
        request.setConvcyanceCharge(getConveyanceAmount());
        request.setConvenienceCharges(getConvenienceAmount());
        request.setTicketPaymentStatus(paymentStatus);
        request.setRemarks(edtRemark.getText().toString());
   //     request.setDueAmount(Integer.parseInt(txtDueAmount.getText().toString()));
        request.setDueAmount(getTotalDueAmount());
        request.setSpareParts(sparePartCartModelList);
        request.setTicketAmount(getTicketAmount());

        Type type=new TypeToken<AddJobCardRequest>(){}.getType();

        String json=new Gson().toJson(request,type);

        return json;
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity) mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if (!response.message.equals("")) {
                    ToastUtils.showToast(mContext, response.message);
                } else {
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            String responseObject = null;
            Type type;
            CommonJsonResponseNew<String> response;
            switch (reqType) {
                case ApiConstants.REQUEST_ADD_JOB_CARD:

                    type = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response2 = new Gson().fromJson(responseString, type);
                    ToastUtils.showToast(mContext, response2.message);
                    ((MainActivity)mContext).ticketStatusChange();
                    ((MainActivity)mContext).openTicketListView(getFragmentManager());
                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity) mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();

                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                if(!responseNew.message.equals("")) {
                    ToastUtils.showToast(mContext, responseNew.message);
                }
                else {
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            Type type;
            switch (reqType) {
                case ApiConstants.REQUEST_SPAREPART_TYPE:

                     type = new TypeToken<CommonJsonResponseNew<SparePartsTypeResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<SparePartsTypeResponse> response = new Gson().fromJson(new Gson().toJson(responseObject), type);

                    sparePartTypeModelList=response.data.getSpareTypes();
                    bindSparePartTypeSpinner(sparePartTypeModelList);
                    break;

                case ApiConstants.REQUEST_SPAREPART_NAME:

                    type = new TypeToken<CommonJsonResponseNew<SparePartsNameResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<SparePartsNameResponse> responseSpareName = new Gson().fromJson(new Gson().toJson(responseObject), type);

                    List<SparePartsNameModel> sparePartsNameModelListLocal=responseSpareName.data.getSpareParts();

                    for (SparePartsNameModel sparePartsNameModel:sparePartsNameModelListLocal) {
                        if(sparePartsNameModel.getStatus() !=0){
                            sparePartsNameModelList.add(sparePartsNameModel);
                        }
                    }

                    bindSparePartNameSpinner(sparePartsNameModelList);
                    break;

                case ApiConstants.REQUEST_JOB_CARD:

                    type = new TypeToken<CommonJsonResponseNew<TicketAmountResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<TicketAmountResponse> response2 = new Gson().fromJson(new Gson().toJson(responseObject), type);
                    ToastUtils.showToast(mContext, response2.message);
                    updateFieldValues(response2.data);
                    break;

                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void bindSparePartNameSpinner(List<SparePartsNameModel> sparePartsNameModelList) {

        for (SparePartsNameModel sparePartsNameModel : sparePartsNameModelList) {
            spareNames.add(sparePartsNameModel.getSparePartName());
        }

        if(spareNames.size()==0){
            ToastUtils.showToast(mContext,"No spare part found for this category.");
            return;
        }

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.single_row_dropdown, R.id.txt_single_row_item, spareNames);
        spSparePartName.setAdapter(adapter);

    }

    private void bindSparePartTypeSpinner(List<SparePartTypeModel> sparePartTypeResponse) {
        spareTypeNames=new ArrayList<>();
        for(SparePartTypeModel sparePartTypeModel:sparePartTypeResponse){
            spareTypeNames.add(sparePartTypeModel.getSpareTypeName());
        }

        ArrayAdapter adapter=new ArrayAdapter(mContext,R.layout.single_row_dropdown,R.id.txt_single_row_item,spareTypeNames);
        spSparePartType.setAdapter(adapter);
        hitApiRequest(ApiConstants.REQUEST_SPAREPART_NAME);

    }

    private void updateFieldValues(TicketAmountResponse data) {

        txtTicketAmount.setText(Integer.toString(data.getTicketamount()));
        txtAdvancePayment.setText(Integer.toString(data.getAdvancePaid()));
        txtDueAmount.setText(Integer.toString(data.getDuePayment()));



    }

    @Override
    public void onRemoveItemClick(final int position) {
        new AlertDialog.Builder(mContext)
                .setTitle("Remove Spare Part")
                .setMessage("Do you sure want to remove spare part?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(mContext, "Spare part removed.", Toast.LENGTH_SHORT).show();
                        sparePartCartModelList.remove(position);
                        updateTotalDueAmount();
                        updateSparePartCartAdapter();
                    }})
                .setNegativeButton(android.R.string.no, null).show();

    }

    @Override
    public void onPlusItemClick(int position) {
        in.makenmake.serviceengineer.model.SparePartCartModel sparePartCartModel = sparePartCartModelList.get(position);
        sparePartCartModel.setQty(sparePartCartModel.getQty() + 1);
        sparePartCartModelList.set(position, sparePartCartModel);
        updateTotalDueAmount();
        updateSparePartCartAdapter();

    }

    @Override
    public void onMinusItemClick(int position) {
        in.makenmake.serviceengineer.model.SparePartCartModel sparePartCartModel = sparePartCartModelList.get(position);

        int quantity = sparePartCartModel.getQty();

        if (quantity > 1) {
            sparePartCartModel.setQty(sparePartCartModel.getQty() - 1);
            sparePartCartModelList.set(position, sparePartCartModel);
            updateTotalDueAmount();
            updateSparePartCartAdapter();
        }
    }

    @Override
    public void onAddManuallySparePart(in.makenmake.serviceengineer.model.SparePartCartModel sparePartCartModel) {
        addSparePartToCart(sparePartCartModel);
    }

    public interface OnTicketStatusChange {
        void ticketStatusChange();
    }

    private void addSparePartToCart(SparePartCartModel sparePartCartModel){
        sparePartCartModelList.add(sparePartCartModel);
        updateSparePartCartAdapter();
        updateTotalDueAmount();
    }

   /* private void scrollToBottom(){
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }*/

    private void updateSparePartCartAdapter() {
            adapter.setTicketList(sparePartCartModelList);
            adapter.notifyDataSetChanged();
    }


    private final TextWatcher conveyanceWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // textView.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {

            if(getConveyanceAmount()>=0){
                updateTotalDueAmount();
                txtConveyanceCharge.setText(Float.toString(getConveyanceAmount()));
            }

        }
    };

    private final TextWatcher convenienceWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // textView.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {

            if(getConvenienceAmount()>=0){
                updateTotalDueAmount();
            }
        }
    };


    private Float getTotalDueAmount(){
        String totalDueAmountText= txtTotalDueAmount.getText().toString();
        return totalDueAmountText.equalsIgnoreCase("")?0:Float.parseFloat(totalDueAmountText);
    }
    private Float getConvenienceAmount(){
        String convenienceText= edtConvenienceCharges.getText().toString();
       return convenienceText.equalsIgnoreCase("")?0:Float.parseFloat(convenienceText);
    }
    private Float getConveyanceAmount(){
        String conveyanceText= edtConveyanceDistance.getText().toString();
        return conveyanceText.equalsIgnoreCase("")?0:Float.parseFloat(conveyanceText)*3;
    }
    private Float getTicketAmount() {
        String ticketAmountText= txtTicketAmount.getText().toString();
        return ticketAmountText.equalsIgnoreCase("")?0:Float.parseFloat(ticketAmountText);
    }

    private void updateTotalDueAmount(){

        float amount=getConvenienceAmount()+getConveyanceAmount()+ calculateUpdateSparePartAmount()+getTicketAmount();
        txtTotalDueAmount.setText(Float.toString(amount));
    }



    private Float calculateUpdateSparePartAmount(){
        float amount=0f;
        for (SparePartCartModel sparePartCartModel :sparePartCartModelList) {
            amount+=sparePartCartModel.getSellingPrice()*sparePartCartModel.getQty();
        }

        txtSparePartAmount.setText(Float.toString(amount));
        return amount;
    }



}
