package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.constants.enums.TicketStatus;
import in.makenmake.serviceengineer.constants.enums.TicketStatusString;
import in.makenmake.serviceengineer.model.Address;
import in.makenmake.serviceengineer.model.Ticket;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

public class TicketDetailsFragment extends Fragment implements UpdateJsonListener.onUpdateViewJsonListener {

    @BindView(R.id.txtTicketId_ticketDetailView) TextView txtTicketId;
    @BindView(R.id.txtTicketDataTime) TextView txtTicketDataTime;
    @BindView(R.id.txtTicketTypeName) TextView txtTicketTypeName;
    @BindView(R.id.txtTicketCategoryName) TextView txtTicketCategoryName;
    @BindView(R.id.txtTicketStatus) TextView txtTicketStatus;
    @BindView(R.id.txtTicketDiscription) TextView txtTicketDiscription;

    @BindView(R.id.txtClientName) TextView txtClientName;
    @BindView(R.id.txtClientContactNumber) TextView txtClientContactNumber;
    @BindView(R.id.imgCallCustomerCare) ImageView imgCallCustomerCare;
    @BindView(R.id.txtClientAddress) TextView txtClientAddress;

    @BindView(R.id.img_map_link) ImageView img_map_link;
    @BindView(R.id.btn_ticket_reject) Button btn_ticket_reject;
    @BindView(R.id.btn_ticket_check_in) Button btn_ticket_check_in;
    @BindView(R.id.btn_ticket_check_out) Button btn_ticket_check_out;
    @BindView(R.id.btn_ticket_accept) Button btn_ticket_accept;
    @BindView(R.id.btn_ticket_pause) Button btn_ticket_pause;
    @BindView(R.id.btn_ticket_complete) Button btn_ticket_complete;

    private Context mContext;
    private Ticket mTicket;
    private int ticketStatus;

    private static final int PERMISSION_CALLBACK_CONSTANT = 101;
    private static final int REQUEST_PERMISSION_SETTING = 102;

    private boolean sentToSettings = false;

    public TicketDetailsFragment() {
        // Required empty public constructor
    }


    public static TicketDetailsFragment newInstance(String param1, String param2) {
        TicketDetailsFragment fragment = new TicketDetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_ticket_details, container, false);
        ButterKnife.bind(this,view);
        /*
        mTicket = this.getArguments().getParcelable("ticket");
        ticketStatus=this.getArguments().getInt("ticketStatus");
        */
        mTicket = EngineerPreference.getInstance().getSelectedTicket();
       // ticketStatus=mTicket.getTicketStatus();

       /* if(ticketStatus== TicketStatus.accepted){
            updateTicketStatus();
        }*/

        setData();
        updateButton();
        return view;
    }

    private void updateTicketStatus() {
        hitApiRequest(ApiConstants.REQUEST_UPDATE_TICKET_STATUS);
        ((MainActivity)mContext).updateLocationBaseActivity();
    }

    private void updateButton(){

        // remove this button
        btn_ticket_check_out.setVisibility(View.GONE);

        if(mTicket.getTicketStatus().equalsIgnoreCase(TicketStatusString.assigned)){
            /*
            btn_ticket_accept.setVisibility(View.VISIBLE);
            btn_ticket_reject.setVisibility(View.VISIBLE);*/
            btn_ticket_check_in.setVisibility(View.GONE);
            btn_ticket_complete.setVisibility(View.GONE);
            btn_ticket_pause.setVisibility(View.GONE);


        }
        else if(mTicket.getTicketStatus().equalsIgnoreCase(TicketStatusString.accepted)){
            btn_ticket_accept.setVisibility(View.GONE);
            btn_ticket_check_in.setVisibility(View.VISIBLE);
            btn_ticket_reject.setVisibility(View.VISIBLE);

            btn_ticket_complete.setVisibility(View.GONE);
            btn_ticket_pause.setVisibility(View.GONE);
        }
        else if(mTicket.getTicketStatus().equalsIgnoreCase(TicketStatusString.in_progress)){
            if(!mTicket.isCheckedOut()){
                btn_ticket_accept.setVisibility(View.GONE);
                btn_ticket_check_in.setVisibility(View.GONE);
                btn_ticket_reject.setVisibility(View.GONE);
                btn_ticket_complete.setVisibility(View.VISIBLE);
                btn_ticket_pause.setVisibility(View.VISIBLE);

            }
            else{
                btn_ticket_accept.setVisibility(View.GONE);
                btn_ticket_check_in.setVisibility(View.VISIBLE);
                btn_ticket_reject.setVisibility(View.GONE);
                btn_ticket_complete.setVisibility(View.GONE);
                btn_ticket_pause.setVisibility(View.GONE);
                btn_ticket_check_out.setVisibility(View.GONE);
            }

        }
        else {
            ToastUtils.showToast(mContext,"for DevTeam : Error this ticket status is not handled");
        }
    }

    private void setData() {
        txtTicketId.setText(Integer.toString(mTicket.getTicketId()));
        txtTicketDataTime.setText(mTicket.getTimeSlot());
        txtTicketTypeName.setText(mTicket.getConsumerServices().getServiceName());
        txtTicketCategoryName.setText(mTicket.getConsumerServices().getServiceCategory());
        txtTicketStatus.setText(mTicket.getTicketStatus());
        txtTicketDiscription.setText(mTicket.getDescription());

        txtClientContactNumber.setText(mTicket.getConsumerDetails().getMobileNumber());
        txtClientName.setText(mTicket.getConsumerDetails().getCustomerName());
        txtClientAddress.setText(mTicket.getConsumerDetails().getConsumerAddress().getAddress());
    }

    @OnClick(R.id.btn_ticket_accept)
    public void onClickAcceptTicket(){

        ticketStatus=TicketStatus.accepted;
        mTicket.setTicketStatus("accepted");
        updateTicketStatus();


    }

    @OnClick({R.id.imgCallCustomerCare,R.id.txtClientContactNumber})
    public void onCallCustomerNumber(){
        if(txtClientContactNumber.getText().toString().equals("")){
            ToastUtils.showToast(mContext,"invalid mobile number");
            return;
        }


        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CALL_PHONE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Permission");
                builder.setMessage("This app needs phone permission to call.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (EngineerPreference.getPermissionCallPhone()) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Permission");
                builder.setMessage("This app needs phone permission to call.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity(), "Go to Permissions to Grant Phone Call permission", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALLBACK_CONSTANT);
            }
            EngineerPreference.setPermissionCallPhone(true);

        } else {
            //You already have the permission, just go ahead.
            callMobileNumber();
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT){
            //check if all permissions are granted


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                callMobileNumber();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CALL_PHONE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Permission");
                    builder.setMessage("This app needs phone permission to call.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALLBACK_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getActivity(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                callMobileNumber();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                callMobileNumber();
            }
        }
    }



    private void callMobileNumber() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+txtClientContactNumber.getText().toString()));
        startActivity(callIntent);
    }

    @OnClick(R.id.btn_ticket_reject)
    public void onClickRejectTicket(){
        ticketStatus= TicketStatus.reject;
        mTicket.setTicketStatus("rejected");
        updateTicketStatus();

    }

    @OnClick(R.id.btn_ticket_check_in)
    public void onClickTicketCheckIn(){

        if(EngineerPreference.getTicketInProgress() != null){
            ToastUtils.showToast(mContext,"You can't check in while working on a ticket.");
            return;
        }

        ImageUploadFragment imageUploadFragment=new ImageUploadFragment();
        Bundle bundle=new Bundle();
        bundle.putString("requestBy","checkIn");
        imageUploadFragment.setArguments(bundle);
        ((MainActivity)mContext).replaceFragment(TicketDetailsFragment.class.getSimpleName(),imageUploadFragment,null,true);
    }

    @OnClick(R.id.btn_ticket_pause)
    public void onTcketPauseClick(){
        ImageUploadFragment imageUploadFragment=new ImageUploadFragment();
        Bundle bundle=new Bundle();
        bundle.putString("requestBy","pause");
        imageUploadFragment.setArguments(bundle);
        ((MainActivity)mContext).replaceFragment(TicketDetailsFragment.class.getSimpleName(),imageUploadFragment,null,true);
    }

    @OnClick(R.id.btn_ticket_complete)
    public void onTicketCompleteClick(){
        ImageUploadFragment imageUploadFragment=new ImageUploadFragment();
        Bundle bundle=new Bundle();
        bundle.putString("requestBy","complete");
        imageUploadFragment.setArguments(bundle);
        ((MainActivity)mContext).replaceFragment(TicketDetailsFragment.class.getSimpleName(),imageUploadFragment,null,true);

    }


    @OnClick(R.id.btn_ticket_check_out)
    public void onClickTicketCheckOut(){
        ImageUploadFragment imageUploadFragment=new ImageUploadFragment();
        Bundle bundle=new Bundle();
        bundle.putString("requestBy","checkOut");
        imageUploadFragment.setArguments(bundle);
        ((MainActivity)mContext).replaceFragment(TicketDetailsFragment.class.getSimpleName(),imageUploadFragment,null,true);
    }

    @OnClick(R.id.img_map_link)
    public void onOpenMapLinkClick(){
        MapFragment mapFragment=new MapFragment();
        Address address=mTicket.getConsumerDetails().getConsumerAddress();
        Bundle data=new Bundle();

        data.putDouble("latitude",address.getLatitude());
        data.putDouble("longitude",address.getLongitutde());

        mapFragment.setArguments(data);
        ((MainActivity)mContext).replaceFragment(TicketDetailsFragment.class.getSimpleName(),mapFragment,null,true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_UPDATE_TICKET_STATUS:
                ((MainActivity)mContext).showProgressDialog("Updating Ticket Status...");
                url = ApiConstants.URL_UPDATE_TICKET_STATUS + mTicket.getTicketId() + "&status=" + ticketStatus;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }
    private String getJson(){
        /*TicketStatusRequest ticketStatusRequest=new TicketStatusRequest(
                mTicket.getTicketId(),
                ticketStatus
        );
        Gson gson = new Gson();
        Type type = new TypeToken<TicketStatusRequest>() {
        }.getType();
        String json = gson.toJson(ticketStatusRequest, type);
*/
        return "{}";

    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_UPDATE_TICKET_STATUS:


                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                     ToastUtils.showToast(mContext,response.message);
                    handleResponse();
                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void handleResponse() {

        ((MainActivity)mContext).openTicketListView(getFragmentManager());
        ((MainActivity)mContext).ticketStatusChange();
    }


    public interface OnTicketStatusChange{
        void ticketStatusChange();
    }


    public interface OnTicketDetailFragmentInteractionListener{
        void openMapLink(double lat,double logn);
    }

}
