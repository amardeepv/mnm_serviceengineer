package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.AccountModel;

/**
 * Created by Bucky on 27/03/2017.
 */

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.MyViewHolder>{
    private Context mContext;
    private List<AccountModel> accountModelList;
    public AccountAdapter.IMyViewHolderClicks mListener;

    public void setAccountModelList(List<AccountModel> accountModelList) {
        this.accountModelList = accountModelList;
    }
    public AccountAdapter(Context mContext, List<AccountModel> accountModelList, AccountAdapter.IMyViewHolderClicks listener) {
        this.mContext = mContext;
        this.accountModelList = accountModelList;
        this.mListener=listener;
    }

    public AccountAdapter(Context mContext, List<AccountModel> accountModelList) {
        this.mContext = mContext;
        this.accountModelList = accountModelList;

    }


    public  class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView itemImage;

        public TextView itemId,itemName;

        public MyViewHolder(View view,AccountAdapter.IMyViewHolderClicks listener) {
            super(view);
            mListener=listener;

            itemName = (TextView) view.findViewById(R.id.txt_itemName_account);
            itemImage= (ImageView) view.findViewById(R.id.img_itemImage_account);


        }



    }
    public static interface IMyViewHolderClicks {
        public void onAccountItemClick(int postion);

    }



    @Override
    public AccountAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_account, parent, false);


        return new AccountAdapter.MyViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final AccountAdapter.MyViewHolder holder, final int position) {


        AccountModel accountModel= accountModelList.get(position);

        Glide.with(mContext).load(accountModel.getImage()).into(holder.itemImage);
        holder.itemName.setText(accountModel.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onAccountItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return accountModelList.size();
    }



    /*public void swap(ArrayList<Data> datas){
        data.clear();
        data.addAll(datas);
        notifyDataSetChanged();
    }*/

    // to handle empty data set

    /*public void swap(List list){
        if (mFeedsList != null) {
            mFeedsList.clear();
            mFeedsList.addAll(list);
        }
        else {
            mFeedsList = list;
        }
        notifyDataSetChanged();
    }*/
}
