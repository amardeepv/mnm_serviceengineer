package in.makenmake.serviceengineer.ui.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.constants.enums.TicketStatus;
import in.makenmake.serviceengineer.model.request.CheckInCheckOutRequest;
import in.makenmake.serviceengineer.model.request.EngineerSignOutRequest;
import in.makenmake.serviceengineer.model.request.ImagesType;
import in.makenmake.serviceengineer.model.request.ImagesTypeList;
import in.makenmake.serviceengineer.model.request.Wages;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInFragment extends BaseFragment implements UpdateJsonListener.onUpdateViewJsonListener,UpdateListener.onUpdateViewListener {

    @BindView(R.id.rbtn_viaOtp)
    RadioButton rbtn_viaOtp;
    @BindView(R.id.rbtn_withoutOtp)
    RadioButton rbtn_withoutOtp;
    @BindView(R.id.edt_otp_checkIn)
    EditText edtOtp;
    @BindView(R.id.edt_permissionProviderPerson)
    EditText edt_permissionProviderPerson;
    @BindView(R.id.edt_estimatedTime)
    EditText edt_estimatedTime;
    @BindView(R.id.edt_workDiscription)
    EditText edt_workDiscription;
    @BindView(R.id.btnCheckIn_checkInDetails)
    Button btnCheckIn_checkInDetails;
    @BindView(R.id.linearLayout_wages)
    LinearLayout linearLayout_wages;
    @BindView(R.id.btn_add_wages)
    Button btn_add_wages;
    @BindView(R.id.edt_reason)
    EditText edt_reason;
    @BindView(R.id.edt_NextDate)
    EditText edt_Next_date;
    @BindView(R.id.edt_wages1)
    EditText edt_wages1;
    @BindView(R.id.radioGroup_checkIn)
    RadioGroup radioGroup_checkIn;
    @BindView(R.id.txt_label_radio_group)
    TextView txt_label_radio_group;
    @BindView(R.id.txt_generateOtp)
    Button txt_generateOtp;

    @BindView(R.id.linearLayout_checkInCheckout)
    LinearLayout linearLayout_checkInCheckout;

    @BindView(R.id.linearLayout_pause)
    LinearLayout linearLayout_pause;

    @BindView(R.id.txt_anything_to_keep) TextView txt_anything_to_keep;

    private DatePickerDialog datePickerDialog;
    String myFormat = "dd/MM/yyyy";
    String nextDate;
    ImagesTypeList imagesTypeList;
    Context mContext;
    private String requestBy;

    List<EditText> listWages;

    public CheckInFragment() {
        // Required empty public constructor
    }

    @Override
    public void setHeader() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_in, container, false);
        ButterKnife.bind(this, view);
        listWages = new ArrayList<>();

        try {
            imagesTypeList = this.getArguments().getParcelable("images");
            requestBy = this.getArguments().getString("requestBy");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(requestBy !=null){
            if(requestBy.equals("checkIn")){
                txt_label_radio_group.setText("Select Type of Check In");
            }
        }
        updateUI();
        return view;
    }

    private void updateUI() {
        if (!requestBy.equals("checkIn")) {

            edt_permissionProviderPerson.setVisibility(View.GONE);
            edt_estimatedTime.setVisibility(View.GONE);
            //  edt_workDiscription.setHint("Check Out Description");
        }
        if (requestBy.equals("complete")) {
            /*edtOtp.setVisibility(View.GONE);
            edt_permissionProviderPerson.setVisibility(View.GONE);
            edt_estimatedTime.setVisibility(View.GONE);
            edt_workDiscription.setHint("Check Out Description");*/

            linearLayout_pause.setVisibility(View.GONE);
        } else if (requestBy.equals("pause")) {
            linearLayout_pause.setVisibility(View.VISIBLE);
        }


    }


    @OnClick(R.id.rbtn_viaOtp)
    public void onViaOtpRadioButtonClick() {
        edtOtp.setVisibility(View.VISIBLE);
        edt_permissionProviderPerson.setVisibility(View.GONE);
        txt_generateOtp.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.rbtn_withoutOtp)
    public void onWithoutOtpRadioButtonClick() {
        edtOtp.setVisibility(View.GONE);
        edt_permissionProviderPerson.setVisibility(View.VISIBLE);
        txt_generateOtp.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnCheckIn_checkInDetails)
    public void onCheckInButtonClick() {
        if (validateData()) {
            if (rbtn_viaOtp.isChecked()) {
                  hitApiRequest(ApiConstants.REQUEST_CHECK_IN_OTP_VERF);
            } else {
                hitApiRequest(ApiConstants.REQUEST_CHECK_IN);
                ((MainActivity)mContext).updateLocationBaseActivity();
            }
        }
    }

    @OnClick(R.id.txt_anything_to_keep)
    public void OnAnythingToKeepClick(){
        btn_add_wages.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_generateOtp)
    public void onGenerateOtp(){
        hitApiRequest(ApiConstants.REQUEST_GENERATE_OTP);
        txt_generateOtp.setText("Resend Otp");
    }

    @OnClick(R.id.edt_NextDate)
    public void OnNextDateEditTextClick() {
        selectDate(edt_Next_date);
    }

    public void selectDate(final EditText edtView) {

        final Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                edtView.setText(sdf.format(myCalendar.getTime()));
                nextDate =new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(myCalendar.getTime());


            }

        };


        DatePickerDialog dialog = new DatePickerDialog(mContext, onDateSetListener,
                myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar maxD = Calendar.getInstance();
        maxD.setTime(new Date());
        maxD.add(Calendar.DATE, 90);
        // user is required atleast tell 2 day before leave : issue mm-6
        long twoDay = 1000 * 60 * 60 * 24 * 2;

        maxD.add(Calendar.DATE, 90);

        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        dialog.getDatePicker().setMaxDate(maxD.getTime().getTime());

        dialog.show();

    }

    @OnClick(R.id.btn_add_wages)
    public void onPlusClick() {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                android.widget.LinearLayout.LayoutParams.MATCH_PARENT,
                android.widget.LinearLayout.LayoutParams.WRAP_CONTENT);

        EditText edttext = new EditText(mContext);
        listWages.add(edttext);
        edttext.setHint("Item Name "+listWages.size());
        edttext.setLayoutParams(params);
        linearLayout_wages.addView(edttext);
    }


    private boolean validateData() {

        if (requestBy.equals("checkIn")) {
            if (rbtn_viaOtp.isChecked()) {
                if (edtOtp.getText().toString().equals("")) {
                    ToastUtils.showToast(mContext, "Please enter OTP.");
                    return false;
                }
            }

            if (edt_estimatedTime.getText().toString().equals("")) {
                ToastUtils.showToast(mContext, "Please enter Estimated Time.");
                return false;
            }
        }
        if (requestBy.equalsIgnoreCase("complete")) {

        }

        if (requestBy.equalsIgnoreCase("pause")) {
            if (rbtn_viaOtp.isChecked()) {
                if (edtOtp.getText().toString().equals("")) {
                    ToastUtils.showToast(mContext, "Please enter OTP.");
                    return false;
                }
            }

            if(nextDate==null){
                ToastUtils.showToast(mContext, "Please enter Next Date.");
                return false;
            }
        }

        return true;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }


    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest = null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHECK_IN:
                ((MainActivity) mContext).showProgressDialog("Checking In...");
                url = ApiConstants.URL_CHECKIN_CHECKOUT;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;
            case ApiConstants.REQUEST_ENGINEER_SIGNOUT:
                ((MainActivity) mContext).showProgressDialog("Signing out...");
                url = ApiConstants.URL_ENGINEER_SIGNOUT;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getEngineerSignOutJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            case ApiConstants.REQUEST_GENERATE_OTP:
                ((MainActivity) mContext).showProgressDialog("Sending Otp to customer...");
                className= CommonJsonResponseNew.class;
                url = ApiConstants.URL_GENERATE_OTP+EngineerPreference.getInstance().getSelectedTicket().getTicketId();
                    request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {
                    });

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_CHECK_IN_OTP_VERF:
                ((MainActivity) mContext).showProgressDialog("Verifying Otp ...");
                int ticketId=EngineerPreference.getInstance().getSelectedTicket().getTicketId();
                String otp=edtOtp.getText().toString();
                url = ApiConstants.URL_VERIFY_CHECKIN_OTP+ticketId+"&OTP="+otp;
                className= CommonJsonResponseNew.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType,className) {
                },new HashMap<String,String>());

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;


            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getEngineerSignOutJson() {
        String json = "";
        EngineerSignOutRequest request = new EngineerSignOutRequest();

        if (requestBy.equalsIgnoreCase("complete")) {
            request.setTicketStatus(TicketStatus.completed);
            request.setReason("");
            request.setTicketNextDate("");
            request.setTicketId(EngineerPreference.getInstance().getSelectedTicket().getTicketId());
            request.setWages(new ArrayList<Wages>());
        } else if (requestBy.equalsIgnoreCase("pause")) {
            request.setTicketStatus(TicketStatus.in_progress);
            request.setReason(edt_reason.getText().toString());
            request.setTicketNextDate(nextDate);
            request.setTicketId(EngineerPreference.getInstance().getSelectedTicket().getTicketId());

            List<Wages> data = new ArrayList<>();
            Wages wages = new Wages();
            wages.setWagesName(edt_wages1.getText().toString());
            data.add(wages);

            for (EditText editText : listWages) {
                wages = new Wages();
                wages.setWagesName(editText.getText().toString());
                data.add(wages);
            }

            request.setWages(data);
        }

        Type type = new TypeToken<EngineerSignOutRequest>() {
        }.getType();
        json = new Gson().toJson(request, type);

        return json;
    }

    private String getJson() {
        String json = "{}";
        CheckInCheckOutRequest request = new CheckInCheckOutRequest();
        request.setTicketId(EngineerPreference.getInstance().getSelectedTicket().getTicketId());
        request.setImagesType(imagesTypeList!=null?imagesTypeList.getImageList():new ArrayList<ImagesType>());
        if (requestBy.equals("checkIn")) {
            request.setServiceFrom(getFormatedTime());
            request.setEstimateTime(edt_estimatedTime.getText().toString());
            request.setWorkDescCheckIn(edt_workDiscription.getText().toString());

            request.setServiceTo("");
            request.setWorkDescCheckOut("");
        } else {
            request.setServiceFrom("");
            request.setEstimateTime("");
            request.setWorkDescCheckIn("");

            request.setServiceTo(getFormatedTime());
            request.setWorkDescCheckOut(edt_workDiscription.getText().toString());
        }

        Gson gson = new Gson();
        Type type = new TypeToken<CheckInCheckOutRequest>() {
        }.getType();
        json = gson.toJson(request, type);

        return json;
    }

    private String getFormatedTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss", Locale.US);

        return sdf.format(new Date());
    }


    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity) mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if (!response.message.equals("")) {
                    ToastUtils.showToast(mContext, response.message);
                } else {
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            String responseObject = null;
            Type type;
            CommonJsonResponseNew<String> response;
            switch (reqType) {
                case ApiConstants.REQUEST_CHECK_IN:

                    type = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    response = new Gson().fromJson(responseString, type);
                    ToastUtils.showToast(mContext, response.message);

                    openNextView();
                    break;

                case ApiConstants.REQUEST_CHECK_IN_OTP_VERF:

                    type = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    response = new Gson().fromJson(responseString, type);
                    ToastUtils.showToast(mContext, response.message);


                    hitApiRequest(ApiConstants.REQUEST_CHECK_IN);
                    ((MainActivity)mContext).updateLocationBaseActivity();

                    break;
                case ApiConstants.REQUEST_ENGINEER_SIGNOUT:

                    type = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    response = new Gson().fromJson(responseString, type);
                    ToastUtils.showToast(mContext, response.message);


                    handleSignOutResponse();

                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity) mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();

                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                if(!responseNew.message.equals("")) {
                    ToastUtils.showToast(mContext, responseNew.message);
                }
                else {
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GENERATE_OTP:

                   Type type = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response = new Gson().fromJson(new Gson().toJson(responseObject), type);
                    ToastUtils.showToast(mContext, response.message);
                    break;

                case ApiConstants.REQUEST_CHECK_IN_OTP_VERF:

                    Type type2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response2 = new Gson().fromJson(new Gson().toJson(responseObject), type2);
                    ToastUtils.showToast(mContext, response2.message);


                    hitApiRequest(ApiConstants.REQUEST_CHECK_IN);
                    ((MainActivity)mContext).updateLocationBaseActivity();

                    break;

                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void handleSignOutResponse() {
        if (requestBy.equals("pause")) {
            ((MainActivity)mContext).openTicketListView(getFragmentManager());
            ((MainActivity)mContext).ticketStatusChange();
            return;
        } else if (requestBy.equals("complete")) {
            ToastUtils.showToast(mContext, "open JOB Card View");
            ((MainActivity) mContext).replaceFragment(CheckInFragment.class.getSimpleName(), new JobCardFragment(), null, true);
        }
    }

    private void openNextView() {
        if (requestBy.equals("checkIn")) {
           /* getFragmentManager().popBackStack();
            getFragmentManager().popBackStack();*/
            ((MainActivity)mContext).openTicketListView(getFragmentManager());
            ((MainActivity)mContext).ticketStatusChange();

        } else if (requestBy.equals("pause")) {
            updateStatusOfTicket();

        } else if (requestBy.equals("complete")) {
            updateStatusOfTicket();
        }

    }

   /* private void openSignOutView() {
        updateStatusOfTicket();
    }


    private void openCompleteView() {
        updateStatusOfTicket();
    }*/

    public interface OnTicketStatusChange {
        void ticketStatusChange();
    }

    private void updateStatusOfTicket() {
        hitApiRequest(ApiConstants.REQUEST_ENGINEER_SIGNOUT);
    }




}
