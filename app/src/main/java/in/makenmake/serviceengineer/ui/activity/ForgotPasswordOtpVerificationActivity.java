package in.makenmake.serviceengineer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.StringUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;

public class ForgotPasswordOtpVerificationActivity extends BaseActivity {

    @BindView(R.id.edt_otp_forgotPass)
    EditText edt_otp_forgotPass;

    @BindView(R.id.edt_pass_conf_forgotPass)
    EditText edt_pass_conf_forgotPass;

    @BindView(R.id.edt_pass_forgotPass)
    EditText edt_pass_forgotPass;
    @BindView(R.id.txt_resend_otp_forgotPassword)
    TextView txt_resend_otp_forgotPassword;

    @BindView(R.id.btn_submit_forgotPass)
    Button btn_submit_forgotPass;

    @BindView(R.id.img_cancel_forgotPass)
    ImageView img_cancel_forgotPass;

    private String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_otp_verification);

        ButterKnife.bind(this);
        mobileNumber = getIntent().getStringExtra("mobileNumber");
        String requestBy = getIntent().getStringExtra("requestBy");

        if(requestBy != null){
            onResendOtpClick();
        }
    }


    @OnClick(R.id.btn_submit_forgotPass)
    public void onSubmitButtonClick() {

        if (validateData()) {
            hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD_VERIFY);
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edt_pass_forgotPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter otp.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edt_pass_forgotPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter new password.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edt_pass_conf_forgotPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter confirm password.");
            return false;
        } else if (!(edt_pass_forgotPass.getText().toString().equals(edt_pass_conf_forgotPass.getText().toString()))) {
            ToastUtils.showToast(this, "Password does not match.");
            return false;
        }
        return true;
    }

    @OnClick(R.id.img_cancel_forgotPass)
    public void OnCancelButtonClick() {
        finish();
    }


    @OnClick(R.id.txt_resend_otp_forgotPassword)
    public void onResendOtpClick() {
        hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD);
    }

    @Override
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest = null;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_FORGOT_PASSWORD_VERIFY:
                showProgressDialog("Verifying otp...");
                url = ApiConstants.URL_FORGOT_PASSWORD_OTP + mobileNumber + "&OTP=" + edt_otp_forgotPass.getText().toString() + "&password=" + edt_pass_forgotPass.getText().toString();
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_FORGOT_PASSWORD:
                showProgressDialog("Sending otp...");
                url = ApiConstants.URL_FORGOT_PASSWORD_MOBILE + mobileNumber;
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                CommonJsonResponseNew<String> response = (CommonJsonResponseNew<String>) responseObject;
                if (!response.message.equals("")) {
                    ToastUtils.showToast(this, response.message);
                } else {
                    ToastUtils.showToast(this, "Some error occured.");
                }
                return;
            }

            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD_VERIFY:
                    CommonJsonResponseNew<String> response = (CommonJsonResponseNew<String>) responseObject;

                    Toast.makeText(this, response.message, Toast.LENGTH_SHORT).show();

                    if (response.hasError == false) {
                        closeActivity();
                    }
                   /* if (changePasswordResponse.getStatus() > 0) {
                        Bundle bunle = new Bundle();
                        bunle.putString(AppConstants.EXTRA_PHONE_NO, entMobileNo.getText().toString());
                        bunle.putBoolean(AppConstants.FROM_TROUBLE, false);
                        ((BaseActivity) getActivity()).replaceFragment(EnterMobileFragment.class.getSimpleName(), new ForgotPasswordOtpFragment(), bunle, true);
                    } else {
                        ToastUtils.showToast(this, "Wrong number");
                    }*/
                    break;
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    CommonJsonResponseNew<String> response1 = (CommonJsonResponseNew<String>) responseObject;

                    Toast.makeText(this, response1.message, Toast.LENGTH_SHORT).show();

                /*if(response.hasError==false){
                    Intent intent=new Intent(this,ForgotPasswordOtpVerificationActivity.class);
                    intent.putExtra("mobileNumber",entMobileNo.getText().toString());
                    startActivity(intent);
                }*/
                    break;
                default:

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void closeActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


}
