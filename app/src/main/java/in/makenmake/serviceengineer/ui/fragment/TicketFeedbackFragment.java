package in.makenmake.serviceengineer.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.AddFeedbackRequest;
import in.makenmake.serviceengineer.model.response.CompleteTicketListResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

public class TicketFeedbackFragment extends Fragment implements
        UpdateListener.onUpdateViewListener,
        UpdateJsonListener.onUpdateViewJsonListener {


    @BindView(R.id.ratingFeedback)
    RatingBar ratingFeedback;
    @BindView(R.id.edtFeedbackMessage)
    EditText edtFeedbackMessage;
    @BindView(R.id.btnSubmitFeedback)
    Button btnSubmitFeedback;

    Context mContext;
  /*  private Spinner spin;*/
    String ticket;
    List<String> completeTicketList = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ticket_feedback, container, false);
        ButterKnife.bind(this, view);


        ticket=this.getArguments().getString("ticket");

        if(ticket.equals("")){
            ((MainActivity)mContext).onBackPressed();
        }

        TextView txt_ticketId=(TextView)view.findViewById(R.id.txt_ticket_id_feedback);

        txt_ticketId.setText("Selected ticket No. : "+ticket);


        hitApiRequest(ApiConstants.REQUEST_TICKET_COMPLETE);
       /* spin = (Spinner) view.findViewById(R.id.spinner);*/

        /*
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {

                ticket = String.valueOf(spin.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        */


        ratingFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @OnClick(R.id.btnSubmitFeedback)
    public void onSubmitFeedback() {
        if (ticket.equals("Please select the ticket")) {
            ToastUtils.showToast(mContext,"Please select the ticket");
            return;
        }
       /* if (completeTicketList.size()==0) {
            ToastUtils.showToast(mContext,"You don't have any complete ticket.");
            return;
        }*/
        hitApiRequest(ApiConstants.REQUEST_ADD_FEEDBACK);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
       ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest = null;
        String url;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_TICKET_COMPLETE:
                url = ApiConstants.URL_GET_COMPLETE_TICKET + EngineerPreference.getInstance().getUserId();
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_ADD_FEEDBACK:
                url = ApiConstants.URL_ADD_FEEDBACK;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getJSON());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getJSON() {
        String json = "";
        int feedbackRating = Math.round(ratingFeedback.getRating())*2;
        String msg = edtFeedbackMessage.getText().toString();
        //int rating, String feedbackMsg, int ticketID
        AddFeedbackRequest request = new AddFeedbackRequest(feedbackRating, msg, Integer.parseInt(ticket));

        Type type = new TypeToken<AddFeedbackRequest>() {
        }.getType();

        json = new Gson().toJson(request, type);
        return json;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response =( CommonJsonResponseNew<String>)responseObject ;
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_TICKET_COMPLETE:
                    Type listTicket = new TypeToken<CommonJsonResponseNew<CompleteTicketListResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<CompleteTicketListResponse> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listTicket);
                   /* setSpinnerUi(responseNew.data);*/
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

        ((BaseActivity) mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                ToastUtils.showToast(mContext, responseNew.message);
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_ADD_FEEDBACK:
                    //   responseObject = new Gson().fromJson(responseString, AddFolderFeeResponse.class);

                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);
                    if (!responseNew.message.equals("")) {
                        ToastUtils.showToast(mContext, responseNew.message);
                    }

                    ratingFeedback.setRating(0);
                    edtFeedbackMessage.setText("");
                    ((MainActivity)mContext).onBackPressed();
                    break;

                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /*
    private void setSpinnerUi(CompleteTicketListResponse response) {
        //checkNumberResponse.add();
        completeTicketList.add("Please select the ticket");
        for (CompleteTicket list : response.getCompletedList()) {
            completeTicketList.add(list.getT() + "");
        }
        spin.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, completeTicketList));

    }
    */
}
