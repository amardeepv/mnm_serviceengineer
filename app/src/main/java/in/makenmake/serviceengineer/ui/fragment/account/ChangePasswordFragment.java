package in.makenmake.serviceengineer.ui.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.ChangePasswordRequest;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class ChangePasswordFragment extends Fragment implements UpdateListener.onUpdateViewListener {

    @BindView(R.id.edtOldPassword) EditText edtOldPassword;
    @BindView(R.id.edtNewPassword) EditText edtNewPassword;
    @BindView(R.id.edtNewConfPassword) EditText edtNewConfPassword;
    @BindView(R.id.btn_submit_chan_pass) Button btn_submit_chan_pass;
    @BindView(R.id.btn_cancel_chan_pass) Button btn_cancel_chan_pass;
    @BindView(R.id.input_layout_old_pass) TextInputLayout input_layout_old_pass;
    @BindView(R.id.input_layout_new_pass) TextInputLayout input_layout_new_pass;
    @BindView(R.id.input_layout_conf_pass) TextInputLayout input_layout_conf_pass;




    Context mContext;
    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    public static ChangePasswordFragment newInstance(String param1, String param2) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_change_password, container, false);

        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @OnClick(R.id.btn_submit_chan_pass)
    public void OnSubmitButtonClick(){

        if(validData()){
            hitApiRequest(ApiConstants.REQUEST_CHANGE_PASSWORD);
        }
    }

    @OnClick(R.id.btn_cancel_chan_pass)
    public void OnCancelButtonClick(){
        ((MainActivity)mContext).removeFragmentFromBackStack(this);
    }


    private boolean validData(){
        removeAllErrorMessage();

        if(edtOldPassword.getText().toString()==null ||edtOldPassword.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter old password.");
            input_layout_old_pass.setError("Please enter old password.");
            requestFocus(edtOldPassword);
            return false;
        }

        if(edtNewPassword.getText().toString()==null ||edtNewPassword.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter new password.");
            input_layout_new_pass.setError("Please enter new password.");
            requestFocus(edtNewPassword);
            return false;
        }

        if(edtNewConfPassword.getText().toString()==null ||edtNewConfPassword.getText().toString().equals("")) {
            ToastUtils.showToast(mContext,"Please enter confirm password.");
            input_layout_conf_pass.setError("Please enter confirm password.");
            requestFocus(edtNewConfPassword);
            return false;
        }

        if(!edtNewConfPassword.getText().toString().equals(edtNewPassword.getText().toString())) {
            ToastUtils.showToast(mContext,"Confirm password not match.");
            input_layout_conf_pass.setError("Confirm password not match.");
            requestFocus(edtNewConfPassword);
            return false;
        }

        return true;
    }

    private void removeAllErrorMessage(){
        input_layout_old_pass.setError(null);
        input_layout_new_pass.setError(null);
        input_layout_conf_pass.setError(null);



    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            ((MainActivity)mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHANGE_PASSWORD:
                ((MainActivity)mContext).showProgressDialog();
                url = ApiConstants.URL_CHANGE_PASSWORD;
                className=CommonJsonResponseNew.class;
                    request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType,className) {
                    }, getParams(reqType));

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

            params.put("userId", EngineerPreference.getInstance().getUserId()+"");
            params.put("oldPassword", edtOldPassword.getText().toString());
            params.put("newPassword", edtNewPassword.getText().toString());

        return params;
    }

    private String getJson(){

        ChangePasswordRequest request=new ChangePasswordRequest(
                EngineerPreference.getInstance().getUserId(),
                edtOldPassword.getText().toString(),
                edtNewPassword.getText().toString()
        );


        Gson gson = new Gson();
        Type type = new TypeToken<ChangePasswordRequest>() {
        }.getType();
        String json = gson.toJson(request, type);

        return json;

    }

    //<editor-fold desc=" commented block">

/*    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_CHANGE_PASSWORD:
                    clearAllFiled();
                    ToastUtils.showToast(mContext,"Password Changed Successfully. Please login again to continue.");
                    ((MainActivity)mContext).openLoginActivity();

                   *//* Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> response = new Gson().fromJson(responseString, listType2);

                    if(response.message.equals("Data Added Successfully")){
                        ToastUtils.showToast(mContext,"Leave applied successfully");
                        clearAllFiled();
                        ((MainActivity)mContext).removeFragmentFromBackStack(this);
                    }*//*

                   *//* if(responseString=="Account  created"){
                        ToastUtils.showToast(mContext,"Please verify your mobile number");
                       // startActivityForResult(new Intent(mContext,OtpVerificationActivity.class),VERIFY_MOBILE_NUMBER_REQUEST);
                    }
                    else{
                        ToastUtils.showToast(mContext,"Some error occurred");
                    }*//*
                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }*/

    //</editor-fold>

    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();

                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                if(!responseNew.message.equals("")) {
                    ToastUtils.showToast(mContext, responseNew.message);
                }
                else {
                    ToastUtils.showToast(mContext, getString(R.string.some_error_occured));
                }
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_CHANGE_PASSWORD:

                    clearAllFiled();
                    ToastUtils.showToast(mContext,"Password Changed Successfully. Please login again to continue.");
                    ((MainActivity)mContext).onLogout();


                    break;
                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void clearAllFiled() {
        edtOldPassword.setText("");
        edtNewPassword.setText("");
        edtNewConfPassword.setText("");
    }

}
