package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.TicketHistoryList;
import in.makenmake.serviceengineer.model.response.CompleteTicketListResponse;
import in.makenmake.serviceengineer.model.response.TicketHistoryDetail;
import in.makenmake.serviceengineer.model.response.TicketHistoryDetailResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.TicketHistoryDetailAdapter;
import in.makenmake.serviceengineer.utils.EngineerPreference;


public class TicketHistoryFragment extends BaseFragment {

    private String url;
    List<Integer> comTicketList;
    CompleteTicketListResponse ticket_list;
    List<String> completeTicketList = new ArrayList<String>();
 /*   Spinner spin;*/
    private RecyclerView listview;
    private TicketHistoryDetailAdapter ticketListAdapter;
    List<TicketHistoryDetail> ticketDetail;
    Context mContext;

    TicketHistoryList ticketHistoryList;

    String ticket;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setHeader() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_ticket_history, container, false);
       // ButterKnife.bind(this,view);
        try {

            ticket= this.getArguments().getString("ticket");

            TextView txt_ticketId=(TextView)view.findViewById(R.id.txt_ticket_id_history);

            txt_ticketId.setText("Selected ticket No. : "+ticket);


            hitApiRequest(ApiConstants.REQUEST_TICKET_HISTORY_BY_ID);

            comTicketList=new ArrayList<>();
            ticketDetail=new ArrayList<>();

            listview = (RecyclerView) view.findViewById(R.id.listview);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            listview.setLayoutManager(linearLayoutManager);

            ticketListAdapter=new TicketHistoryDetailAdapter(mContext,ticketDetail);


            listview.setAdapter(ticketListAdapter);
            /*spin = (Spinner) view.findViewById(R.id.spinner);

            hitApiRequest(ApiConstants.REQUEST_TICKET_COMPLETE);

            spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {

                    ticket = String.valueOf(spin.getSelectedItem());

                    if (!ticket.equals("Please select the ticket")) {
                        hitApiRequest(ApiConstants.REQUEST_TICKET_HISTORY_BY_ID);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }
            });

            */
        } catch (Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_TICKET_COMPLETE:
                url = ApiConstants.URL_GET_COMPLETE_TICKET + EngineerPreference.getInstance().getUserId();
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_TICKET_HISTORY_BY_ID:
                url = ApiConstants.URL_GET_TICKET_HISTORY + ticket;
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response =( CommonJsonResponseNew<String>)responseObject ;
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occurred.");
                }
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_TICKET_COMPLETE:
                    Type listTicket = new TypeToken<CommonJsonResponseNew<CompleteTicketListResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<CompleteTicketListResponse> responseNew= new Gson().fromJson(new Gson().toJson(responseObject), listTicket);
                 /*   setSpinnerUi(responseNew.data);*/

                    break;
                case ApiConstants.REQUEST_TICKET_HISTORY_BY_ID:
                    Type listType = new TypeToken<CommonJsonResponseNew<TicketHistoryDetailResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<TicketHistoryDetailResponse> ticketDetailList = new Gson().fromJson(new Gson().toJson(responseObject), listType);

                    ticketDetail=ticketDetailList.data.getHistoryList();
                    updateTicketDetailAdapter();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateTicketDetailAdapter() {
        ticketListAdapter.setListData(ticketDetail);
        ticketListAdapter.notifyDataSetChanged();
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


/*    private void setSpinnerUi(CompleteTicketListResponse response) {
        //checkNumberResponse.add();
        completeTicketList.add("Please select the ticket");
        for (CompletedList list:response.getCompletedList()) {
            completeTicketList.add(list.getTicketId()+ "");
        }
        spin.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, completeTicketList));

    }*/

}
