package in.makenmake.serviceengineer.ui.activity;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;

import in.makenmake.serviceengineer.ui.fragment.intro.FirstFragment;
import in.makenmake.serviceengineer.ui.fragment.intro.SeconedFragment;
import in.makenmake.serviceengineer.ui.fragment.intro.ThirdFragment;

public class IntroActivity extends AppIntro implements FirstFragment.OnFragmentInteractionListener,
                                                        SeconedFragment.OnFragmentInteractionListener,
                                                        ThirdFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
        addSlide(new FirstFragment());
        addSlide(new SeconedFragment());
        addSlide(new ThirdFragment());


        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest.
        //addSlide(AppIntroFragment.newInstance(title, description, image, backgroundColor));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));

        // Hide Skip/Done button.
        showSkipButton(false);
        setProgressButtonEnabled(false);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(true);
        setVibrateIntensity(30);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
