package in.makenmake.serviceengineer.ui.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.map.utils.DataParser;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.Ticket;

public class MapFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "MapFragment";

    private GoogleMap mMap;
    Context mContext;
    private MapView mMapView;
    private Ticket mTicket;

    Marker sourceMarker;
    Marker destinationMarker;
    List<Integer> distance;
    private Polyline line;
    private ArrayList<LatLng> MarkerPoints;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    LinearLayout linearLayout;

    private static final int REQUEST_CODE_AUTOCOMPLETE_Source = 1;
    private static final int REQUEST_CODE_AUTOCOMPLETE_destination = 2;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 3;
    private LatLng latLngClientLocatio;

    @BindView(R.id.txtDistance) TextView txtDistnace;
    @BindView(R.id.btnDirection) Button btnDirection;

    public MapFragment() {
        // Required empty public constructor
    }

    public static MapFragment newInstance(String param1, String param2) {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        ButterKnife.bind(this,view);

        // Initializing
        MarkerPoints = new ArrayList<>();


        /*// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) ((MainActivity)mContext).getSupportFragmentManager()
                .findFragmentById(R.id.map_engineer_location);*/

        mMapView = (MapView) view.findViewById(R.id.map_engineer_location);
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);
        //   mMapView.getMapAsync(this);
        mMapView.onCreate(savedInstanceState);


        mMapView.getMapAsync(this);


       // latLngClientLocatio=new LatLng(this.getArguments().getDouble("latitude"),this.getArguments().getDouble("longitude"));
        latLngClientLocatio = new LatLng(28.429504, 77.066500);






/*
        // Gets to GoogleMap from the MapView and does initialization stuff
        mMap = mMapView.getMapAsync();
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
        mMap.animateCamera(cameraUpdate);

*/


        return view;
    }

    @OnClick(R.id.btnDirection)
    public void OnDirectionButtonClick(){
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", latLngClientLocatio.latitude, latLngClientLocatio.longitude, "Where the party is at");
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(mContext, "Please install Google maps application for direction.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMapView.onDestroy();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiClient();
        enableMyLocation();

        addLocationOfClient();


        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                //  processNewLatLng(point);


            }
        });

    }

    private void addLocationOfClient() {
        if (latLngClientLocatio != null) {
            processNewLatLng(latLngClientLocatio, REQUEST_CODE_AUTOCOMPLETE_destination);
        } else {
            ToastUtils.showToast(mContext, "Some error");
        }
    }


    private void processNewLatLng(LatLng latLng, int requestCode) {

    /*    // no marker is added to map
        if(MarkerPoints.size()==0){
            MarkerPoints.add(latLng);
            setMarker(latLng,requestCode);
        }
        else if(MarkerPoints.size()==1){
            if(requestCode==REQUEST_CODE_AUTOCOMPLETE_Source){
                MarkerPoints.add(0,latLng);
            }
            else if(requestCode==REQUEST_CODE_AUTOCOMPLETE_destination){
                MarkerPoints.add(1,latLng);
            }
            setMarker(latLng,requestCode);
        }
        else if(MarkerPoints.size() >1){
            line.remove();

            if(requestCode==REQUEST_CODE_AUTOCOMPLETE_Source){
                MarkerPoints.remove(0);
                sourceMarker.remove();
                MarkerPoints.add(0,latLng);
            }
            else if(requestCode==REQUEST_CODE_AUTOCOMPLETE_destination){
                MarkerPoints.remove(1);
                destinationMarker.remove();
                MarkerPoints.add(1,latLng);
            }
            setMarker(latLng,requestCode);
        }*/


        if (requestCode == REQUEST_CODE_AUTOCOMPLETE_destination) {
            MarkerPoints.add(latLng);
            setMarker(latLng, requestCode);
        }
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE_Source) {
            if (MarkerPoints.size() < 2) {
                MarkerPoints.add(latLng);
            } else {
                if (line != null) {
                    line.remove();
                    if (sourceMarker != null) {
                        sourceMarker.remove();
                    }
                    MarkerPoints.add(1, latLng);
                    setMarker(latLng, requestCode);
                }
            }
        }

/*
        if(MarkerPoints.size()<=2) {

            if (requestCode == REQUEST_CODE_AUTOCOMPLETE_Source) {
                MarkerPoints.add(0, latLng);
            } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE_destination) {
                MarkerPoints.add(1, latLng);
            }
        }
        else if(MarkerPoints.size() >=2){
            if(line!=null) {
                line.remove();
            }

            if(requestCode==REQUEST_CODE_AUTOCOMPLETE_Source){
                MarkerPoints.remove(0);
                sourceMarker.remove();
                MarkerPoints.add(0,latLng);
            }
            else if(requestCode==REQUEST_CODE_AUTOCOMPLETE_destination){
                MarkerPoints.remove(1);
                destinationMarker.remove();
                MarkerPoints.add(1,latLng);
            }
            setMarker(latLng,requestCode);
        }
*/


        if (MarkerPoints.size() >= 2) {
            // Checks, whether start and end locations are captured
            checkNewDirection();
        }
    }

    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        //mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(10000);
        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        /*
        if(location ==null){
            Toast.makeText(this,"cant get location update",Toast.LENGTH_LONG).show();
        }
        else {
            LatLng ll=new LatLng(location.getLatitude(),location.getLongitude());
            CameraUpdate cameraUpdate= CameraUpdateFactory.newLatLngZoom(ll,15);
            mMap.animateCamera(cameraUpdate);
        }
*/
        if(mLastLocation!=null){
            float distance=mLastLocation.distanceTo(location);
            if(distance<10){
                return;
            }
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
   /*     MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        List<Address> addressList=MapUtils.getAddressListFromLatLng(PlaceCompleteActivity.this,latLng,1);


        if(addressList.size()>0){
            edtSource.setText(MapUtils.getAddress(addressList.get(0)));
        }*/
        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        // setMarker(latLng,REQUEST_CODE_AUTOCOMPLETE_Source);
        processNewLatLng(latLng, REQUEST_CODE_AUTOCOMPLETE_Source);


/*        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }*/
    }


    private void setMarker(LatLng latLng, int requestCode) {
        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(latLng);

        /**
         * For the start location, the color of marker is GREEN and
         * for the end location, the color of marker is RED.
         */
        if (MarkerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (MarkerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }


        if (requestCode == REQUEST_CODE_AUTOCOMPLETE_Source) {
            sourceMarker = mMap.addMarker(options);
        } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE_destination) {
            destinationMarker = mMap.addMarker(options);
        }

    }

    /**
     * Helper method to format information about a place nicely.
     */
    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    private void drawLine() {
        if (sourceMarker != null && destinationMarker != null) {
            PolylineOptions options = new PolylineOptions()
                    .add(sourceMarker.getPosition())
                    .add(destinationMarker.getPosition())
                    .color(Color.BLUE)
                    .width(3);
            line = mMap.addPolyline(options);
        }

    }

    private void checkNewDirection() {
        LatLng origin = MarkerPoints.get(0);
        LatLng dest = MarkerPoints.get(1);

        // Getting URL to the Google Directions API
        String url = getUrl(origin, dest);
        Log.d("checkNewDirection", url.toString());
        FetchUrl FetchUrl = new FetchUrl();

        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));

    }


    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

                distance = parser.getRouteDistance(jObject);


            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }


            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.RED);

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }


            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                line = mMap.addPolyline(lineOptions);

                showDistance();


            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }

    private void showDistance() {
        int selectedRouteDistance = distance.get(0);
        float distanceInKm = selectedRouteDistance / 1000 + (float) (selectedRouteDistance % 1000) / 1000;

        txtDistnace.setText("Distance is : " + distanceInKm + " KM");

       // Snackbar.make(linearLayout, "Distance is : " + distanceInKm + " KM", Snackbar.LENGTH_LONG).show();

        // Toast.makeText(mContext, "Hi, Distance is " +distanceInKm +" KM", Toast.LENGTH_LONG).show();
    }


    private void checkMapDraw() {
        checkNewDirection();
    }
}
