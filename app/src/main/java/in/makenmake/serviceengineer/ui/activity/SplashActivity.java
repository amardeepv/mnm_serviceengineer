package in.makenmake.serviceengineer.ui.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;

import in.bapps.ui.BaseActivity;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.location.LocationBroadcastReceiver;
import in.makenmake.serviceengineer.sync.LocationSyncJob;

/**
 * Created by bucky on 04-09-2016.
 */
public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = 2000;

    private FirebaseAnalytics mFirebaseAnalytics;

    private String TAG = SplashActivity.class.getSimpleName();

    private LocationBroadcastReceiver locationReceiver;

   /* private void getLocation() {
        IntentFilter localIntentFilter = new IntentFilter("lcoation_intent_action");
        this.locationReceiver = new LocationBroadcastReceiver();
        this.locationReceiver.setLocationListener(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(this.locationReceiver, localIntentFilter);
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

// Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        LocationSyncJob.schedulePeriodic();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               /* Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);*/
                if (in.makenmake.serviceengineer.utils.EngineerPreference.getInstance().getUserId() !=0) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);

                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    public void onLocationUpdate(Location paramLocation) {
      //  Log.d("location", paramLocation.getLatitude());
    }


    @Override
    public void onStart() {
        super.onStart();
/*        if (!StringUtils.isNullOrEmpty(EngineerPreference.getInstance().getDeviceToken())) {
            startNextActivity(3000);
            return;
        }
        this.gcmUtil = new GcmUtil(this, new GcmUtil.OnRegIdReceivedListener() {
            public void onRegIdReceived(boolean paramBoolean, String paramString) {
                if (paramBoolean) {
                    Log.e(SplashActivity.this.TAG, "regId ------------" + paramString);
                    SplashActivity.this.startNextActivity(500);
                    return;
                }
                AlertDialogUtils.showAlertDialog(SplashActivity.this, "Retry", "Unable to receive GCM id, press \"OK\" to retry", new AlertDialogUtils.OnButtonClickListener() {
                    public void onButtonClick(int paramInt) {
                        SplashActivity.this.gcmUtil.registerInBackground();
                    }
                });
            }
        });*/
    }


    protected void startNextActivity(int paramInt) {
       /* new Handler().postDelayed(new Runnable() {
                                      public void run() {
                                          if (EngineerPreference.getInstance().getLoggedIn()) ;
                                          for (Object localObject = MainActivity.class; ; localObject = ViewPagerActivity.class) {
                                              SplashActivity.this.startActivity(new Intent(SplashActivity.this, (Class) localObject));
                                              SplashActivity.this.finish();
                                              return;
                                          }
                                      }
                                  }
                , paramInt);
                */
    }
}

