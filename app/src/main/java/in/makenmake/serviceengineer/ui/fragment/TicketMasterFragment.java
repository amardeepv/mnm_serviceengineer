package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.bapps.ui.BaseFragment;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.Ticket;
import in.makenmake.serviceengineer.ui.adapter.TicketMasterAdapter;

public class TicketMasterFragment extends BaseFragment implements ActionBar.TabListener{

    Context mContext=null;
    private OnFragmentInteractionListener mListener;
    ViewPager pager;
    ActionBar actionBar;
    TabLayout tabLayout;

    Ticket mTicket;

    public TicketMasterFragment() {
        // Required empty public constructor
    }

    public static TicketMasterFragment newInstance(String param1, String param2) {
        TicketMasterFragment fragment = new TicketMasterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setHeader() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_ticket_master, container, false);

        mTicket = this.getArguments().getParcelable("ticket");
        int ticketStatus=this.getArguments().getInt("ticketStatus");

      /*  Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((MainActivity)mContext).setSupportActionBar(toolbar);*/
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);

        pager= (ViewPager) view.findViewById(R.id.ticketMaster);
        TicketMasterAdapter adapter=new TicketMasterAdapter(getFragmentManager(),mTicket,ticketStatus);
        pager.setAdapter(adapter);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        tabLayout.addTab(tabLayout.newTab().setText("Detail"));
       // tabLayout.addTab(tabLayout.newTab().setText("Job Card"));
        tabLayout.addTab(tabLayout.newTab().setText("History"));

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}
