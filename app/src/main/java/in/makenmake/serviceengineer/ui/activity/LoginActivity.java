package in.makenmake.serviceengineer.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.application.LocaleHelper;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.StringUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.LoginRequest;
import in.makenmake.serviceengineer.model.response.EngineerInfo;
import in.makenmake.serviceengineer.model.response.EngineerInfoObjectRequest;
import in.makenmake.serviceengineer.model.response.LoginResponseNew;
import in.makenmake.serviceengineer.utils.CommonMethods;
import in.makenmake.serviceengineer.utils.EngineerPreference;

import static in.bapps.application.BaseApplication.mContext;

public class LoginActivity extends BaseActivity implements UpdateJsonListener.onUpdateViewJsonListener {

   @BindView(R.id.edtMobileNumber)
   EditText edtMobileNumber;

    @BindView(R.id.edtPassword)
    EditText edtPassword;

    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnRegister)
    Button btnRegister;

    @BindView(R.id.txtForgotPassword)
    TextView txtForgotPassword;

    @BindView(R.id.txtCallCustomerCare)
    TextView txtCallCustomerCare;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        ButterKnife.setDebug(true);

    }


    @OnClick(R.id.btnRegister)
    public void onRegisterButtonClick(){
        startActivity(new Intent(this,RegistrationActivity.class));
      //  startActivity(new Intent(this,OtpVerificationActivity.class));
    }
    
    @OnClick(R.id.btnLogin)
    public void onLogin(){
        if(validateDate()){
            hitApiRequest(ApiConstants.REQUEST_LOGIN);
          //  startActivity(new Intent(LoginActivity.this,MainActivity.class));

        }
    }

    @OnClick(R.id.txtForgotPassword)
    public void onForgotPasswordClick(){
        startActivity(new Intent(this,GetMobileNumberActivity.class));
    }


    private boolean validateDate() {

        if (StringUtils.isNullOrEmpty(edtMobileNumber.getText().toString())) {
            ToastUtils.showToast(this, "Please enter mobile No.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtMobileNumber.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
       /* switch (view.getId()) {
            case R.id.btnSetting:
                openSettingPasswordActivity();
                break;
            case R.id.btnFigure:

                break;
            case R.id.btnGPS:
                onGPSButtonClick();
                break;
            case R.id.txtLogin:
                *//*if (validateData()) {
                    checkLogin();
                }*//*

                checkLogin();
                break;
            default:
                super.onClick(view);
        }*/

    }

    private void onGPSButtonClick() {


/*     it will progesbar in new layout at whole screen
       RelativeLayout layout = new RelativeLayout(this);
        progressBar = new ProgressBar(LoginActivity.this,null,android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(progressBar,params);
        setContentView(layout);*/


      //  progressBar.setVisibility(View.VISIBLE);


    }


    private void openSettingPasswordActivity() {
        /*Intent intent = new Intent(this, SettingPasswordActivity.class);
        this.startActivity(intent);
*/

    }


    private void checkLogin() {





      /*  if (autocomplete.getText().toString().trim() == "") {
            Toast.makeText(LoginActivity.this, "Please enter valid ID/Name", Toast.LENGTH_SHORT).show();

            return;
        }*/

     /*   if (callSign.getSelectedItem() == "Search/Select SignIn") {
            Toast.makeText(LoginActivity.this, "Please select call SignIn", Toast.LENGTH_SHORT).show();
            return;
        }*/

/*
        if (edtPassword.getText().toString().trim().equalsIgnoreCase(assignedOfficers.getDevicePassword())) {
            Intent intent = new Intent(this, HomeActivity.class);

            intent.putExtra("AssignedOfficer",assignedOfficers);

           // EngineerPreference.getInstance().setUsername(officerSearch.getSelectedItem().toString());

            this.startActivity(intent);

            this.finish();
        }
        else {
            Toast.makeText(LoginActivity.this, "Please enter valid password", Toast.LENGTH_SHORT).show();
        }*/
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    CommonMethods.hideKeyboard(LoginActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }




    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyJsonRequest request=null;
        VolleyStringRequest stringRequest=null;
        String url;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LOGIN:
                showProgressDialog();
                 url = ApiConstants.URL_LOGIN;
                className = CommonJsonResponseNew.class;
                try {
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this,this,reqType) {
                    }, getParams(reqType));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            case ApiConstants.REQUEST_ENG_INFO:
                showProgressDialog();
                 url = ApiConstants.URL_ENG_INFO+EngineerPreference.getInstance().getUserId();
                className = CommonJsonResponseNew.class;

                stringRequest = VolleyStringRequest.doGet(url, new UpdateListener(this,this,reqType,className) {
                    });

                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(stringRequest, url);
                break;

            default:
                super.hitApiRequest(reqType);
                break;
        }
    }

    private String getParams(int reqType) {
        String json="";
        Gson gson=new Gson();
        if (reqType == ApiConstants.REQUEST_LOGIN) {
            LoginRequest request=new LoginRequest(
                    edtMobileNumber.getText().toString(),
                    edtPassword.getText().toString()
            );

            Type type=new TypeToken<LoginRequest>(){}.getType();
            json=gson.toJson(request,type);
        }

        return json;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occurred.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_ENG_INFO:
                    Type type=new TypeToken<CommonJsonResponseNew<EngineerInfoObjectRequest>>(){}.getType();

                    CommonJsonResponseNew<EngineerInfoObjectRequest> responseNew=new Gson().fromJson(new Gson().toJson(responseObject),type);

                    if(responseNew.data.getEngineer().size()==0){
                        ToastUtils.showToast(this,"No data for this engineer available");
                        return;
                    }
                    EngineerInfo response=responseNew.data.getEngineer().get(0);

                    EngineerPreference.setEmailId(response.getEmailID());
                    EngineerPreference.setCurrentAddress(response.getCurrentAddress());
                    EngineerPreference.setORIGIN(response.getOrigin());
                    EngineerPreference.setMobileNumber(response.getMobileNumber());
                    EngineerPreference.setImagePath(response.getImagePath());
                    EngineerPreference.setFullName(response.getSengineerFirstName()+" "+response.getSengineerLastName());


                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    List<String> officerList = new ArrayList<>();


    /**
     * update last login user id in officer name and Officer id
     */


    private void setLastLoginId(){

       // UUID uuid=java.util.UUID.randomUUID();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }


    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();

        btnLogin.setText(resources.getString(R.string.lbl_btn_login));
        btnRegister.setText(resources.getString(R.string.btn_lbl_register));
        txtCallCustomerCare.setText(resources.getString(R.string.lbl_btn_call_customer_care));
        txtForgotPassword.setText(resources.getString(R.string.lbl_btn_forgot_password));

    }

    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

        removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                if(responseNew.messageCode.equalsIgnoreCase("ACT12")){

                    serverMigration();
                }

                ToastUtils.showToast(this, responseNew.message);
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN:

                    //   LoginRespone loginRespone = (LoginRespone) responseObject;

                  /*  final Object responseObject = new Gson().fromJson(responseStr, classObject);

                            if (responseObject instanceof CommonJsonResponse) {
                                CommonJsonResponse responseModel = (CommonJsonResponse) responseObject;
                            }
*/


                    Type type=new TypeToken<CommonJsonResponseNew<LoginResponseNew>>(){}.getType();

                    CommonJsonResponseNew<LoginResponseNew> data=new Gson().fromJson(responseString,type);

                    if(data.hasError==true){
                        ToastUtils.showToast(this,data.message);
                        return;
                    }

                    if(!data.messageCode.equalsIgnoreCase(ApiConstants.MessageConstant.LOGIN_SUCCESS)){
                        if(data.messageCode.equalsIgnoreCase(ApiConstants.MessageConstant.LOGIN_ERROR_NON_VERIFIED_ACCOUNT_)){
                            Intent intent = new Intent(mContext, OtpVerificationActivity.class);
                            intent.putExtra("mobileNumber", edtMobileNumber.getText().toString());
                            startActivity(intent);
                            return;
                        }
                        if(data.messageCode.equalsIgnoreCase(ApiConstants.MessageConstant.LOGIN_ERROR_INACTIVATED)){
                            ToastUtils.showToast(this,"Please call customer care to activate your account.");
                            return;
                        }
                        if(data.messageCode.equalsIgnoreCase("ACT20")){
                            serverMigration();
                            return;
                        }

                        ToastUtils.showToast(this,data.message);
                        return;
                    }
                   // String username, int userId,String auth_token,String expire_in, String issuedDate,String expireDate

                    LoginResponseNew responseNew=data.data;

                        EngineerPreference.getInstance().OnUserLogin(
                                responseNew.getAccess().userName,
                                responseNew.getUserId(),
                                responseNew.getAccess().token,
                                responseNew.getAccess().expiresIn,
                                responseNew.getAccess().issued,
                                responseNew.getAccess().expires
                        );

                    hitApiRequest(ApiConstants.REQUEST_ENG_INFO);


                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void serverMigration(){
        Intent intent = new Intent(mContext, ForgotPasswordOtpVerificationActivity.class);

        intent.putExtra("mobileNumber", edtMobileNumber.getText().toString());
        intent.putExtra("requestBy","dataMigration");
        startActivity(intent);
    }
}