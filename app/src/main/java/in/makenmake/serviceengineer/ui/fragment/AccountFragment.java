package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.ui.BaseFragment;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.DateUtils;
import in.bapps.utils.SimpleDividerItemDecoration;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.constants.AppConstants;
import in.makenmake.serviceengineer.model.AccountModel;
import in.makenmake.serviceengineer.model.LocalAttendance;
import in.makenmake.serviceengineer.model.request.MarkAttendance;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.AccountAdapter;
import in.makenmake.serviceengineer.ui.fragment.account.ChangePasswordFragment;
import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends BaseFragment implements AccountAdapter.IMyViewHolderClicks, UpdateJsonListener.onUpdateViewJsonListener {


    Context mContext;
    RecyclerView recyclerView;
    List<AccountModel> accountItemList=null;
    private AccountAdapter adapter;
    private String attendanceType;

    public AccountFragment() {
        // Required empty public constructor
    }

    public static AccountFragment newInstance(String param1, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setHeader() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_account, container, false);
        //ButterKnife.bind(this,view);
        //TODO :Butterknife use
        accountItemList=new ArrayList<>();

        recyclerView= (RecyclerView) view.findViewById(R.id.rview_account_item);
        adapter=new AccountAdapter(mContext,accountItemList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(mContext));
        // recyclerView.addItemDecoration(new AlbumActivity.GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        getAccountItemList();

        return view;
    }

    private void getAccountItemList() {
        int[] covers = new int[]{
                R.drawable.ic_account_black,
                R.drawable.ic_home_map_marker_black,
                R.drawable.ic_lock_reset_black,
                R.drawable.atte,
                };

       /* AccountModel a = new AccountModel(1,"Update Profile Info",covers[0],true);
        accountItemList.add(a);

        a = new AccountModel(2,"Update Address",covers[1],true);
        accountItemList.add(a);*/

        AccountModel a  = new AccountModel(3,"Change Password",covers[2],true);
        accountItemList.add(a);

        processAttendanceStatus();


        adapter.notifyDataSetChanged();
    }

    private void processAttendanceStatus(){
        if(!EngineerPreference.getORIGIN().equals("I")) {
            return;
        }
        AccountModel a = new AccountModel(3, "Mark Attendance", R.drawable.atte, true);


        LocalAttendance attendance=EngineerPreference.getLocalAttendance();

        if (attendance == null) {
            a.setName("In Attendance");
        } else if (attendance.getAttendanceType() == AppConstants.LocalAttendanceType.outAttendance) {
            a.setName("In Attendance");
            attendanceType=AppConstants.LocalAttendanceType.inAttendance;
        } else {
            a.setName("Out Attendance");
            attendanceType=AppConstants.LocalAttendanceType.outAttendance;
        }

        accountItemList.add(a);
    }

    private boolean validateInTimeForAttendance(){

        try {
            String inTimeMinString = "09:00:00";
            Date inTimeMin = new SimpleDateFormat("HH:mm:ss").parse(inTimeMinString);
            Calendar inTimeMinCalendar = Calendar.getInstance();
            inTimeMinCalendar.setTime(inTimeMin);

            String inTimeMaxString = "09:30:00";
            Date inTimeMax = new SimpleDateFormat("HH:mm:ss").parse(inTimeMaxString);
            Calendar inTimeMaxCalendar = Calendar.getInstance();
            inTimeMaxCalendar.setTime(inTimeMax);
            inTimeMaxCalendar.add(Calendar.DATE, 1);


/*            String outTimeString = "19:00:00";
            Date outTimeDate = new SimpleDateFormat("HH:mm:ss").parse(outTimeString);
            Calendar outTimeCalendar = Calendar.getInstance();
            outTimeCalendar.setTime(outTimeDate);
            outTimeCalendar.add(Calendar.DATE, 1);


            String outTimeMaxString = "19:00:00";
            Date outTimeMaxDate = new SimpleDateFormat("HH:mm:ss").parse(outTimeMaxString);
            Calendar outTimeMaxCalendar = Calendar.getInstance();
            outTimeMaxCalendar.setTime(outTimeMaxDate);
            outTimeMaxCalendar.add(Calendar.DATE, 1);*/


           // String someRandomTime = "01:00:00";
            Date currentDate = new SimpleDateFormat("HH:mm:ss").parse(new SimpleDateFormat("HH:mm:ss").format(new Date()));
            Calendar calendarCurrent = Calendar.getInstance();
            calendarCurrent.setTime(currentDate);
            calendarCurrent.add(Calendar.DATE, 1);

            Date currentTime = calendarCurrent.getTime();
            if (currentTime.after(inTimeMinCalendar.getTime()) && currentTime.before(inTimeMaxCalendar.getTime())) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private boolean validateOutTimeForAttendance(){

        try {
         /*   String string1 = "09:00:00";
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);

            String string2 = "09:30:00";
            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);*/


            String outTimeString = "19:00:00";
            Date outTimeDate = new SimpleDateFormat("HH:mm:ss").parse(outTimeString);
            Calendar outTimeCalendar = Calendar.getInstance();
            outTimeCalendar.setTime(outTimeDate);
            outTimeCalendar.add(Calendar.DATE, 1);


            String outTimeMaxString = "19:00:00";
            Date outTimeMaxDate = new SimpleDateFormat("HH:mm:ss").parse(outTimeString);
            Calendar outTimeMaxCalendar = Calendar.getInstance();
            outTimeMaxCalendar.setTime(outTimeMaxDate);
            outTimeMaxCalendar.add(Calendar.DATE, 1);


            // String someRandomTime = "01:00:00";
            Date currentDate = new SimpleDateFormat("HH:mm:ss").parse(new SimpleDateFormat("HH:mm:ss").format(new Date()));
            Calendar calendarCurrent = Calendar.getInstance();
            calendarCurrent.setTime(currentDate);
            calendarCurrent.add(Calendar.DATE, 1);

            Date currentTime = calendarCurrent.getTime();
            if(currentTime.after(outTimeCalendar.getTime()) && currentTime.before(outTimeMaxCalendar.getTime())){
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
         mContext=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    private double calculateDistance(Location locationSource,Location destination){
        return locationSource.distanceTo(destination);//in meters
    }


    private boolean validateAttandencePlace(){

        Location maveHouseLocation=new Location("");
        maveHouseLocation.setLatitude(28.461781);
        maveHouseLocation.setLongitude(77.046883);

        Location mLastLocation=((BaseActivity)mContext).mLastLocation;

        if(calculateDistance(maveHouseLocation,mLastLocation)<100){
            return true;
        }

        if(calculateDistance(maveHouseLocation,getApproxClientLocation())<100){
            return true;
        }

        return false;
    }

    private Location getApproxClientLocation() {
        return new Location("");
    }


    @Override
    public void onAccountItemClick(int postion) {
        /*if(postion==0){
            ToastUtils.showToast(mContext,"Update Profile Option selected");
        }
        else if(postion==1){
            ToastUtils.showToast(mContext,"Update Address option selected");
        }*/
         if(postion==0){
            ((MainActivity)mContext).replaceFragment(null, new ChangePasswordFragment(), null, true);
        }
        else if(postion==1){
            markTodayAttendance();
        }
        else{
            ToastUtils.showToast(mContext,"Unknown option selected");
        }

    }

    private void markTodayAttendance() {
        //TODO : update status locally and server
        //TODO : start or stop location services
        //TODO : start or stop ticket services

       /* if(!validateInTimeForAttendance()){
            ToastUtils.showToast(mContext,"You cant mark attendance at this time");
            return;
        }*/
        if(!validateAttandencePlace()){
            ToastUtils.showToast(mContext,"You can only mark attendance at office or client location.");
            return;
        }

        hitApiRequest(ApiConstants.REQUEST_MARK_ATTENDANCE);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_MARK_ATTENDANCE:
                url = ApiConstants.URL_MARK_ATTENDANCE;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(getActivity(), this, reqType) {
                    }, getJSON());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private String getJSON() {
        String json;
        Gson gson = new Gson();
        MarkAttendance markAttendance = new MarkAttendance(
                DateUtils.getFormattedDate(new Date(), "yyyy-MM-dd hh:mm:ss"),                   //2017-03-16T07:53:27.548Z
                EngineerPreference.getInstance().getUserId()
        );
        Type type = new TypeToken<MarkAttendance>() {
        }.getType();
        json = gson.toJson(markAttendance, type);
        return json;
    }


    public void updateView(String responseString, boolean isSuccess, int reqType) {

        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(mContext, "Some error occured.");
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_MARK_ATTENDANCE:
                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);
                    if (!responseNew.message.equals("")) {
                        ToastUtils.showToast(mContext, responseNew.message);
                        updateAttendanceStatus();
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void updateAttendanceStatus() {
        LocalAttendance attendance=new LocalAttendance();
        if(attendanceType== AppConstants.LocalAttendanceType.inAttendance){
            attendance.setAttendanceType(AppConstants.LocalAttendanceType.inAttendance);
        }
        else {
            attendance.setAttendanceType(AppConstants.LocalAttendanceType.outAttendance);
        }
        attendance.setAttendanceTime(new Date());

        EngineerPreference.setLocalAttendance(attendance);

        /*reload account item list*/
        accountItemList=new ArrayList<>();
        getAccountItemList();
    }


}
