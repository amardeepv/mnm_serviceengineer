package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.bapps.utils.DateUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.CompleteTicket;

/**
 * Created by Bucky on 04/06/2017.
 */

public class CompleteTicketAdapter extends RecyclerView.Adapter<CompleteTicketAdapter.MyViewHolder> {
    private Context mContext;
    private List<CompleteTicket> completeTicketList;
    public CompleteTicketAdapter.IMyViewHolderClicks mListener;

    public void setTicketList(List<CompleteTicket> completeTicketList) {
        this.completeTicketList = completeTicketList;
    }



    public  class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView serviceIcon;

        public TextView ticketId=null,userName, txt_service_name,paymentStatus,txt_payment,txt_ticket_date;
        TextView inprocess;
        ImageView ticketFeedback,ticketHistory;
        CardView cardView;

        public MyViewHolder(View view, CompleteTicketAdapter.IMyViewHolderClicks listener) {
            super(view);
            mListener=listener;

            ticketId = (TextView) view.findViewById(R.id.txt_ticket_id);
            serviceIcon= (ImageView) view.findViewById(R.id.img_service_icon);
            txt_payment = (TextView) view.findViewById(R.id.txt_payment);
            txt_service_name = (TextView) view.findViewById(R.id.txt_service_name);
            paymentStatus= (TextView) view.findViewById(R.id.txt_payment_status);
            txt_ticket_date= (TextView) view.findViewById(R.id.txt_ticket_date);
            //inprocess= (TextView) view.findViewById(R.id.txt_inprocess_ticket);
            ticketFeedback= (ImageView) view.findViewById(R.id.img_ticket_feedback);
            ticketHistory= (ImageView) view.findViewById(R.id.img_ticket_history);





          /*  acceptTicket.setOnClickListener(this);
            rejectTicket.setOnClickListener(this);
            cardView.setOnClickListener(this);*/


        }

/*        @Override
        public void onClick(View v) {
            int _ticketId=Integer.parseInt(ticketId.getText().toString());
            if(v.getId()==acceptTicket.getId()){
                mListener.onTicketAcceptButtonClick(_ticketId);
            }
            else if(v.getId()==rejectTicket.getId()){
                mListener.onTicketRejectButtonClick(_ticketId);
            }
            if(v.getId()==R.id.card_view){
                mListener.onTicketViewClick(_ticketId);
            }

        }*/

    }
    public  interface IMyViewHolderClicks {
        void onTicketFeedbackClick(int ticketId);
        void onTicketHistoryClick(int ticketId);
    }

    public CompleteTicketAdapter(Context mContext, List<CompleteTicket> completeTicketList,CompleteTicketAdapter.IMyViewHolderClicks listener) {
        this.mContext = mContext;
        this.completeTicketList = completeTicketList;
        this.mListener=listener;
    }

    @Override
    public CompleteTicketAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_completed_ticket_row, parent, false);

        return new CompleteTicketAdapter.MyViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final CompleteTicketAdapter.MyViewHolder holder, final int position) {
        CompleteTicket ticket= completeTicketList.get(position);

        String status=ticket.getTicketStatus();

        String createdDate=ticket.getCreated().substring(0,10);

        createdDate= DateUtils.getFormattedDate("yyyy-MM-dd","dd-MM-yy",createdDate);

        Glide.with(mContext).load("http://icons.iconarchive.com/icons/icons8/windows-8/128/Household-Electrical-icon.png").into(holder.serviceIcon);
        holder.paymentStatus.setText(ticket.getPaymentStatus());
        holder.ticketId.setText(ticket.getTicketID()+"");
        holder.txt_service_name.setText(ticket.getServiceName());
        holder.txt_payment.setText(Float.toString(ticket.getAmount()));
        holder.txt_ticket_date.setText(createdDate);

        holder.ticketFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onTicketFeedbackClick(position);
            }
        });

        holder.ticketHistory.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mListener.onTicketHistoryClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return completeTicketList.size();
    }




}
