package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.SparePartCartModel;

/**
 * Created by Bucky on 19/05/2017.
 */
public class SparePartCartAdapter extends RecyclerView.Adapter<SparePartCartAdapter.MyViewHolder> {
    private Context mContext;
    private List<SparePartCartModel> sparePartCartModelList;
    public IMyViewHolderClicks mListener;

    public void setTicketList(List<SparePartCartModel> sparePartCartModelList) {
        this.sparePartCartModelList = sparePartCartModelList;
    }



    public  class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgDelete,imgPlus,imgMinus;

        public TextView txt_sparePart_name,txt_sparePartType_id,
                txt_mrp_price,txt_numberOfSpareParts,txt_selling_price,txt_total_price;

        public MyViewHolder(View view, IMyViewHolderClicks listener) {
            super(view);
            mListener=listener;

            txt_sparePart_name = (TextView) view.findViewById(R.id.txt_sparePart_name);
            txt_sparePartType_id = (TextView) view.findViewById(R.id.txt_sparePartType_id);
            imgDelete= (ImageView) view.findViewById(R.id.imgDelete);
            txt_mrp_price = (TextView) view.findViewById(R.id.txt_mrp_price);
            txt_numberOfSpareParts = (TextView) view.findViewById(R.id.txtNumberOfSpareParts);
            txt_selling_price= (TextView) view.findViewById(R.id.txt_selling_price);
            txt_total_price= (TextView) view.findViewById(R.id.txt_total_price);
            imgPlus= (ImageView) view.findViewById(R.id.imgPlus);
            imgMinus= (ImageView) view.findViewById(R.id.imgMinus);

          /*  acceptTicket.setOnClickListener(this);
            rejectTicket.setOnClickListener(this);
            cardView.setOnClickListener(this);*/


        }

/*        @Override
        public void onClick(View v) {
            int _ticketId=Integer.parseInt(ticketId.getText().toString());
            if(v.getId()==acceptTicket.getId()){
                mListener.onTicketAcceptButtonClick(_ticketId);
            }
            else if(v.getId()==rejectTicket.getId()){
                mListener.onTicketRejectButtonClick(_ticketId);
            }
            if(v.getId()==R.id.card_view){
                mListener.onTicketViewClick(_ticketId);
            }

        }*/

    }
    public  interface IMyViewHolderClicks {
        void onRemoveItemClick(int position);
        void onPlusItemClick(int position);
        void onMinusItemClick(int position);

    }

    public SparePartCartAdapter(Context mContext, List<SparePartCartModel> sparePartCartModelList,IMyViewHolderClicks listener) {
        this.mContext = mContext;
        this.sparePartCartModelList = sparePartCartModelList;
        this.mListener=listener;
    }

    @Override
    public SparePartCartAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spare_part_card, parent, false);

        return new SparePartCartAdapter.MyViewHolder(itemView,mListener);
    }

    @Override
    public void onBindViewHolder(final SparePartCartAdapter.MyViewHolder holder, final int position) {
/*        Ticket ticketModel = ticketList.get(position);
        holder.title.setText(ticketModel.getName());
        holder.count.setText(ticketModel.getNumOfSongs() + " songs");

        // loading album cover using Glide library
        Glide.with(mContext).load(ticketModel.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });*/


        SparePartCartModel sparePartCartModel=sparePartCartModelList.get(position);

        holder.txt_sparePart_name.setText(sparePartCartModel.getSparePartName());
        holder.txt_sparePartType_id.setText(sparePartCartModel.getSparePartTypeModel().getSpareTypeName());
        holder.txt_mrp_price.setText(Float.toString(sparePartCartModel.getMrpPrice()));
        holder.txt_selling_price.setText(Float.toString(sparePartCartModel.getSellingPrice()));
        holder.txt_numberOfSpareParts.setText(Integer.toString(sparePartCartModel.getQty()));
        holder.txt_total_price.setText(Float.toString(sparePartCartModel.getQty()*sparePartCartModel.getSellingPrice()));

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRemoveItemClick(position);
            }
        });

        holder.imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onMinusItemClick(position);
            }
        });

        holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPlusItemClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return sparePartCartModelList.size();
    }



    /*public void swap(ArrayList<Data> datas){
        data.clear();
        data.addAll(datas);
        notifyDataSetChanged();
    }*/

    // to handle empty data set

    /*public void swap(List list){
        if (mFeedsList != null) {
            mFeedsList.clear();
            mFeedsList.addAll(list);
        }
        else {
            mFeedsList = list;
        }
        notifyDataSetChanged();
    }*/
}
