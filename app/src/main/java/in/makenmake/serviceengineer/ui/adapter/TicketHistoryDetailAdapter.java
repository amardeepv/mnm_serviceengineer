package in.makenmake.serviceengineer.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.bapps.utils.DateUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.response.TicketHistoryDetail;

/**
 * Created by Bucky on 03/05/2017.
 */

public class TicketHistoryDetailAdapter extends RecyclerView.Adapter<TicketHistoryDetailAdapter.MyViewHolder>{

    private Context mContext;
    private List<TicketHistoryDetail> detailList;

    public TicketHistoryDetailAdapter(Context mContext, List<TicketHistoryDetail> detailList) {
        this.mContext = mContext;
        this.detailList = detailList;
    }

    public void setListData(List<TicketHistoryDetail> ticketDetail) {
        detailList=ticketDetail;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView Modified;
        public TextView Assign_to;
        public TextView Status;

        public MyViewHolder(View itemView) {
            super(itemView);

            Modified = (TextView) itemView.findViewById(R.id.ServiceType);
            Assign_to = (TextView) itemView.findViewById(R.id.AssignTo);
            Status = (TextView) itemView.findViewById(R.id.Status);

        }

    }

    @Override
    public TicketHistoryDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_ticket_history, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TicketHistoryDetailAdapter.MyViewHolder holder, int position) {
        TicketHistoryDetail detail=detailList.get(position);

        //2017-05-29T22:12:25.037

        String date=detail.getModified().substring(0,10);
        String time=detail.getModified().substring(11,19);

        date=DateUtils.getFormattedDate("yyyy-MM-dd","dd-MM-yy",date);

        holder.Status.setText(detail.getStatus());
        holder.Modified.setText(time+"  "+date);
        holder.Assign_to.setText(detail.getAssignedTo());

    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }
}


   /* private Context mContext;
    private LayoutInflater mInflator;
    private List<TicketHistoryDetail> mlistData;

    public TicketHistoryDetailAdapter(Context context) {
        this.mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mlistData=new ArrayList<>();
    }

public class  ViewHolder extends RecyclerView.ViewHolder {
    TextView Modified;
    TextView Assign_to;
    TextView Status;

    public ViewHolder(View itemView) {
        super(itemView);

        Modified = (TextView) itemView.findViewById(R.id.ServiceType);
        Assign_to = (TextView) itemView.findViewById(R.id.AssignTo);
        Status = (TextView) itemView.findViewById(R.id.Status);

    }
}


    public void setListData(List<TicketHistoryDetail> completeTicketListResponse) {
        this.mlistData = completeTicketListResponse;
    }

    @Override
    public TicketHistoryDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ticket_history, parent, false);
        ViewHolder holder= new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.Status.setText(mlistData.get(position).getStatus());
        holder.Modified.setText(mlistData.get(position).getModified());
        holder.Assign_to.setText(mlistData.get(position).getAssignedTo());
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }*/