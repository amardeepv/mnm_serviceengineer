package in.makenmake.serviceengineer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.StringUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;

public class GetMobileNumberActivity extends BaseActivity {


    @BindView(R.id.entMobileNo)
    EditText entMobileNo;

    @BindView(R.id.btn_submit_getmobile)
    Button btn_submit_getmobile;

    @BindView(R.id.img_cancel_getmobile)
    ImageView img_cancel_getmobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_mobile_number);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_submit_getmobile)
    public void OnSubmitButtonClick() {
        if(validate()) {
            hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD);
        }
    }

    private boolean validate() {

        if (!StringUtils.isValidMobileNumber(entMobileNo.getText().toString(), false, 10)) {
            Toast.makeText(this, "Enter Mobile Number.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @OnClick(R.id.img_cancel_getmobile)
    public void OnCancelButtonClick() {
        finish();

    }

    @Override
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_FORGOT_PASSWORD:
                showProgressDialog();
                url = ApiConstants.URL_FORGOT_PASSWORD_MOBILE+entMobileNo.getText().toString();
                className = CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                CommonJsonResponseNew<String> response = (CommonJsonResponseNew<String>) responseObject;

                Toast.makeText(this,response.message, Toast.LENGTH_SHORT).show();
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    CommonJsonResponseNew<String> response = (CommonJsonResponseNew<String>) responseObject;

                    Toast.makeText(this,response.message, Toast.LENGTH_SHORT).show();

                    if(response.hasError==false){
                        Intent intent=new Intent(this,ForgotPasswordOtpVerificationActivity.class);
                        intent.putExtra("mobileNumber",entMobileNo.getText().toString());
                        startActivity(intent);
                    }
                   /* if (changePasswordResponse.getStatus() > 0) {
                        Bundle bunle = new Bundle();
                        bunle.putString(AppConstants.EXTRA_PHONE_NO, entMobileNo.getText().toString());
                        bunle.putBoolean(AppConstants.FROM_TROUBLE, false);
                        ((BaseActivity) getActivity()).replaceFragment(EnterMobileFragment.class.getSimpleName(), new ForgotPasswordOtpFragment(), bunle, true);
                    } else {
                        ToastUtils.showToast(this, "Wrong number");
                    }*/
                    break;
                default:

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
