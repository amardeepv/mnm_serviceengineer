package in.makenmake.serviceengineer.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.makenmake.serviceengineer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImagePickFragment extends Fragment {


    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.captureFront)
    Button btnPhoto;

    public ImagePickFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_image_pick, container, false);

        ButterKnife.bind(this,view);

        return view;
    }


    @OnClick(R.id.captureFront)
    public void btnPhotoClick(){
/*        Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);*/
    }


}
