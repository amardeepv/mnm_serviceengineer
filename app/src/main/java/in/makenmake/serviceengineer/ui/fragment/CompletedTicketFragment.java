package in.makenmake.serviceengineer.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.CompleteTicket;
import in.makenmake.serviceengineer.model.response.CompleteTicketListResponse;
import in.makenmake.serviceengineer.ui.activity.MainActivity;
import in.makenmake.serviceengineer.ui.adapter.CompleteTicketAdapter;
import in.makenmake.serviceengineer.utils.EngineerPreference;


public class CompletedTicketFragment extends Fragment implements CompleteTicketAdapter.IMyViewHolderClicks, UpdateListener.onUpdateViewListener {

    private static final String TAG = "CompletedTicketFragment";
    private Context mContext;

    private RecyclerView recyclerView;
    private CompleteTicketAdapter adapter;
    private List<CompleteTicket> mTicketList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_completed_ticket, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rview_completed_ticket_list);

        mTicketList = new ArrayList<>();
        adapter = new CompleteTicketAdapter(mContext, mTicketList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new AlbumActivity.GridSpacingItemDecoration(2, dpToPx(10), true));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setAddDuration(1000);
        itemAnimator.setRemoveDuration(1000);

        recyclerView.setItemAnimator(itemAnimator);
        recyclerView.setAdapter(adapter);
        onLoadTicketList();

        return view;
    }

    public void onLoadTicketList() {
        hitApiRequest(ApiConstants.REQUEST_GET_COMPLETED_TICKET);
    }


    public void hitApiRequest(int reqType) {
        String url;
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_COMPLETED_TICKET:
                ((MainActivity)mContext).showProgressDialog();
                url = ApiConstants.URL_GET_COMPLETED_TICKET+ EngineerPreference.getInstance().getUserId();
                className=CommonJsonResponseNew.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType,className) {});

                ((BaseApplication) mContext.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((MainActivity)mContext).removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> response =( CommonJsonResponseNew<String>)responseObject ;
                if(!response.message.equals("")){
                    ToastUtils.showToast(mContext, response.message);
                }
                else{
                    ToastUtils.showToast(mContext, "Some error occured.");
                }
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_COMPLETED_TICKET:

                    Type listType2 = new TypeToken<CommonJsonResponseNew<CompleteTicketListResponse>>() {
                    }.getType();
                    CommonJsonResponseNew<CompleteTicketListResponse> response = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                    mTicketList = response.data.getCompletedList();
                    updateCompleteTicketAdapter(mTicketList);

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateCompleteTicketAdapter(List<CompleteTicket> mTicketList) {
        adapter.setTicketList(mTicketList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       mContext=context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }

    @Override
    public void onTicketFeedbackClick(int ticketId) {
        // ToastUtils.showToast(getActivity(),"On Ticket feedback click");

        TicketFeedbackFragment ticketFeedbackFragment = new TicketFeedbackFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ticket", Integer.toString(mTicketList.get(ticketId).getTicketID()));
        ticketFeedbackFragment.setArguments(bundle);

        ((MainActivity) mContext).replaceFragment(CompletedTicketFragment.class.getSimpleName(), ticketFeedbackFragment, null, true);

    }

    @Override
    public void onTicketHistoryClick(int ticketId) {
      //  ToastUtils.showToast(getActivity(),"On Ticket history click");FeedbackHistory

        TicketHistoryFragment ticketHistoryFragment=new TicketHistoryFragment();
        Bundle bundle=new Bundle();

/*        TicketHistoryList ticketHistoryList=new TicketHistoryList(mTicketList.get(ticketId).getTicketHistoryList());

        bundle.putParcelable("ticketHistory",ticketHistoryList);
        ticketHistoryFragment.setArguments(bundle);*/


        bundle.putString("ticket",Integer.toString(mTicketList.get(ticketId).getTicketID()));
        ticketHistoryFragment.setArguments(bundle);

        ((MainActivity)mContext).replaceFragment(CompletedTicketFragment.class.getSimpleName(),ticketHistoryFragment,null,true);
    }
}
