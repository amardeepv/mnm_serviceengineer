package in.makenmake.serviceengineer.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.model.SparePartCartModel;
import in.makenmake.serviceengineer.model.response.SparePartTypeModel;

/**
 * Created by Bucky on 19/05/2017.
 */

@SuppressLint("ValidFragment")
public class AddSparePartDialog extends DialogFragment implements View.OnClickListener {

    private SparePartCartModel sparePartCartModel;

    private EditText edt_spare_part_name,edt_mrp_price,edt_sellingPrice;
    private TextView txtNumberOfSpareParts,txt_total_amount;
    private ImageView imgMinus,imgPlus,imgClose;
    private Button btnAddSparePart_dialog;
    private Spinner sp_skill_list;

    private List<String> spareTypeNames;
    private AddSparePartListener addSparePartListener;
    private List<SparePartTypeModel> sparePartTypeModelList;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        return super.onCreateDialog(savedInstanceState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title and frame from dialog-fragment
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialogDark);
    }

    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_spare_part, container, false);


        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogFadeAnimation;
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().setGravity(Gravity.TOP);
        getDialog().setCanceledOnTouchOutside(true);


        edt_spare_part_name = (EditText) view.findViewById(R.id.edt_spare_part_name);
        edt_mrp_price = (EditText) view.findViewById(R.id.edt_mrp_price);
        edt_sellingPrice = (EditText) view.findViewById(R.id.edt_sellingPrice);

        txtNumberOfSpareParts= (TextView) view.findViewById(R.id.txtNumberOfSpareParts);
        txt_total_amount= (TextView) view.findViewById(R.id.txt_total_amount);
        btnAddSparePart_dialog= (Button) view.findViewById(R.id.btnAddSparePart_dialog);
        imgMinus= (ImageView) view.findViewById(R.id.imgMinus);
        imgPlus= (ImageView) view.findViewById(R.id.imgPlus);
        imgClose= (ImageView) view.findViewById(R.id.imgClose);
        sp_skill_list= (Spinner) view.findViewById(R.id.sp_skill_list);

        ArrayAdapter adapter=new ArrayAdapter(getActivity(),R.layout.single_row_dropdown,R.id.txt_single_row_item,spareTypeNames);
        sp_skill_list.setAdapter(adapter);

        imgPlus.setOnClickListener(this);
        imgMinus.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        btnAddSparePart_dialog.setOnClickListener(this);

        edt_sellingPrice.addTextChangedListener(sellingPriceWatcher);


        return view;
    }

    private final TextWatcher sellingPriceWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // textView.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {
            updateTotalPrice();

        }
    };

    private Float getSellingPrice(){
        String sellingPriceText= edt_sellingPrice.getText().toString();
        return sellingPriceText.equalsIgnoreCase("")?0:Float.parseFloat(sellingPriceText);
    }

    private void updateTotalPrice(){
        txt_total_amount.setText(Float.toString(getSellingPrice()*getQuantity()));
    }

    @Override
    public void onClick(View pClickSource) {
        switch (pClickSource.getId()) {
            case R.id.btnAddSparePart_dialog:
                onAddSparePart();

                break;
            case R.id.imgMinus:
                onMinusButtonClick();
                break;
            case R.id.imgPlus:
                onPlusButtonClick();
                break;
            case R.id.imgClose:
                onCloseDialog();

                break;
            default:

        }
    }

    private void onCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to cancel?")
                .setPositiveButton("Yes",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                } )
                .setNegativeButton("No",  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // User pressed No button. Write Logic Here
                    }
                }).show();

    }

    private void onPlusButtonClick() {
       setQuantity(getQuantity()+1);
        updateTotalPrice();
    }

    private void onMinusButtonClick() {
        if(getQuantity()>1){
            setQuantity(getQuantity()-1);
            updateTotalPrice();
        }
    }



    private int getQuantity(){
        return Integer.parseInt(txtNumberOfSpareParts.getText().toString());
    }
    private void setQuantity(int quantity){
        txtNumberOfSpareParts.setText(Integer.toString(quantity));
    }


    private void onAddSparePart() {

        if (validateData()) {
            SparePartCartModel sparePartCartModel = new SparePartCartModel();
            sparePartCartModel.setSparePartTypeModel(sparePartTypeModelList.get(sp_skill_list.getSelectedItemPosition()));
            sparePartCartModel.setQty(Integer.parseInt(txtNumberOfSpareParts.getText().toString()));
            sparePartCartModel.setSparePartName(edt_spare_part_name.getText().toString());
            sparePartCartModel.setManuallyAdded(true);
            sparePartCartModel.setMrpPrice(Float.parseFloat(edt_mrp_price.getText().toString()));
            sparePartCartModel.setSellingPrice(Float.parseFloat(edt_sellingPrice.getText().toString()));

            addSparePartListener.onAddManuallySparePart(sparePartCartModel);

            dismiss();
        }
    }

    private boolean validateData() {

        if(edt_spare_part_name.getText().toString().equals("")){
            ToastUtils.showToast(getActivity(),"Please enter spare part name.");
            return false;
        }
        if(edt_mrp_price.getText().toString().equals("")){
            ToastUtils.showToast(getActivity(),"Please enter mrp.");
            return false;
        }
        if(edt_sellingPrice.getText().toString().equals("")){
            ToastUtils.showToast(getActivity(),"Please enter selling price.");
            return false;
        }

        return true;
    }




    public void setData(List<String> spareTypeNames, List<SparePartTypeModel> sparePartTypeModelList,AddSparePartListener addSparePartListener) {
        this.spareTypeNames=spareTypeNames;
        this.sparePartTypeModelList=sparePartTypeModelList;
        this.addSparePartListener=addSparePartListener;
    }


    public interface AddSparePartListener{
        void onAddManuallySparePart(SparePartCartModel sparePartCartModel);
    }
}