package in.makenmake.serviceengineer.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.bapps.application.BaseApplication;
import in.bapps.listener.UpdateJsonListener;
import in.bapps.model.CommonJsonResponseNew;
import in.bapps.network.VolleyJsonRequest;
import in.bapps.network.VolleyStringRequest;
import in.bapps.ui.BaseActivity;
import in.bapps.utils.ConnectivityUtils;
import in.bapps.utils.ToastUtils;
import in.makenmake.serviceengineer.R;
import in.makenmake.serviceengineer.constants.ApiConstants;
import in.makenmake.serviceengineer.model.request.OtpVerficationRequest;

public class OtpVerificationActivity extends BaseActivity implements UpdateJsonListener.onUpdateViewJsonListener {

    @BindView(R.id.edt_otp)
    EditText edtOtpNumber;

    @BindView(R.id.btn_submit_otp)
    Button btnSubmitOtp;

    @BindView(R.id.img_cancel_otp)
    ImageView img_cancel_otp;

    String mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        ButterKnife.bind(this);

        mobileNumber = getIntent().getStringExtra("mobileNumber");
    }


    @OnClick(R.id.btn_submit_otp)
    public void btnSubmitOtp(){
     //   ToastUtils.showToast(this,"Entered otp is "+edtOtpNumber.getText());
        hitApiRequest(ApiConstants.REQUEST_OTP_VERFICATION);
    }



    @OnClick(R.id.img_cancel_otp)
    public void btnCancel(){
       finish();
    }

    private void closeActivity(){
        /*Intent data=new Intent();

        data.putExtra("isVerified",true);
        setResult(RESULT_OK,data);*/

        Intent intent=new Intent(this,LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        VolleyJsonRequest jsonRequest=null;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_OTP_VERFICATION:
                url = ApiConstants.URL_OTP_VERIFICATION;
                className = String.class;
                try {
                    jsonRequest = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType) {
                    }, getJson());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(jsonRequest, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(String responseString, boolean isSuccess, int reqType) {

        removeProgressDialog();
        try {
            if (!isSuccess) {
                Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                }.getType();
                CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);

                ToastUtils.showToast(this, responseNew.message);
                return;
            }
            String responseObject = null;
            switch (reqType) {
                case ApiConstants.REQUEST_OTP_VERFICATION:

                    Type listType2 = new TypeToken<CommonJsonResponseNew<String>>() {
                    }.getType();
                    CommonJsonResponseNew<String> responseNew = new Gson().fromJson(responseString, listType2);
                    //  hitApiRequest(ApiConstants.ADD_BILL);


                    ToastUtils.showToast(this,responseNew.message+", Please login to continue.");
                    closeActivity();
                    break;



                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    public String getJson() {
        OtpVerficationRequest request = new OtpVerficationRequest(
                Integer.parseInt(edtOtpNumber.getText().toString()),
                mobileNumber
        );

        Gson gson = new Gson();
        Type type = new TypeToken<OtpVerficationRequest>() {
        }.getType();
        String json = gson.toJson(request, type);

        return json;
    }
}
