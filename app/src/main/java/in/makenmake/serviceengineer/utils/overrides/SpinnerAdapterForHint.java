package in.makenmake.serviceengineer.utils.overrides;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mohitbaweja on 11/10/16.
 */

public class SpinnerAdapterForHint extends ArrayAdapter{
    public SpinnerAdapterForHint(Context context, int resource) {
        super(context, resource);
    }

    public SpinnerAdapterForHint(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public SpinnerAdapterForHint(Context context, int resource, Object[] objects) {
        super(context, resource, objects);
    }

    public SpinnerAdapterForHint(Context context, int resource, int textViewResourceId, Object[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public SpinnerAdapterForHint(Context context, int resource, List objects) {
        super(context, resource, objects);
    }

    public SpinnerAdapterForHint(Context context, int resource, int textViewResourceId, List objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public boolean isEnabled(int position){
        if(position == 0)
        {
            // Disable the first item from Spinner
            // First item will be use for hint
            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;
        if(position == 0){
            // Set the hint text color gray
            tv.setTextColor(Color.GRAY);
        }
        else {
            tv.setTextColor(Color.BLACK);
        }
        return view;
    }
}
