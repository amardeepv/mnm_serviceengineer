package in.makenmake.serviceengineer.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

/**
 * Created by mohitbaweja on 06/10/16.
 */

public class CommonMethods {

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public static boolean containsBothNumbersAndLetters(String password) {
        boolean digitFound = false;
        boolean letterFound = false;
        for (char ch : password.toCharArray()) {
            if (Character.isDigit(ch)) {
                digitFound = true;
            }
            if (Character.isLetter(ch)) {
                letterFound = true;
            }
            if (digitFound && letterFound) {
                // as soon as we got both a digit and a letter return true
                return true;
            }
        }
        // if not true after passing through the entire string, return false
        return false;
    }


    public static boolean isAnyDigit(String data){
        boolean anyDigit=false;

        for (char ch :data.toCharArray()) {
            if(Character.isDigit(ch)){
                anyDigit=true;
            }
        }

        return anyDigit;
    }


    //TODO: commonProgessBar
    public void commonProgessBar(){
         /*    //ProgressBar(Context context, AttributeSet attrs, int defStyleAttr)
        // Initialize a new Progressbar instance
        // Create a new progress bar programmatically
        ProgressBar pb = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);

        // Create new layout parameters for progress bar
        LayoutParams lp = new LayoutParams(
                550, // Width in pixels
                LayoutParams.WRAP_CONTENT // Height of progress bar
        );

        // Apply the layout parameters for progress bar
        pb.setLayoutParams(lp);

        // Get the progress bar layout parameters
        LayoutParams params = (LayoutParams) pb.getLayoutParams();

        // Set a layout position rule for progress bar
     //   params.addRule(RelativeLayout.BELOW, tv.getId());

        // Apply the layout rule for progress bar
        pb.setLayoutParams(params);

        // Set the progress bar color
        pb.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

        // Finally,  add the progress bar to layout
        r1.addView(pb);*/
    }

    public static UUID getNewUUID(){
        return UUID.randomUUID();
    }




    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    @SuppressWarnings("unused")
    public static byte[] getFileDataFromResource(Context context, @DrawableRes int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        return getFileDataFromBitmap(bitmap);
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        return getFileDataFromBitmap(bitmap);
    }

    /**
     * Turn bitmap into byte array
     *
     * @param bitmap data image
     * @return byte array
     */
    public static byte[] getFileDataFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
