package in.makenmake.serviceengineer.utils;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;

import java.lang.reflect.Type;

import in.bapps.application.BaseApplication;
import in.makenmake.serviceengineer.constants.AppConstants;
import in.makenmake.serviceengineer.model.LocalAttendance;
import in.makenmake.serviceengineer.model.Ticket;

public class EngineerPreference {

    private static SharedPreferences mPreferences;
    private static EngineerPreference mInstance;
    private static Editor mEditor;

    private static String IS_LOGGED_IN = "is_logged_in";
    private static String USER_ID = "user_id";
    private static String USER_NAME = "user_name";
    private static String DEVICE_ID="device_id";

    private static String DEVICE_TOKEN="device_token";

    private static String PRIMARY_SKILL="primary_skill";
    private static String APP_VERSION="app_version";



    private String defaultLanguage="defaultLanguage";
    private String webServiceUrl="webServiceUrl";

    private static String defaultSetting="defaultSetting";



    private static String AUTH_TOKEN="auth_token";
    private static String AUTH_TOKEN_ISSUED_DATE="token_issue_date";
    private static String AUTH_TOKEN_EXPIRE_DATE="token_expire_date";
    private static String AUTH_TOKEN_EXPIRE_IN="token_expire_in";


    private static String SELECTED_TICKET="selectedTicket";
    private static String TICKET_IN_PROGRESS="ticketInProgress";


    private static String PERMISSION_CALL_PHONE="permissionCallPhone";
    private static String PERMISSION_CAMERA="permissionCamera";


    private static String LOCAL_ATTENDANCE="LocalAttendance";








    public  String getAuthToken() {

        return mPreferences.getString(AUTH_TOKEN,"");
    }

    public  void setAuthToken(String authToken) {
        mEditor.putString(AUTH_TOKEN, authToken).apply();
    }

    public  String getAuthTokenIssuedDate() {

        return mPreferences.getString(AUTH_TOKEN_ISSUED_DATE,null);
    }

    public  void setAuthTokenIssuedDate(String authTokenIssuedDate) {
        mEditor.putString(AUTH_TOKEN_ISSUED_DATE, authTokenIssuedDate).apply();
    }

    public  String getAuthTokenExpireDate() {

        return mPreferences.getString(AUTH_TOKEN_EXPIRE_DATE,null);
    }

    public  void setAuthTokenExpireDate(String authTokenExpireDate) {
        mEditor.putString(AUTH_TOKEN_EXPIRE_DATE, authTokenExpireDate).apply();
    }

    public  String getAuthTokenExpireIn() {
        return mPreferences.getString(AUTH_TOKEN_EXPIRE_IN,"");
    }

    public  void setAuthTokenExpireIn(String authTokenExpireIn) {
        mEditor.putString(AUTH_TOKEN_EXPIRE_IN, authTokenExpireIn).apply();
    }

    //Location related
    private static String LAST_GEO="last_geo";
    private static String LATITUDE = "latitude";
    private static String LONGITUDE = "longitude";

    private static String EMAIL_ID="emailId";
    private static String CURRENT_ADDRESS="currentAddress";
    private static String MOBILE_NUMBER="mobileNumber";
    private static String ORIGIN="origin";
    private static String IMAGE_PATH="ImagePath";
    private static String FULL_NAME="fullName";




    private static String ENGINEER_STATUS="engineer_status";

    private static String ENGINEER_TYPE="engineer_type";

    private static String appLang  ="appLang";

    public  String getAppLang() {
        return mPreferences.getString(appLang,AppConstants.appLang_eng);
    }

    public void setAppLang(String newLang) {
        mEditor.putString(appLang,newLang);
    }





    private EngineerPreference() {
    }

    public static EngineerPreference getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new EngineerPreference();
            mPreferences = context.getSharedPreferences(AppConstants.NIGHT_ADVISOR_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }


    public void setLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN, value).apply();
    }

    public boolean getLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN, false);
    }


    public void setUserId(int value) {
        mEditor.putInt(USER_ID, value).apply();
    }

    public int getUserId() {
        return mPreferences.getInt(USER_ID, 0);
    }


    public void setUserName(String value) {
        mEditor.putString(USER_NAME, value).apply();
    }

    public String getUserName() {
        return mPreferences.getString(USER_NAME, null);
    }

    public String getDefaultLanguage() {
        return mPreferences.getString(defaultLanguage,"en");
    }

    public void setDefaultLanguage(String lang) {
        mEditor.putString(defaultLanguage,lang);
    }


    /**
     * // get your access token from SharedPrefs
     * @return if user logined return token else return null
     */
    private String getAccessToken() {
        return mPreferences.getString(defaultLanguage,"en");
    }

    /**
     * to set userDeatils and token details when user login
     * @param username
     * @param userId
     * @param auth_token
     * @param expire_in
     * @param issuedDate
     * @param expireDate
     */
    public void OnUserLogin(String username, int userId,String auth_token,String expire_in, String issuedDate,String expireDate) {
        mEditor.putString(USER_NAME, username).apply();
        mEditor.putInt(USER_ID, userId).apply();
        mEditor.putString(AUTH_TOKEN, auth_token).apply();
        mEditor.putString(AUTH_TOKEN_EXPIRE_IN, expire_in).apply();
        mEditor.putString(AUTH_TOKEN_ISSUED_DATE, issuedDate).apply();
        mEditor.putString(AUTH_TOKEN_EXPIRE_DATE, expireDate).apply();

    }

    public void OnUserLogut() {
        mEditor.putString(USER_NAME, null).apply();
        mEditor.putInt(USER_ID, 0).apply();
        mEditor.putString(AUTH_TOKEN, null).apply();
        mEditor.putString(AUTH_TOKEN_EXPIRE_IN, null).apply();
        mEditor.putString(AUTH_TOKEN_ISSUED_DATE, null).apply();
        mEditor.putString(AUTH_TOKEN_EXPIRE_DATE, null).apply();

    }



    /**
     * to check if auth_token is expired or not
     * @return return false if token is NOT expired else return true
     */
    public boolean hasTokenExpired() {

        DateTime expireOn=DateTime.parse(mPreferences.getString(AUTH_TOKEN_EXPIRE_DATE,null));

        if(expireOn.isAfter(DateTime.now())){
            return true;
        }
        else{
            return false;
        }
    }

    public String getEngineerStatus() {
        return mPreferences.getString(ENGINEER_STATUS,null);
    }

    public void setEngineerStatus(String engineerStatus) {
        mEditor.putString(ENGINEER_STATUS,engineerStatus).apply();
    }

    public String getEngineerType() {
        return mPreferences.getString(ENGINEER_TYPE,null);
    }

    public void setEngineerType(String engineerType) {
        mEditor.putString(ENGINEER_TYPE,engineerType).apply();
    }

    public static String getDeviceId() {
        return mPreferences.getString(DEVICE_ID,null);
    }

    public static void setDeviceId(String deviceId) {
        mEditor.putString(DEVICE_ID,deviceId).apply();
    }

    //TODO : why default is "aa"
    public static String getDeviceToken() {
        return mPreferences.getString(DEVICE_TOKEN,"aa");
    }

    public static void setDeviceToken(String deviceToken) {
        mEditor.putString(DEVICE_TOKEN,deviceToken).apply();
    }


    public static int getAppVersion() {
        return mPreferences.getInt(APP_VERSION,1);
    }

    public static void setAppVersion(int appVersion) {
        mEditor.putInt(APP_VERSION,appVersion).apply();
    }

    public static String getPrimarySkill() {
        return mPreferences.getString(PRIMARY_SKILL,null);
    }

    public static void setPrimarySkill(String primarySkill) {
        mEditor.putString(PRIMARY_SKILL,primarySkill).apply();
    }

    public static String getLastGeo() {
        return mPreferences.getString(LAST_GEO,null);
    }

    public static void setLastGeo(String lastGeo) {
        mEditor.putString(LAST_GEO,lastGeo).apply();
    }

    public static String getLatitude() {
        return mPreferences.getString(LATITUDE,null);
    }

    public static void setLatitude(String lat) {
        mEditor.putString(LATITUDE,lat).apply();
    }

    public static String getLongitude() {
        return mPreferences.getString(LONGITUDE,null);
    }

    public static void setLongitude(String lng) {
        mEditor.putString(LONGITUDE,lng).apply();
    }

    public Ticket getSelectedTicket() {

        String ticket = mPreferences.getString(SELECTED_TICKET, null);
        Type listType = new TypeToken<Ticket>() {
        }.getType();
        return new Gson().fromJson(ticket, listType);
    }

    public void setSelectedTicket(Ticket selectedTicket) {
        Gson gson = new Gson();
        mEditor.putString(SELECTED_TICKET, gson.toJson(selectedTicket)).apply();
    }


    public static Ticket getTicketInProgress() {
        String ticket = mPreferences.getString(TICKET_IN_PROGRESS, null);
        Type listType = new TypeToken<Ticket>() {
        }.getType();
        return new Gson().fromJson(ticket, listType);
    }

    public static void setTicketInProgress(Ticket ticketInProgress) {
        Gson gson = new Gson();
        if(ticketInProgress==null){
            mEditor.putString(TICKET_IN_PROGRESS, null).apply();
            return;
        }
        mEditor.putString(TICKET_IN_PROGRESS, gson.toJson(ticketInProgress)).apply();
    }

    public static String getFullName() {
        return mPreferences.getString(FULL_NAME,null);
    }

    public static void setFullName(String fullName) {
        mEditor.putString(FULL_NAME,fullName).apply();
    }

    public static String getImagePath() {
        return mPreferences.getString(IMAGE_PATH,null);
    }

    public static void setImagePath(String imagePath) {
        mEditor.putString(IMAGE_PATH,imagePath).apply();
    }

    public static String getORIGIN() {
        return mPreferences.getString(ORIGIN,null);
    }

    public static void setORIGIN(String origin) {
        mEditor.putString(ORIGIN,origin).apply();
    }

    public static String getMobileNumber() {
        return mPreferences.getString(MOBILE_NUMBER,null);
    }

    public static void setMobileNumber(String mobileNumber) {
        mEditor.putString(MOBILE_NUMBER,mobileNumber).apply();
    }

    public static String getCurrentAddress() {
        return mPreferences.getString(CURRENT_ADDRESS,null);
    }

    public static void setCurrentAddress(String currentAddress) {
        mEditor.putString(CURRENT_ADDRESS,currentAddress).apply();
    }

    public static String getEmailId() {
        return mPreferences.getString(EMAIL_ID,null);
    }

    public static void setEmailId(String emailId) {
        mEditor.putString(EMAIL_ID,emailId).apply();
    }


    public static Boolean getPermissionCallPhone() {
        return mPreferences.getBoolean(Manifest.permission.CALL_PHONE,false);
    }

    public static void setPermissionCallPhone(Boolean permissionCallPhone) {
        mEditor.putBoolean(Manifest.permission.CALL_PHONE,permissionCallPhone).apply();
    }


    public static Boolean getPermissionCamera() {
        return mPreferences.getBoolean(Manifest.permission.CAMERA,false);
    }

    public static void setPermissionCamera(Boolean permissionCamera) {
        mEditor.putBoolean(Manifest.permission.CAMERA,permissionCamera).apply();
    }

    public static Boolean getPermissionLocation() {
        return mPreferences.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION,false);
    }

    public static void setPermissionLocation(Boolean permissionLocation) {
        mEditor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION,permissionLocation).apply();
    }

    public static LocalAttendance getLocalAttendance() {
        String localAttendance = mPreferences.getString(LOCAL_ATTENDANCE, null);
        Type listType = new TypeToken<LocalAttendance>() {
        }.getType();
        return new Gson().fromJson(localAttendance, listType);
    }

    public static void setLocalAttendance(LocalAttendance localAttendance) {
        Gson gson = new Gson();
        if(localAttendance==null){
            mEditor.putString(LOCAL_ATTENDANCE, null).apply();
            return;
        }
        mEditor.putString(LOCAL_ATTENDANCE, gson.toJson(localAttendance)).apply();
    }
}