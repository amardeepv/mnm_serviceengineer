package in.makenmake.serviceengineer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import in.makenmake.serviceengineer.services.LocationUpdateService;

/**
 * Created by Bucky on 04/04/2017.
 */

public class MyBootBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "MyBootBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            context.startService(new Intent(context, LocationUpdateService.class));
        }
    }
}
