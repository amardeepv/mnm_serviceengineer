package in.makenmake.serviceengineer.model.response;

import java.util.List;

/**
 * Created by Bucky on 03/05/2017.
 */

public class TicketHistoryDetailResponse {
    private List<TicketHistoryDetail> HistoryList;

    public TicketHistoryDetailResponse(List<TicketHistoryDetail> historyList) {
        HistoryList = historyList;
    }

    public TicketHistoryDetailResponse() {
    }

    public List<TicketHistoryDetail> getHistoryList() {
        return HistoryList;
    }

    public void setHistoryList(List<TicketHistoryDetail> historyList) {
        HistoryList = historyList;
    }
}
