package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 17/03/2017.
 */

public class MarkAttendance {

    private int attendanceId;
    private String attendDate;
    private int engineerId;
    private String status;

    /*{
        "attendanceId": 0,
            "attendDate": "2017-03-16T07:53:27.548Z",
            "engineerId": 3,
            "status": "P"
    }*/
    public MarkAttendance() {
    }

    public MarkAttendance(String attendDate, int engineerId) {
        this.attendDate = attendDate;
        this.engineerId = engineerId;

        //below data is hardcoded as discussed with monika over skype 17-03

        this.status="P";
        this.attendanceId=0;
    }
}
