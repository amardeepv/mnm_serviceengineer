package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Bucky on 05/06/2017.
 */

public class FeedbackHistoryList implements Parcelable{

    private List<FeedbackHistory> feedbackHistoryList;

    protected FeedbackHistoryList(Parcel in) {
        feedbackHistoryList = in.createTypedArrayList(FeedbackHistory.CREATOR);
    }

    public FeedbackHistoryList(List<FeedbackHistory> feedbackHistoryList) {
        this.feedbackHistoryList = feedbackHistoryList;
    }


    public FeedbackHistoryList() {
    }

    public static final Creator<FeedbackHistoryList> CREATOR = new Creator<FeedbackHistoryList>() {
        @Override
        public FeedbackHistoryList createFromParcel(Parcel in) {
            return new FeedbackHistoryList(in);
        }

        @Override
        public FeedbackHistoryList[] newArray(int size) {
            return new FeedbackHistoryList[size];
        }
    };

    public List<FeedbackHistory> getFeedbackHistoryList() {
        return feedbackHistoryList;
    }

    public void setFeedbackHistoryList(List<FeedbackHistory> feedbackHistoryList) {
        this.feedbackHistoryList = feedbackHistoryList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(feedbackHistoryList);
    }
}
