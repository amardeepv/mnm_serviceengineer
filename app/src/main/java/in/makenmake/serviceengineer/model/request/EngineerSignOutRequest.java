package in.makenmake.serviceengineer.model.request;

import java.util.List;

/**
 * Created by Bucky on 26/04/2017.
 */

public class EngineerSignOutRequest {
    /*{
        "ticketId": 0,
            "ticketStatus": 0,
            "ticketNextDate": "2017-04-25T15:30:36.077Z",
            "reason": "string",
            "wages": [
        {
            "wagesName": "string"
        }
        ]
    }*/

    private int ticketId;
    private int ticketStatus;
    private String ticketNextDate;
    private String reason;
    private List<Wages> wages;


    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(int ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getTicketNextDate() {
        return ticketNextDate;
    }

    public void setTicketNextDate(String ticketNextDate) {
        this.ticketNextDate = ticketNextDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<Wages> getWages() {
        return wages;
    }

    public void setWages(List<Wages> wages) {
        this.wages = wages;
    }
}
