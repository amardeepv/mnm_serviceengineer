package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 27/03/2017.
 */

public class ChangePasswordRequest {
    private int userId;
    private String oldPassword;
    private String newPassword;

    public ChangePasswordRequest(int userId, String oldPassword, String newPassword) {
        this.userId = userId;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public ChangePasswordRequest() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /*{
        "userId": 0,
            "oldPassword": "string",
            "newPassword": "string"
    }*/
}
