package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 28/04/2017.
 */

public class SparePartsNameModel {

/*            "SparePartTypeId": 1,
            "SparePartName": "Switch",
            "MRPPrice": 100,
            "SellingPrice": 110,
            "Status": 1*/

    private int SparePartTypeId;
    private String SparePartName;
    private float MRPPrice;
    private float SellingPrice;
    private int Status;


    public int getSparePartTypeId() {
        return SparePartTypeId;
    }

    public void setSparePartTypeId(int sparePartTypeId) {
        SparePartTypeId = sparePartTypeId;
    }

    public String getSparePartName() {
        return SparePartName;
    }

    public void setSparePartName(String sparePartName) {
        SparePartName = sparePartName;
    }

    public float getMRPPrice() {
        return MRPPrice;
    }

    public void setMRPPrice(float MRPPrice) {
        this.MRPPrice = MRPPrice;
    }

    public float getSellingPrice() {
        return SellingPrice;
    }

    public void setSellingPrice(float sellingPrice) {
        SellingPrice = sellingPrice;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
}
