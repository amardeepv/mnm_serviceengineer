package in.makenmake.serviceengineer.model.request;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 23/04/2017.
 */

public class ImagesType implements Parcelable{

    private String imageType ="jpg";
    private String imageName;
    private String imagePath;

    public ImagesType() {
    }

    public ImagesType(String imageName, String imagePath) {
        this.imageName = imageName;
        this.imagePath = imagePath;
        this.imageType= "jpg";
    }

    public ImagesType(String imageType, String imageName, String imagePath) {
        this.imageType = imageType;
        this.imageName = imageName;
        this.imagePath = imagePath;
    }

    protected ImagesType(Parcel in) {
        imageType = in.readString();
        imageName = in.readString();
        imagePath = in.readString();
    }

    public static final Creator<ImagesType> CREATOR = new Creator<ImagesType>() {
        @Override
        public ImagesType createFromParcel(Parcel in) {
            return new ImagesType(in);
        }

        @Override
        public ImagesType[] newArray(int size) {
            return new ImagesType[size];
        }
    };

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageType);
        dest.writeString(imageName);
        dest.writeString(imagePath);
    }
}
