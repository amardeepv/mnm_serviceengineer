package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 03/05/2017.
 */

public class CompletedList {
    private int ticketId;

    public CompletedList(int ticketId) {
        this.ticketId = ticketId;
    }

    public CompletedList() {
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }
}
