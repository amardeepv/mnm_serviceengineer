package in.makenmake.serviceengineer.model;

/**
 * Created by Bucky on 27/03/2017.
 */

public class AccountModel {
    private int id;
    private String name;
    private int image;
    private boolean isActive;

    public AccountModel(int id, String name, int image,boolean isActive) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.isActive=isActive;
    }

    public AccountModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
