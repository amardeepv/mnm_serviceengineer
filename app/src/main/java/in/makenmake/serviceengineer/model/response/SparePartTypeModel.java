package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 28/04/2017.
 */

public class SparePartTypeModel {

    /*"sparePartId": 1,
            "spareTypeName": "Electrical"
    */
    private int sparePartId;
    private String spareTypeName;

    public SparePartTypeModel(int sparePartId, String spareTypeName) {
        this.sparePartId = sparePartId;
        this.spareTypeName = spareTypeName;
    }

    public SparePartTypeModel() {
    }

    public int getSparePartId() {
        return sparePartId;
    }

    public void setSparePartId(int sparePartId) {
        this.sparePartId = sparePartId;
    }

    public String getSpareTypeName() {
        return spareTypeName;
    }

    public void setSpareTypeName(String spareTypeName) {
        this.spareTypeName = spareTypeName;
    }
}
