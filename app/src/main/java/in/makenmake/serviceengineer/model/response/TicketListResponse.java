package in.makenmake.serviceengineer.model.response;

import java.util.List;

import in.makenmake.serviceengineer.model.Ticket;

/**
 * Created by Bucky on 28/03/2017.
 */

public class TicketListResponse {
    public boolean hasError;
    public String messageCode;
    public List<String> messages;
    public TicketListObject data;

    public TicketListResponse() {
    }


    public class TicketListObject{
        public List<Ticket> EngineerTicketList;
    }

}
