package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 26/04/2017.
 */

public class Wages {

    private String wagesName;

    public String getWagesName() {
        return wagesName;
    }

    public void setWagesName(String wagesName) {
        this.wagesName = wagesName;
    }

}
