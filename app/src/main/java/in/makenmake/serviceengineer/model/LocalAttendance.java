package in.makenmake.serviceengineer.model;

import java.util.Date;

/**
 * Created by Bucky on 19/06/2017.
 */

public class LocalAttendance {
    private String attendanceType;
    private java.util.Date attendanceTime;

    public String getAttendanceType() {
        return attendanceType;
    }

    public void setAttendanceType(String attendanceType) {
        this.attendanceType = attendanceType;
    }

    public Date getAttendanceTime() {
        return attendanceTime;
    }

    public void setAttendanceTime(Date attendanceTime) {
        this.attendanceTime = attendanceTime;
    }
}
