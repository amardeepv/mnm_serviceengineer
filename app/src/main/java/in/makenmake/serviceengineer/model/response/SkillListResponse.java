package in.makenmake.serviceengineer.model.response;

import java.util.List;

import in.bapps.model.CommonJsonResponse;

/**
 * Created by Bucky on 18/03/2017.
 */

public class SkillListResponse extends CommonJsonResponse{

    public boolean hasError;
    public String messageCode;
    public List<String> messages;
    public SkillObject data;

    public SkillListResponse() {
    }


    public class SkillObject{
        public List<Skills> Skills;
    }

}

