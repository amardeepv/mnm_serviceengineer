package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 28/04/2017.
 */

public class GetJobCardDetailsResponse {

/*          "tickeId": 0,
            "engineerId": 0,
            "convenienceCharges": null,
            "totalAmount": null,
            "advanceAmount": null,
            "dueAmount": null,
            "amountRecived": null,
            "ticketAmount": null,
            "convcyanceCharge": null,
            "SpareParts": null*/


    private int tickeId;
    private int engineerId;
    private int convenienceCharges;
    private int totalAmount;
    private int advanceAmount;
    private int dueAmount;
    private int amountRecived;
    private int ticketAmount;
    private int convcyanceCharge;
    private Object SpareParts;

    public GetJobCardDetailsResponse() {
    }

    public int getTickeId() {
        return tickeId;
    }

    public void setTickeId(int tickeId) {
        this.tickeId = tickeId;
    }

    public int getEngineerId() {
        return engineerId;
    }

    public void setEngineerId(int engineerId) {
        this.engineerId = engineerId;
    }

    public int getConvenienceCharges() {
        return convenienceCharges;
    }

    public void setConvenienceCharges(int convenienceCharges) {
        this.convenienceCharges = convenienceCharges;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(int advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public int getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(int dueAmount) {
        this.dueAmount = dueAmount;
    }

    public int getAmountRecived() {
        return amountRecived;
    }

    public void setAmountRecived(int amountRecived) {
        this.amountRecived = amountRecived;
    }

    public int getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(int ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public int getConvcyanceCharge() {
        return convcyanceCharge;
    }

    public void setConvcyanceCharge(int convcyanceCharge) {
        this.convcyanceCharge = convcyanceCharge;
    }

    public Object getSpareParts() {
        return SpareParts;
    }

    public void setSpareParts(Object spareParts) {
        SpareParts = spareParts;
    }
}
