package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 03/05/2017.
 */

public class EngineerInfo {
    private int detailID;
    private int sengineerID;
    private String sengineerFirstName;
    private String sengineerLastName;
    private String emailID;
    private String gender;
    private String dob;
    private int zoneID;
    private int subZoneID;
    private String currentAddress;
    private int currentCountry;
    private int currentState;
    private int currentDistrict;
    private int currentCity;
    private String mobileNumber;
    private int createdBy;
    private String created;
    private String modified;
    private int modifiedBy;
    private String origin;    //I internal engineer E external engineer
    private String panNumber;
    private String agencyCode;
    private String ImagePath;

    public int getDetailID() {
        return detailID;
    }

    public void setDetailID(int detailID) {
        this.detailID = detailID;
    }

    public int getSengineerID() {
        return sengineerID;
    }

    public void setSengineerID(int sengineerID) {
        this.sengineerID = sengineerID;
    }

    public String getSengineerFirstName() {
        return sengineerFirstName;
    }

    public void setSengineerFirstName(String sengineerFirstName) {
        this.sengineerFirstName = sengineerFirstName;
    }

    public String getSengineerLastName() {
        return sengineerLastName;
    }

    public void setSengineerLastName(String sengineerLastName) {
        this.sengineerLastName = sengineerLastName;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getZoneID() {
        return zoneID;
    }

    public void setZoneID(int zoneID) {
        this.zoneID = zoneID;
    }

    public int getSubZoneID() {
        return subZoneID;
    }

    public void setSubZoneID(int subZoneID) {
        this.subZoneID = subZoneID;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public int getCurrentCountry() {
        return currentCountry;
    }

    public void setCurrentCountry(int currentCountry) {
        this.currentCountry = currentCountry;
    }

    public int getCurrentState() {
        return currentState;
    }

    public void setCurrentState(int currentState) {
        this.currentState = currentState;
    }

    public int getCurrentDistrict() {
        return currentDistrict;
    }

    public void setCurrentDistrict(int currentDistrict) {
        this.currentDistrict = currentDistrict;
    }

    public int getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(int currentCity) {
        this.currentCity = currentCity;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public int getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }
}
