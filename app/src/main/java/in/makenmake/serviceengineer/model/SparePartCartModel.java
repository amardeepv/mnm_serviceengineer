package in.makenmake.serviceengineer.model;

import in.makenmake.serviceengineer.model.response.SparePartTypeModel;

/**
 * Created by Bucky on 19/05/2017.
 */

public class SparePartCartModel {
            /*"sparePartName": "string",
            "sparePartAmount": 0,
            "sparePartsTypeId": 0,
            "qty": 0,
            "mrpPrice": 0,
            "sellingPrice": 0*/

    private String sparePartName;
    private Float sparePartAmount;
    private SparePartTypeModel sparePartTypeModel;
    private int sparePartsTypeId;
    private int qty;
    private Float mrpPrice;
    private Float sellingPrice;
    private boolean isManuallyAdded;

    public String getSparePartName() {
        return sparePartName;
    }

    public void setSparePartName(String sparePartName) {
        this.sparePartName = sparePartName;
    }

    public Float getSparePartAmount() {
        return sparePartAmount;
    }

    public void setSparePartAmount(Float sparePartAmount) {
        this.sparePartAmount = sparePartAmount;
    }

    public SparePartTypeModel getSparePartTypeModel() {
        return sparePartTypeModel;
    }

    public void setSparePartTypeModel(SparePartTypeModel sparePartTypeModel) {
        this.sparePartTypeModel = sparePartTypeModel;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Float getMrpPrice() {
        return mrpPrice;
    }

    public void setMrpPrice(Float mrpPrice) {
        this.mrpPrice = mrpPrice;
    }

    public Float getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Float sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public boolean isManuallyAdded() {
        return isManuallyAdded;
    }

    public void setManuallyAdded(boolean manuallyAdded) {
        isManuallyAdded = manuallyAdded;
    }

    public int getSparePartsTypeId() {
        return sparePartsTypeId;
    }

    public void setSparePartsTypeId(int sparePartsTypeId) {
        this.sparePartsTypeId = sparePartsTypeId;
    }
}
