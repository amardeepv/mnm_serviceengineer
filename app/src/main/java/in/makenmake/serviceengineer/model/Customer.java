package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 29/01/2017.
 */

public class Customer implements Parcelable{

    private int userId;
    private String customerName;
    private String emailId;
    private String mobileNumber;
    private Address consumerAddress;

    public Customer(int userId, String customerName, String emailId, String mobileNumber, Address consumerAddress) {
        this.userId = userId;
        this.customerName = customerName;
        this.emailId = emailId;
        this.mobileNumber = mobileNumber;
        this.consumerAddress = consumerAddress;
    }

    public Customer() {
    }

    protected Customer(Parcel in) {
        userId = in.readInt();
        customerName = in.readString();
        emailId = in.readString();
        mobileNumber = in.readString();
        consumerAddress = in.readParcelable(Address.class.getClassLoader());
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Address getConsumerAddress() {
        return consumerAddress;
    }

    public void setConsumerAddress(Address consumerAddress) {
        this.consumerAddress = consumerAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeString(customerName);
        dest.writeString(emailId);
        dest.writeString(mobileNumber);
        dest.writeParcelable(consumerAddress, flags);
    }
}
