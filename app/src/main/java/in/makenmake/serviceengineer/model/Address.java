package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 29/01/2017.
 */

public class Address implements Parcelable{
    private int cityId;
    private String cityName;
    private int stateId;
    private String stateName;
    private String address;
    private int areaId;
    private String areaName;
    private String houseNumber;
    private String locality;
    private  double longitutde;
    private  double latitude;

    public Address(int cityId, String cityName, int stateId, String stateName, String address, int areaId, String areaName, String houseNumber, String locality, double longitutde, double latitude) {
        this.cityId = cityId;
        this.cityName = cityName;
        this.stateId = stateId;
        this.stateName = stateName;
        this.address = address;
        this.areaId = areaId;
        this.areaName = areaName;
        this.houseNumber = houseNumber;
        this.locality = locality;
        this.longitutde = longitutde;
        this.latitude = latitude;
    }

    public Address() {
    }

    protected Address(Parcel in) {
        cityId = in.readInt();
        cityName = in.readString();
        stateId = in.readInt();
        stateName = in.readString();
        address = in.readString();
        areaId = in.readInt();
        areaName = in.readString();
        houseNumber = in.readString();
        locality = in.readString();
        longitutde = in.readDouble();
        latitude = in.readDouble();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public double getLongitutde() {
        return longitutde;
    }

    public void setLongitutde(double longitutde) {
        this.longitutde = longitutde;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cityId);
        dest.writeString(cityName);
        dest.writeInt(stateId);
        dest.writeString(stateName);
        dest.writeString(address);
        dest.writeInt(areaId);
        dest.writeString(areaName);
        dest.writeString(houseNumber);
        dest.writeString(locality);
        dest.writeDouble(longitutde);
        dest.writeDouble(latitude);
    }
}
