package in.makenmake.serviceengineer.model;

/**
 * Created by user on 19-09-16.
 */
public class AllServices {

    private String categroy1;
    private String serviceID;
    private String Title;
    private String Content;
    private String imageIcon;
    private String ImagePath;

    public String getCategroy1() {
        return categroy1;
    }

    public void setCategroy1(String categroy1) {
        this.categroy1 = categroy1;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(String imageIcon) {
        this.imageIcon = imageIcon;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }
}
