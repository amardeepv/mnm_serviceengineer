package in.makenmake.serviceengineer.model.response;

import java.util.List;

/**
 * Created by Bucky on 18/05/2017.
 */

public class SparePartsTypeResponse {
    private List<SparePartTypeModel> SpareTypes;

    public List<SparePartTypeModel> getSpareTypes() {
        return SpareTypes;
    }

    public void setSpareTypes(List<SparePartTypeModel> spareTypes) {
        SpareTypes = spareTypes;
    }
}
