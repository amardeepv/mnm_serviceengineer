package in.makenmake.serviceengineer.model.request;

import java.util.List;

/**
 * Created by Bucky on 23/04/2017.
 */

public class CheckInCheckOutRequest {
    /*{
            "ticketId": 0,
            "serviceFrom": "string",
            "serviceTo": "string",
            "workDescCheckIn": "string",
            "workDescCheckOut": "string",
            "estimateTime": "string",
            "imagesType": [
        {
            "imageType": "string",
                "imageName": "string",
                "imagePath": "string"
        }
        ]
    }*/

    private int ticketId;
    private String serviceFrom;
    private String serviceTo;
    private String workDescCheckIn;
    private String workDescCheckOut;
    private String estimateTime;
    private List<ImagesType> imagesType;

    public CheckInCheckOutRequest(int ticketId, String serviceFrom, String serviceTo, String workDescCheckIn, String workDescCheckOut, String estimateTime, List<ImagesType> imagesType) {
        this.ticketId = ticketId;
        this.serviceFrom = serviceFrom;
        this.serviceTo = serviceTo;
        this.workDescCheckIn = workDescCheckIn;
        this.workDescCheckOut = workDescCheckOut;
        this.estimateTime = estimateTime;
        this.imagesType = imagesType;
    }

    public CheckInCheckOutRequest() {
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getServiceFrom() {
        return serviceFrom;
    }

    public void setServiceFrom(String serviceFrom) {
        this.serviceFrom = serviceFrom;
    }

    public String getServiceTo() {
        return serviceTo;
    }

    public void setServiceTo(String serviceTo) {
        this.serviceTo = serviceTo;
    }

    public String getWorkDescCheckIn() {
        return workDescCheckIn;
    }

    public void setWorkDescCheckIn(String workDescCheckIn) {
        this.workDescCheckIn = workDescCheckIn;
    }

    public String getWorkDescCheckOut() {
        return workDescCheckOut;
    }

    public void setWorkDescCheckOut(String workDescCheckOut) {
        this.workDescCheckOut = workDescCheckOut;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public List<ImagesType> getImagesType() {
        return imagesType;
    }

    public void setImagesType(List<ImagesType> imagesType) {
        this.imagesType = imagesType;
    }
}
