package in.makenmake.serviceengineer.model.request;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Bucky on 24/04/2017.
 */

public class ImagesTypeList implements Parcelable{
    private List<ImagesType> imageList;

    public ImagesTypeList(List<ImagesType> imagesTypes) {
        this.imageList = imagesTypes;
    }

    public ImagesTypeList() {
    }

    protected ImagesTypeList(Parcel in) {
        imageList = in.createTypedArrayList(ImagesType.CREATOR);
    }

    public static final Creator<ImagesTypeList> CREATOR = new Creator<ImagesTypeList>() {
        @Override
        public ImagesTypeList createFromParcel(Parcel in) {
            return new ImagesTypeList(in);
        }

        @Override
        public ImagesTypeList[] newArray(int size) {
            return new ImagesTypeList[size];
        }
    };

    public List<ImagesType> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImagesType> imageList) {
        this.imageList = imageList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(imageList);
    }
}
