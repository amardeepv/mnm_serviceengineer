package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 16/03/2017.
 */

public class Register {
    private String engineerFirstName;
    private String engineerLastName;
    private String emailID;
    private String currentAddress;
    private int currentCountry;
    private int currentState;
    private int currentDistrict;
    private int currentCity;
    private String mobileNumber;
    private String agencyCode;
    private String password;
    private int skillID;

    public Register() {
    }

    public Register(String engineerFirstName,String engineerLastName, String emailID, String mobileNumber, String password,int skillID) {
        this.engineerFirstName = engineerFirstName;
        this.engineerLastName = engineerLastName;
        this.emailID = emailID;
        this.mobileNumber = mobileNumber;
        this.password = password;
        this.agencyCode="";
        this.currentAddress="";
        this.skillID=skillID;
    }
    /* "engineerLastName": "string",
            "emailID": "string",
            "currentAddress": "string",
            "currentCountry": 0,
            "currentState": 0,
            "currentDistrict": 0,
            "currentCity": 0,
            "mobileNumber": "string",
            "agencyCode": "string",
            "password": "string",
            "skillID": 0*/

    public String getEngineerFirstName() {
        return engineerFirstName;
    }

    public Register setEngineerFirstName(String engineerFirstName) {
        this.engineerFirstName = engineerFirstName;
        return this;
    }

    public String getEngineerLastName() {
        return engineerLastName;
    }

    public Register setEngineerLastName(String engineerLastName) {
        this.engineerLastName = engineerLastName;
        return this;
    }

    public String getEmailID() {
        return emailID;
    }

    public Register setEmailID(String emailID) {
        this.emailID = emailID;
        return this;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public Register setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
        return this;
    }

    public int getCurrentCountry() {
        return currentCountry;
    }

    public Register setCurrentCountry(int currentCountry) {
        this.currentCountry = currentCountry;
        return this;
    }

    public int getCurrentState() {
        return currentState;
    }

    public Register setCurrentState(int currentState) {
        this.currentState = currentState;
        return this;
    }

    public int getCurrentDistrict() {
        return currentDistrict;
    }

    public Register setCurrentDistrict(int currentDistrict) {
        this.currentDistrict = currentDistrict;
        return this;
    }

    public int getCurrentCity() {
        return currentCity;
    }

    public Register setCurrentCity(int currentCity) {
        this.currentCity = currentCity;
        return this;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public Register setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public Register setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Register setPassword(String password) {
        this.password = password;
        return this;
    }

    public int getSkillID() {
        return skillID;
    }

    public Register setSkillID(int skillID) {
        this.skillID = skillID;
        return this;
    }
}
