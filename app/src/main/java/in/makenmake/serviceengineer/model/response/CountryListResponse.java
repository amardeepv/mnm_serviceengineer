package in.makenmake.serviceengineer.model.response;


import in.bapps.model.CommonJsonResponse;

/**
 * Created by user on 26-09-16.
 */
public class CountryListResponse extends CommonJsonResponse {
    String id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}