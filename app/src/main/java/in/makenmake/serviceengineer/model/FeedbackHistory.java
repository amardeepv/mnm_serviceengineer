package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 05/06/2017.
 */

public class FeedbackHistory implements Parcelable{

/*
    "feedbackDate": "2017-06-01T17:05:45.207",
            "feebackMsg": "Good",
            "rating": 5,
            "feedbackBy": "Customer"
*/


    private String feedbackDate;
    private String feebackMsg;
    private int rating;
    private String feedbackBy;

    protected FeedbackHistory(Parcel in) {
        feedbackDate = in.readString();
        feebackMsg = in.readString();
        rating = in.readInt();
        feedbackBy = in.readString();
    }

    public static final Creator<FeedbackHistory> CREATOR = new Creator<FeedbackHistory>() {
        @Override
        public FeedbackHistory createFromParcel(Parcel in) {
            return new FeedbackHistory(in);
        }

        @Override
        public FeedbackHistory[] newArray(int size) {
            return new FeedbackHistory[size];
        }
    };

    public String getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeebackMsg() {
        return feebackMsg;
    }

    public void setFeebackMsg(String feebackMsg) {
        this.feebackMsg = feebackMsg;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getFeedbackBy() {
        return feedbackBy;
    }

    public void setFeedbackBy(String feedbackBy) {
        this.feedbackBy = feedbackBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(feedbackDate);
        dest.writeString(feebackMsg);
        dest.writeInt(rating);
        dest.writeString(feedbackBy);
    }
}
