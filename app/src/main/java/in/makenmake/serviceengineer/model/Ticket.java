package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 29/01/2017.
 */

public class Ticket implements Parcelable{
    private int ticketId;
    private String timeSlot;
    private String ticketOpenTime;
    private String Description;
  //  private TicketType ticketType;
    private String ticketStatus;
    private boolean isCheckedOut;
    private Service consumerServices;
    private Customer consumerDetails;
    private Plan consumerPlans;


/*    {
        "ticketId":2133,
            "ticketOpenTime":"2017-03-24T09:57:00.82",
            "ticketStatus":"Accepted",
            "ticketType":6,
            "timeSlot":"Morning",
            "engineerId":0,
            "consumerDetails":{
                 "userId":0,
                "customerName":"Abhishek ",
                "emailId":"abc@gmail.com",
                "mobileNumber":"1234556778",
                "consumerAddress":{
                    "cityId":100101,
                    "cityName":"Noida",
                    "stateId":80079,
                    "stateName":"Uttar Pradesh",
                    "address":"E-9 Sector 49 Noida 201301",
                    "areaId":20094,
                    "areaName":"Noida",
                    "houseNumber":"E-9",
                    "locality":"Sector 49 Noida",
                    "longitutde":77.049,
                    "latitude":28.4112
        }
    },
        "consumerPlans":{
                 "planId":null,
                "planName":null
             },
        "consumerServices":{
              "serviceId":425498266,
                "serviceType":"Installation",
                "serviceName":"Installation Light & Lamp fitting"
    }
    }*/

    public Ticket(int ticketId, String timeSlot, String ticketOpenTime, String ticketStatus, Service consumerServices, Customer consumerDetails, Plan consumerPlans) {
        this.ticketId = ticketId;
        this.timeSlot = timeSlot;
        this.ticketOpenTime = ticketOpenTime;
        this.ticketStatus = ticketStatus;
        this.consumerServices = consumerServices;
        this.consumerDetails = consumerDetails;
        this.consumerPlans = consumerPlans;
    }
    public Ticket(int ticketId, String timeSlot, String ticketOpenTime, String Description,String ticketStatus, Service consumerServices, Customer consumerDetails, Plan consumerPlans) {
        this.ticketId = ticketId;
        this.timeSlot = timeSlot;
        this.ticketOpenTime = ticketOpenTime;
        this.Description = Description;
        this.ticketStatus = ticketStatus;
        this.consumerServices = consumerServices;
        this.consumerDetails = consumerDetails;
        this.consumerPlans = consumerPlans;
    }

    public Ticket() {
    }

    protected Ticket(Parcel in) {
        ticketId = in.readInt();
        timeSlot = in.readString();
        ticketOpenTime = in.readString();
        Description = in.readString();
        ticketStatus = in.readString();
        consumerServices = in.readParcelable(Service.class.getClassLoader());
        consumerDetails = in.readParcelable(Customer.class.getClassLoader());
        consumerPlans = in.readParcelable(Plan.class.getClassLoader());
        isCheckedOut=in.readByte()!=0;
    }

    public static final Creator<Ticket> CREATOR = new Creator<Ticket>() {
        @Override
        public Ticket createFromParcel(Parcel in) {
            return new Ticket(in);
        }

        @Override
        public Ticket[] newArray(int size) {
            return new Ticket[size];
        }
    };

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTicketOpenTime() {
        return ticketOpenTime;
    }

    public void setTicketOpenTime(String ticketOpenTime) {
        this.ticketOpenTime = ticketOpenTime;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Service getConsumerServices() {
        return consumerServices;
    }

    public void setConsumerServices(Service consumerServices) {
        this.consumerServices = consumerServices;
    }

    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        isCheckedOut = checkedOut;
    }

    public Customer getConsumerDetails() {
        return consumerDetails;
    }

    public void setConsumerDetails(Customer consumerDetails) {
        this.consumerDetails = consumerDetails;
    }

    public Plan getConsumerPlans() {
        return consumerPlans;
    }

    public void setConsumerPlans(Plan consumerPlans) {
        this.consumerPlans = consumerPlans;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getDescription() {
        if(Description ==null || Description.equalsIgnoreCase("")){
            return "No Description";
        }
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ticketId);
        dest.writeString(timeSlot);
        dest.writeString(ticketOpenTime);
        dest.writeString(Description);
        dest.writeString(ticketStatus);
        dest.writeParcelable(consumerServices, flags);
        dest.writeParcelable(consumerDetails, flags);
        dest.writeParcelable(consumerPlans, flags);
        dest.writeByte((byte)(isCheckedOut?1:0));
    }
}



