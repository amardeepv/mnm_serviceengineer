package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 28/04/2017.
 */

public class SparePart {
    /*"sparePartName": "string",
            "sparePartAmount": 0,
            "sparePartsTypeId": 0*/

    private String sparePartName;
    private int sparePartAmount;
    private int sparePartsTypeId;

    public SparePart(String sparePartName, int sparePartAmount, int sparePartsTypeId) {
        this.sparePartName = sparePartName;
        this.sparePartAmount = sparePartAmount;
        this.sparePartsTypeId = sparePartsTypeId;
    }

    public SparePart() {
    }

    public String getSparePartName() {
        return sparePartName;
    }

    public void setSparePartName(String sparePartName) {
        this.sparePartName = sparePartName;
    }

    public int getSparePartAmount() {
        return sparePartAmount;
    }

    public void setSparePartAmount(int sparePartAmount) {
        this.sparePartAmount = sparePartAmount;
    }

    public int getSparePartsTypeId() {
        return sparePartsTypeId;
    }

    public void setSparePartsTypeId(int sparePartsTypeId) {
        this.sparePartsTypeId = sparePartsTypeId;
    }
}
