package in.makenmake.serviceengineer.model.response;

import java.util.List;

/**
 * Created by Bucky on 04/05/2017.
 */

public class EngineerInfoObjectRequest {
    private List<EngineerInfo> Engineer;

    public List<EngineerInfo> getEngineer() {
        return Engineer;
    }

    public void setEngineer(List<EngineerInfo> engineer) {
        Engineer = engineer;
    }
}
