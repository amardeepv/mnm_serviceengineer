package in.makenmake.serviceengineer.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 18/04/2017.
 */

public class ImageList implements Parcelable {

    private String imagePath;
    private String imageName;
    private String message;

    public ImageList() {
    }

    public ImageList(String imagePath, String imageName, String message) {
        this.imagePath = imagePath;
        this.imageName = imageName;
        this.message = message;
    }



    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ImageList> CREATOR = new Creator<ImageList>() {
        @Override
        public ImageList createFromParcel(Parcel in) {
            return new ImageList(in);
        }

        @Override
        public ImageList[] newArray(int size) {
            return new ImageList[size];
        }
    };

    protected ImageList(Parcel in) {
        imagePath = in.readString();
        imageName = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imagePath);
        dest.writeString(imageName);
        dest.writeString(message);
    }
}