package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 05/06/2017.
 */

public class TicketHistory implements Parcelable {
        /*"serviceDate": "2017-05-30T00:00:00",
                "checkIn": "05:00:30",
                "checkOut": "05:02:50",
                "nextShceduleDate": "1900-01-01T00:00:00"*/

    private String serviceDate;
    private String checkIn;
    private String checkOut;
    private String nextShceduleDate;

    protected TicketHistory(Parcel in) {
        serviceDate = in.readString();
        checkIn = in.readString();
        checkOut = in.readString();
        nextShceduleDate = in.readString();
    }

    public static final Creator<TicketHistory> CREATOR = new Creator<TicketHistory>() {
        @Override
        public TicketHistory createFromParcel(Parcel in) {
            return new TicketHistory(in);
        }

        @Override
        public TicketHistory[] newArray(int size) {
            return new TicketHistory[size];
        }
    };

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getNextShceduleDate() {
        return nextShceduleDate;
    }

    public void setNextShceduleDate(String nextShceduleDate) {
        this.nextShceduleDate = nextShceduleDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceDate);
        dest.writeString(checkIn);
        dest.writeString(checkOut);
        dest.writeString(nextShceduleDate);
    }
}
