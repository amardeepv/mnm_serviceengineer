package in.makenmake.serviceengineer.model.response;

import java.util.List;

import in.makenmake.serviceengineer.model.CompleteTicket;

/**
 * Created by Bucky on 03/05/2017.
 */

public class CompleteTicketListResponse {
    private List<CompleteTicket> ServiceDetaillist;

    public CompleteTicketListResponse(List<CompleteTicket> completedList) {
        ServiceDetaillist = completedList;
    }

    public CompleteTicketListResponse() {
    }

    public List<CompleteTicket> getCompletedList() {
        return ServiceDetaillist;
    }

    public void setCompletedList(List<CompleteTicket> completedList) {
        ServiceDetaillist = completedList;
    }
}
