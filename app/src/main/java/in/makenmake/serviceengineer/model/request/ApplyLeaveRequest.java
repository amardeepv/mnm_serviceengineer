package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 17/03/2017.
 */

public class ApplyLeaveRequest {
    /*{
        "engineerID": 7,
            "reason": "test",
            "leavestartdate": "2017-03-16",
            "leaveEndDate": "2017-03-17",
            "noofdays": 1,
            "available": "True",
            "leaveType": "F",
            "created": "2017-03-16",
            "weeklyOff": "",
            "status": 0,
            "type": "L",
            "halfDayType": "FH"
    }*/


    private int engineerID;
    private String reason;
    private String leavestartdate;
    private String leaveEndDate;
    private int noofdays;
    private String leaveType;
    private String created;
    private String weeklyOff;
    private int status;
    private String type;
    private String halfDayType;
    private String available;


    public ApplyLeaveRequest() {
    }

    public ApplyLeaveRequest(int engineerID, String reason, String leavestartdate, String leaveEndDate, int noofdays, String leaveType, String created, String weeklyOff, int status, String type, String halfDayType, String available) {
        this.engineerID = engineerID;
        this.reason = reason;
        this.leavestartdate = leavestartdate;
        this.leaveEndDate = leaveEndDate;
        this.noofdays = noofdays;
        this.leaveType = leaveType;
        this.created = created;
        this.weeklyOff = weeklyOff;
        this.status = status;
        this.type = type;
        this.halfDayType = halfDayType;
        this.available=available;
    }

    public int getEngineerID() {
        return engineerID;
    }

    public void setEngineerID(int engineerID) {
        this.engineerID = engineerID;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getLeavestartdate() {
        return leavestartdate;
    }

    public void setLeavestartdate(String leavestartdate) {
        this.leavestartdate = leavestartdate;
    }

    public String getLeaveEndDate() {
        return leaveEndDate;
    }

    public void setLeaveEndDate(String leaveEndDate) {
        this.leaveEndDate = leaveEndDate;
    }

    public int getNoofdays() {
        return noofdays;
    }

    public void setNoofdays(int noofdays) {
        this.noofdays = noofdays;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getWeeklyOff() {
        return weeklyOff;
    }

    public void setWeeklyOff(String weeklyOff) {
        this.weeklyOff = weeklyOff;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHalfDayType() {
        return halfDayType;
    }

    public void setHalfDayType(String halfDayType) {
        this.halfDayType = halfDayType;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
