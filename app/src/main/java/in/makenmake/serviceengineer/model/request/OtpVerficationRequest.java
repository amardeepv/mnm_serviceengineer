package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 25/03/2017.
 */

public class OtpVerficationRequest {

    public int otp;
    public String mobile;

    public OtpVerficationRequest(int otp, String mobile) {
        this.otp = otp;
        this.mobile = mobile;
    }

    public OtpVerficationRequest() {
    }
    /*{
        "otp": 0,
            "mobile": "string"
    }*/
}
