package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bucky on 29/01/2017.
 */

public class Service implements Parcelable{

    private String serviceName;
    private int serviceId;
    private String serviceType;
    private String serviceCategory;

    public Service(String serviceName, int serviceId, String serviceType,String serviceCategory) {
        this.serviceName = serviceName;
        this.serviceId = serviceId;
        this.serviceType = serviceType;
        this.serviceCategory=serviceCategory;
    }

    public Service() {
    }

    protected Service(Parcel in) {
        serviceName = in.readString();
        serviceId = in.readInt();
        serviceType = in.readString();
        serviceCategory=in.readString();
    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceName);
        dest.writeInt(serviceId);
        dest.writeString(serviceType);
        dest.writeString(serviceCategory);
    }
}
