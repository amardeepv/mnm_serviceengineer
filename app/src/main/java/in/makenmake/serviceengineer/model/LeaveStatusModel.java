package in.makenmake.serviceengineer.model;

/**
 * Created by Bucky on 03/06/2017.
 */

public class LeaveStatusModel {
    /*      "LeaveID": 3,
            "EngineerID": 520592,
            "Reason": "Test",
            "Leavestartdate": "2017-05-29T00:00:00",
            "LeaveEndDate": "2017-05-29T00:00:00",
            "noofdays": 1,
            "Available": "True",
            "leaveType": "F",
            "Created": null,
            "WeeklyOff": null,
            "Status": "Rejected",
            "Type": "L",
            "HalfLeaveType": "          ",
            "Name": "Rd Rd"*/



    private int LeaveID;
    private int EngineerID;
    private String Reason;
    private String Leavestartdate;
    private String LeaveEndDate;
    private int noofdays;
    private String Available;
    private String leaveType;
    private String Created;
    private String WeeklyOff;
    private String Status;
    private String Type;
    private String HalfLeaveType;
    private String Name;


    public int getLeaveID() {
        return LeaveID;
    }

    public void setLeaveID(int leaveID) {
        LeaveID = leaveID;
    }

    public int getEngineerID() {
        return EngineerID;
    }

    public void setEngineerID(int engineerID) {
        EngineerID = engineerID;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getLeavestartdate() {
        return Leavestartdate;
    }

    public void setLeavestartdate(String leavestartdate) {
        Leavestartdate = leavestartdate;
    }

    public String getLeaveEndDate() {
        return LeaveEndDate;
    }

    public void setLeaveEndDate(String leaveEndDate) {
        LeaveEndDate = leaveEndDate;
    }

    public int getNoofdays() {
        return noofdays;
    }

    public void setNoofdays(int noofdays) {
        this.noofdays = noofdays;
    }

    public String getAvailable() {
        return Available;
    }

    public void setAvailable(String available) {
        Available = available;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getWeeklyOff() {
        return WeeklyOff;
    }

    public void setWeeklyOff(String weeklyOff) {
        WeeklyOff = weeklyOff;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getHalfLeaveType() {
        return HalfLeaveType;
    }

    public void setHalfLeaveType(String halfLeaveType) {
        HalfLeaveType = halfLeaveType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
