package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 03/05/2017.
 */

public class TicketHistoryDetail {
    /*      "Name": "Tarsem ",
            "Status": "Accepted",
            "AssignedTo": "Jaskaran ",
            "ModifiedBy": "Jaskaran ",
            "Remark": "Gg",
            "Modified": "2017-04-27T11:32:57.54",
            "TktType": "Inspection & Quote"*/


    private String Name;
    private String Status;
    private String AssignedTo;
    private String ModifiedBy;
    private String Remark;
    private String Modified;
    private String TktType;


    public TicketHistoryDetail(String name, String status, String assignedTo, String modifiedBy, String remark, String modified, String tktType) {
        Name = name;
        Status = status;
        AssignedTo = assignedTo;
        ModifiedBy = modifiedBy;
        Remark = remark;
        Modified = modified;
        TktType = tktType;
    }

    public TicketHistoryDetail() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getAssignedTo() {
        return AssignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        AssignedTo = assignedTo;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public String getModified() {
        return Modified;
    }

    public void setModified(String modified) {
        Modified = modified;
    }

    public String getTktType() {
        return TktType;
    }

    public void setTktType(String tktType) {
        TktType = tktType;
    }
}
