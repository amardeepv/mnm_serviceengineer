package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 04/04/2017.
 */

public class EngineerLocation {

    private int engineerId;
    private double loc_Lan;
    private double loc_Lat;

            /*"engineerId":2002,
            "loc_Lan": 28.613054,
            "loc_Lat": 77.3609083*/

    public EngineerLocation(int engineerId, double loc_Lan, double loc_Lat) {
        this.engineerId = engineerId;
        this.loc_Lan = loc_Lan;
        this.loc_Lat = loc_Lat;
    }

    public int getEngineerId() {
        return engineerId;
    }

    public void setEngineerId(int engineerId) {
        this.engineerId = engineerId;
    }

    public double getLoc_Lan() {
        return loc_Lan;
    }

    public void setLoc_Lan(double loc_Lan) {
        this.loc_Lan = loc_Lan;
    }

    public double getLoc_Lat() {
        return loc_Lat;
    }

    public void setLoc_Lat(double loc_Lat) {
        this.loc_Lat = loc_Lat;
    }
}
