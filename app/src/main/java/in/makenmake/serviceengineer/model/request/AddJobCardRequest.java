package in.makenmake.serviceengineer.model.request;

import java.util.List;

import in.makenmake.serviceengineer.model.SparePartCartModel;

/**
 * Created by Bucky on 26/04/2017.
 */

public class AddJobCardRequest {
    /*      "tickeId": ticket,
            "engineerId": 3,
            "convenienceCharges": convience,
            "totalAmount": total,
            "advanceAmount": advance,
            "dueAmount": due,
            "amountRecived": recieved,
            "ticketAmount": tikcetamount,
            "convcyanceCharge": conveynace,
            "spareParts": ServiceCityRateList
            */

    private int tickeId;
    private int engineerId;
    private float convenienceCharges;
    private float totalAmount;
    private float dueAmount;
    private float amountRecived;
    private float ticketAmount;
    private float convcyanceCharge;
    private String ticketPaymentStatus;
    private String remarks;
    private List<SparePartCartModel> spareParts;


    public int getTickeId() {
        return tickeId;
    }

    public void setTickeId(int tickeId) {
        this.tickeId = tickeId;
    }

    public int getEngineerId() {
        return engineerId;
    }

    public void setEngineerId(int engineerId) {
        this.engineerId = engineerId;
    }

    public float getConvenienceCharges() {
        return convenienceCharges;
    }

    public void setConvenienceCharges(float convenienceCharges) {
        this.convenienceCharges = convenienceCharges;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public float getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(float dueAmount) {
        this.dueAmount = dueAmount;
    }

    public float getAmountRecived() {
        return amountRecived;
    }

    public void setAmountRecived(float amountRecived) {
        this.amountRecived = amountRecived;
    }

    public float getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(float ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public float getConvcyanceCharge() {
        return convcyanceCharge;
    }

    public void setConvcyanceCharge(float convcyanceCharge) {
        this.convcyanceCharge = convcyanceCharge;
    }

    public String getTicketPaymentStatus() {
        return ticketPaymentStatus;
    }

    public void setTicketPaymentStatus(String ticketPaymentStatus) {
        this.ticketPaymentStatus = ticketPaymentStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<SparePartCartModel> getSpareParts() {
        return spareParts;
    }

    public void setSpareParts(List<SparePartCartModel> spareParts) {
        this.spareParts = spareParts;
    }
}
