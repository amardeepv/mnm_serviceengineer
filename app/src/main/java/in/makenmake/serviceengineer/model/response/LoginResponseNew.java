package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 22/03/2017.
 */

public class LoginResponseNew {


    private int userId;
    private String name;
    private int accountStatus;
    private Access access;




    public class Access{
        public String userName;
        public String token;
        public String tokenType;
        public String expiresIn;
        public String issued;
        public String expires;



    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Access getAccess() {
        return access;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    /*  "userId": 1916,
            "name": null,
            "accountStatus": 3,
            "access": {
                "userName": "8889909192",
                "token": "uVSpsKz45MC5jYOqjxkA4vwuPKNzNr0QJsS-tKQmfoHbcnKb6sneVqGd8KnYg4y8EtNVIGuYzZ8AwUrH0WBt115PLdT5Vfz86KqptTgL703wg666YxwIPfJGLSkmgnuFh9kClBPkf_du0sEZErabZ1E4z-YGRpyAh4zZMAN5jMkwTQiwUKv3i9Ph43GgL1vwSAEwdkD8LrU2Thac-8dc9JmqMi9ow-XALT7DzJOzHukCIKBbjX3w731q9fvZi5zKoAlS_yoAXcHFtzWsVClm70ewf-VWvpGppFmtXoi2xIjJ8MKu8A_b_sDngOLkkagtJAaZm5lwqlK3FwrJbIl1ZioFhHIbakxcqGBOB8D5dDdA2lgJrL80KK9g_Quo3ZfTR2Dzj0MWvsMELogWSFdXrfVntR_-_oVRgQZ9uZq78JNnRQhnIUHxBDWgkQHNqeZ8TpWcRX7cu9Sy7G_mSz0_TQ",
                "tokenType": "bearer",
                "expiresIn": "1209599",
                "issued": "Wed, 22 Mar 2017 09:45:42 GMT",
                "expires": "Wed, 05 Apr 2017 09:45:42 GMT"
         }*/
}
