package in.makenmake.serviceengineer.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Bucky on 05/06/2017.
 */

public class TicketHistoryList implements Parcelable{
    private List<TicketHistory> ticketHistoryList;

    public TicketHistoryList(List<TicketHistory> ticketHistoryList) {
        this.ticketHistoryList = ticketHistoryList;
    }

    public TicketHistoryList() {
    }

    protected TicketHistoryList(Parcel in) {
        ticketHistoryList = in.createTypedArrayList(TicketHistory.CREATOR);
    }

    public static final Creator<TicketHistoryList> CREATOR = new Creator<TicketHistoryList>() {
        @Override
        public TicketHistoryList createFromParcel(Parcel in) {
            return new TicketHistoryList(in);
        }

        @Override
        public TicketHistoryList[] newArray(int size) {
            return new TicketHistoryList[size];
        }
    };

    public List<TicketHistory> getTicketHistoryList() {
        return ticketHistoryList;
    }

    public void setTicketHistoryList(List<TicketHistory> ticketHistoryList) {
        this.ticketHistoryList = ticketHistoryList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(ticketHistoryList);
    }
}
