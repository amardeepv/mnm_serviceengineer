package in.makenmake.serviceengineer.model;

/**
 * Created by mohitbaweja on 05/10/16.
 */

public class DashboardModel {


    private String title;
    private int count;
    private int thumbnail;

    public DashboardModel(String title, int count, int thumbnail) {
        this.title = title;
        this.count = count;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
