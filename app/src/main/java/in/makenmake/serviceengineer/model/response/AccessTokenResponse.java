package in.makenmake.serviceengineer.model.response;

import java.util.Date;

/**
 * Created by Bucky on 11/02/2017.
 */

public class AccessTokenResponse {

    public static final String ACCESS_TOKEN_KEY = "access_token";
    public static final String EXPIRES_IN_KEY = "expires_in";
    public static final String USER_ID_KEY = "user_id";

    private static final Date MAX_DATE = new Date(Long.MAX_VALUE);
    private static final Date DEFAULT_EXPIRATION_TIME = MAX_DATE;
    private static final Date DEFAULT_LAST_REFRESH_TIME = new Date();

    // Constants related to JSON serialization.
    private static final int CURRENT_JSON_FORMAT = 1;
    private static final String VERSION_KEY = "version";
    private static final String EXPIRES_AT_KEY = "expires_at";
    private static final String PERMISSIONS_KEY = "permissions";
    private static final String DECLINED_PERMISSIONS_KEY = "declined_permissions";
    private static final String TOKEN_KEY = "token";
    private static final String SOURCE_KEY = "source";
    private static final String LAST_REFRESH_KEY = "last_refresh";
    private static final String APPLICATION_ID_KEY = "application_id";

    private Date expires;
    private String access_token;
    private String applicationId;
    private String userId;









}
