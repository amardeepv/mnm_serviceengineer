package in.makenmake.serviceengineer.model.response;

import java.util.List;

import in.makenmake.serviceengineer.model.LeaveStatusModel;

/**
 * Created by Bucky on 03/06/2017.
 */

public class LeaveStatusResponse {
    private List<LeaveStatusModel> ServiceEngineerByID;

    public List<LeaveStatusModel> getServiceEngineerByID() {
        return ServiceEngineerByID;
    }

    public void setServiceEngineerByID(List<LeaveStatusModel> serviceEngineerByID) {
        ServiceEngineerByID = serviceEngineerByID;
    }
}
