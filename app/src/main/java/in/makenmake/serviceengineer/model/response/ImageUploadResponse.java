package in.makenmake.serviceengineer.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Bucky on 18/04/2017.
 */

public class ImageUploadResponse implements Parcelable{

    private List<ImageList> imageList;

    public ImageUploadResponse(List<ImageList> imageList) {
        this.imageList = imageList;
    }

    protected ImageUploadResponse(Parcel in) {
        //imageList = in.createTypedArrayList(in.makenmake.serviceengineer.model.response.ImageList.CREATOR);
        imageList = in.createTypedArrayList(ImageList.CREATOR);
    }

    public static final Creator<ImageUploadResponse> CREATOR = new Creator<ImageUploadResponse>() {
        @Override
        public ImageUploadResponse createFromParcel(Parcel in) {
            return new ImageUploadResponse(in);
        }

        @Override
        public ImageUploadResponse[] newArray(int size) {
            return new ImageUploadResponse[size];
        }
    };

    public List<ImageList> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageList> imageList) {
        this.imageList = imageList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(imageList);
    }
}

