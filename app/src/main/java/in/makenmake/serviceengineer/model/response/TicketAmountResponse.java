package in.makenmake.serviceengineer.model.response;

/**
 * Created by Bucky on 26/04/2017.
 */

public class TicketAmountResponse {
/*          "ticketId": 3053,
            "ticketamount": 230,
            "advancePaid": 0,
            "duePayment": null*/

    private int ticketId;
    private int ticketamount;
    private int advancePaid;
    private int duePayment;

    public TicketAmountResponse(int ticketId, int ticketamount, int advancePaid, int duePayment) {
        this.ticketId = ticketId;
        this.ticketamount = ticketamount;
        this.advancePaid = advancePaid;
        this.duePayment = duePayment;
    }

    public TicketAmountResponse() {
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketamount() {
        return ticketamount;
    }

    public void setTicketamount(int ticketamount) {
        this.ticketamount = ticketamount;
    }

    public int getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(int advancePaid) {
        this.advancePaid = advancePaid;
    }

    public int getDuePayment() {
        return duePayment;
    }

    public void setDuePayment(int duePayment) {
        this.duePayment = duePayment;
    }
}
