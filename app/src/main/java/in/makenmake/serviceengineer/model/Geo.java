package in.makenmake.serviceengineer.model;


/**
 * Created by Bucky on 29/01/2017.
 */

public class Geo {
    private  double Lat;
    private  double Lng;


    public Geo(double lat, double lng) {
        Lat = lat;
        Lng = lng;
    }

    public Geo() {
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        this.Lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        this.Lng = lng;
    }
}
