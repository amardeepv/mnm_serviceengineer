package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 13/04/2017.
 */

public class TicketStatusRequest {
    private int ticketId;
    private int status;

    public TicketStatusRequest(int ticketId, int status) {
        this.ticketId = ticketId;
        this.status = status;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
