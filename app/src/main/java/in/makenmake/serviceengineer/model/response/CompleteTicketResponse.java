package in.makenmake.serviceengineer.model.response;

import java.util.List;

import in.makenmake.serviceengineer.model.CompleteTicket;

/**
 * Created by Bucky on 05/06/2017.
 */

public class CompleteTicketResponse {
    private List<CompleteTicket> ServiceDetaillist;

    public List<CompleteTicket> getServiceDetaillist() {
        return ServiceDetaillist;
    }

    public void setServiceDetaillist(List<CompleteTicket> serviceDetaillist) {
        ServiceDetaillist = serviceDetaillist;
    }
}
