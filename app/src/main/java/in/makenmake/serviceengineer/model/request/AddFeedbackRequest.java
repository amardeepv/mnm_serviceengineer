package in.makenmake.serviceengineer.model.request;

import in.makenmake.serviceengineer.utils.EngineerPreference;

/**
 * Created by Bucky on 05/05/2017.
 */

public class AddFeedbackRequest {
    /*        "feedBackID": 0,
            "userID": 0,
            "ticketID": 0,
            "feedbackMsg": "string",
            "rating": 0
    */

    private int feedBackID;
    private int userID;
    private int ticketID;
    private String feedbackMsg;
    private int rating;

    public AddFeedbackRequest(int rating, String feedbackMsg, int ticketID) {
        this.rating = rating;
        this.feedbackMsg = feedbackMsg;
        this.ticketID = ticketID;
        this.userID= EngineerPreference.getInstance().getUserId();
        this.feedBackID=0;
    }

    public int getFeedBackID() {
        return feedBackID;
    }

    public void setFeedBackID(int feedBackID) {
        this.feedBackID = feedBackID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public String getFeedbackMsg() {
        return feedbackMsg;
    }

    public void setFeedbackMsg(String feedbackMsg) {
        this.feedbackMsg = feedbackMsg;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
