package in.makenmake.serviceengineer.model;

import java.util.List;

/**
 * Created by Bucky on 05/06/2017.
 */

public class CompleteTicket {
    /*      "ServiceName": "DVR & Camera",
            "ServiceParentName": "CCTV",
            "Amount": 200,
            "ServiceDescription": "Includes cleaning of camera and DVR ",
            "Created": "2017-05-30T13:00:45.62",
            "OrderID": 232878,
            "TicketID": 496774,
            "Expirationdate": "2018-05-29T12:08:43.56",
            "EngineerName": "Rd Rd",
            "ticketDescription": "",
            "ticketStatus": "Completed",
            "paymentStatus": "Received",
            "engineerImagePath": "",
            "rating": null,*/

    private String ServiceName;
    private String ServiceParentName;
    private Float Amount;
    private String ServiceDescription;
    private String Created;
    private int OrderID;
    private int TicketID;
    private String Expirationdate;
    private String EngineerName;
    private String ticketDescription;
    private String ticketStatus;
    private String paymentStatus;
    private String engineerImagePath;
    private int rating;

    private List<TicketHistory> ticketHistoryList;
    private List<FeedbackHistory> feedbackHistoryList;
    private List<SparePartHistory> SparePartsList;


    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceParentName() {
        return ServiceParentName;
    }

    public void setServiceParentName(String serviceParentName) {
        ServiceParentName = serviceParentName;
    }

    public Float getAmount() {
        return Amount;
    }

    public void setAmount(Float amount) {
        Amount = amount;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        ServiceDescription = serviceDescription;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getTicketID() {
        return TicketID;
    }

    public void setTicketID(int ticketID) {
        TicketID = ticketID;
    }

    public String getExpirationdate() {
        return Expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        Expirationdate = expirationdate;
    }

    public String getEngineerName() {
        return EngineerName;
    }

    public void setEngineerName(String engineerName) {
        EngineerName = engineerName;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getEngineerImagePath() {
        return engineerImagePath;
    }

    public void setEngineerImagePath(String engineerImagePath) {
        this.engineerImagePath = engineerImagePath;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<TicketHistory> getTicketHistoryList() {
        return ticketHistoryList;
    }

    public void setTicketHistoryList(List<TicketHistory> ticketHistoryList) {
        this.ticketHistoryList = ticketHistoryList;
    }

    public List<FeedbackHistory> getFeedbackHistoryList() {
        return feedbackHistoryList;
    }

    public void setFeedbackHistoryList(List<FeedbackHistory> feedbackHistoryList) {
        this.feedbackHistoryList = feedbackHistoryList;
    }

    public List<SparePartHistory> getSparePartsList() {
        return SparePartsList;
    }

    public void setSparePartsList(List<SparePartHistory> sparePartsList) {
        SparePartsList = sparePartsList;
    }
}
