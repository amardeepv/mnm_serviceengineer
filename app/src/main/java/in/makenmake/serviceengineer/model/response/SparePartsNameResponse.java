package in.makenmake.serviceengineer.model.response;

import java.util.List;

/**
 * Created by Bucky on 18/05/2017.
 */

public class SparePartsNameResponse {

    private List<SparePartsNameModel> SparePartsStockslist;

    public List<SparePartsNameModel> getSpareParts() {
        return SparePartsStockslist;
    }

    public void setSpareParts(List<SparePartsNameModel> spareParts) {
        SparePartsStockslist = spareParts;
    }

}
