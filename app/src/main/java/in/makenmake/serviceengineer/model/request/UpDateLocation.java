package in.makenmake.serviceengineer.model.request;

/**
 * Created by Bucky on 17/03/2017.
 */

public class UpDateLocation {
    /*"engineerId":3,
            "loc_Lan": 6565.787,
            "loc_Lat": 65223.787*/

    public int engineerId;
    public double loc_Lan;
    public double loc_Lat;

    public UpDateLocation() {
    }

    public UpDateLocation(int engineerId, double loc_Lan, double loc_Lat) {
        this.engineerId = engineerId;
        this.loc_Lan = loc_Lan;
        this.loc_Lat = loc_Lat;
    }
}
