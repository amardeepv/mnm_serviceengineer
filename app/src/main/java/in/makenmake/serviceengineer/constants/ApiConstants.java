package in.makenmake.serviceengineer.constants;

public interface ApiConstants {


	public interface ACTION {
		public static String MAIN_ACTION = "in.makenmake.engineer.action.main";
		public static String INIT_ACTION = "in.makenmake.engineer.action.init";
		public static String PREV_ACTION = "in.makenmake.engineer.action.prev";
		public static String PLAY_ACTION = "in.makenmake.engineer.action.play";
		public static String NEXT_ACTION = "in.makenmake.engineer.action.next";
		public static String STARTFOREGROUND_ACTION = "in.makenmake.engineer.action.startforeground";
		public static String STOPFOREGROUND_ACTION = "in.makenmake.engineer.action.stopforeground";
	}


	interface PARAMS {
		//Login View
		String	USERNAME				= "username";
		String	PASSWORD				= "password";
		String	GRANT_TYPE				= "grant_type";

		//Register view
		String	FIRSTNAME			    = "engineerFirstName";
		String	LASTNAME			    = "engineerLastName";
		String	EMAILID	    			= "emailID";
		String	MOBILE	        		= "mobileNumber";
		String	currentAddress        	= "currentAddress";
		String	currentCountry        	= "currentCountry";
		String	currentState        	= "currentState";
		String	currentDistrict        	= "currentDistrict";
		String	currentCity        		= "currentCity";
		String	agencyCode        		= "agencyCode";
		String	password        		= "password";
		String	skillID        		= "skillID";




		String 	KEY_CATEGORY 			= "Category";
		String 	KEY_PLAN				= "Plan";
		String 	USER_ID				    = "userId";
		String	OLDPASS				    = "oldPass";


		String 	USERID					= "UserId";

    }

	interface MessageConstant{
		String LOGIN_SUCCESS										= "ACT17";
		String LOGIN_SERVER_MiGRATION								= "ACT20";
		String LOGIN_ERROR_DOCUMENT									= "ACT15";
		String LOGIN_ERROR_NON_VERIFIED_ACCOUNT_					= "ACT12";
		String LOGIN_ERROR_INACTIVATED								= "ACT21";
	}



	final int		REQUEST_LOGIN					= 1;
	final int		REQUEST_CHECK_NUMBER_REGISTER	= 2;
	final int		REQUEST_REGISTER				= 3;
	final int		REQUEST_OTP_VERFICATION			= 4;
	final int 		REQUEST_FORGOT_PASSWORD			= 5;
	final int 		REQUEST_TROUBLE_LOGIN			= 6;
	final int 		REQUEST_SKILL_LIST				= 7;

	//Main Activity
	final int 		REQUEST_MARK_ATTENDANCE			= 8;
	final int 		REQUEST_APPLY_LEAVE				= 9;
	final int 		REQUEST_UPDATE_AVAILABILITY		= 10;
	final int 		REQUEST_FORGOT_PASSWORD_VERIFY	= 11;
	final int 		REQUEST_CHANGE_PASSWORD			= 12;
	final int 		REQUEST_LOGOUT					= 13;
	final int 		REQUEST_GET_TICKET				= 14;
	final int 		REQUEST_UPDATE_ENG_LOC			= 15;
	final int 		REQUEST_UPDATE_TICKET_STATUS	= 16;
	final int 		REQUEST_CHECK_IN				= 17;
	final int 		REQUEST_CHECK_OUT				= 18;
	final int 		REQUEST_JOB_CARD				= 19;
	final int 		REQUEST_TICKET_HISTORY_BY_ID	= 20;
	final int 		REQUEST_CHECK_IN_OTP_VERF		= 21;
	final int 		REQUEST_ENGINEER_SIGNOUT		= 22;
	final int 		REQUEST_GENERATE_OTP			= 23;
	final int 		REQUEST_SPAREPART_TYPE			= 24;
	final int 		REQUEST_SPAREPART_NAME			= 25;
	final int 		REQUEST_ADD_JOB_CARD			= 26;
	final int 		REQUEST_TICKET_COMPLETE			= 27;
	final int 		REQUEST_ENG_INFO				= 28;
	final int 		REQUEST_EX_ENG_AVBL				= 29;
	final int 		REQUEST_ADD_FEEDBACK			= 30;
	final int 		REQUEST_LEAVES_STATUS			= 31;
	final int 		REQUEST_GET_COMPLETED_TICKET	= 32;


	/*final int 		REQUEST_FEEDBACK_TICKET			=15;



	final int 		REQUEST_TICKET_HISTORY			=19;


	final int 		REQUEST_WALLET_AMOUNT    	    =27;
	final int 		REQUEST_WALLET_HISTORY   	    =28;
	final int 		REQUEST_TICKET_DETAILS   	    =29;

	final int		REQUEST_STATE_LIST				=30;
	final int		REQUEST_DISTRICT_LIST			=31;
	final int		REQUEST_CITY_LIST				=32;
	final int       REQUEST_USER_INFO				=33;
	final int 		REQUEST_UPDATE_USER_INFO		=34; */






/*	final String	URL_BASE					= "http://services.makenmake.in/mnmapiv4/api/";//dev
	final String	URL_LOGIN					= URL_BASE + "login";*/

//	final String	URL_BASE					= "http://103.25.131.238/mnmapi/";//dev
//	final String	URL_BASE					= "https://www.makenmake.in/mnmapi/";//dev
	final String	URL_BASE					= "https://makenmake.in/mnmapi/";//dev

	final String	NAMESPACE_API				="api/";
	final String	NAMESPACE_ENGINEER			="engineer/";
	final String	NAMESPACE_SKILL				="skills/";
	final String	NAMESPACE_ACCOUNT			="account/";
	final String	NAMESPACE_STATUS			="EngineerStatus/";

	//LOGIN
	final String	URL_LOGIN					= URL_BASE +	NAMESPACE_ACCOUNT +	 		 "login";

	//Registration

	final String	URL_REGISTER				= URL_BASE +	NAMESPACE_ENGINEER +	 "EngineerRegister";
	final String	URL_CHECK_NUMBER_REGISTER	= URL_BASE +	NAMESPACE_API +			 "registerUser?mobileNumber=";
	final String	URL_OTP_VERIFICATION		= URL_BASE +	NAMESPACE_ACCOUNT +		 "VerifyMobile";
	final String    ACCOUNT_URL 				= URL_BASE + 	NAMESPACE_API +			 "getUserInfo?UserId=";

	final String    URL_CHANGE_PASSWORD	        = URL_BASE +	NAMESPACE_ACCOUNT +		 "ChangePassword";
	final String    URL_VERIFY_OTP      	    = URL_BASE + 	NAMESPACE_API +			 "verifyUnverifiedOverOtp?emailId=";

	final String	URL_USER_INFO				= URL_BASE + 	NAMESPACE_API +			 "getUserInfo?UserId=";
	final String    URL_UPDATE_USER_INFO		= URL_BASE + 	NAMESPACE_API +			 "updateUserInfo";
	final String    URL_TROUBLE_LOGIN			= URL_BASE +	NAMESPACE_API +			 "TroubleInLogin?emailId=";
	final String    URL_FORGOT_PASSWORD_MOBILE	= URL_BASE +	NAMESPACE_ACCOUNT +		 "ForgotPasswordOTP?mobile=";
	final String    URL_FORGOT_PASSWORD_OTP		= URL_BASE +	NAMESPACE_ACCOUNT +		 "ForgotPasswordOTPConfrim?mobile=";

	final String    URL_GET_SKILL_LIST			= URL_BASE +	NAMESPACE_SKILL +		 "SkillList";
	final String    URL_APPLY_LEAVE				= URL_BASE +	 						 "AddServiceEngineerLeave";  //TODO : ask for nameSpace
	final String    URL_MARK_ATTENDANCE			= URL_BASE +	 						 "AddEngineerAttendance";  //TODO : ask for nameSpace

	final String    URL_LOGOUT					= URL_BASE +	NAMESPACE_SKILL +		 "SkillList";
	final String    URL_GET_TICKET_LIST			= URL_BASE +							 "GetAllTicketByEngineerId?engineerId=";
	final String    URL_ADD_ENG_LOCATION		= URL_BASE +							 "AddCurrentEngineerLocation";
	final String    URL_UPDATE_TICKET_STATUS	= URL_BASE +							 "UpdateTicketStatus?ticketId=";
	final String    URL_CHECKIN_CHECKOUT		= URL_BASE +	NAMESPACE_ENGINEER +	 "CheckInCheckOut";
	final String    URL_ENGINEER_SIGNOUT		= URL_BASE +	NAMESPACE_ENGINEER +	 "EngineerSignOut";
	final String    URL_GENERATE_OTP			= URL_BASE +	NAMESPACE_ENGINEER +	 "GetCheckinOTP?ticketId=";
	final String    URL_VERIFY_CHECKIN_OTP		= URL_BASE +	NAMESPACE_ENGINEER +	 "VerifyCheckinOTP?ticketId=";
	final String    URL_GET_TICKET_AMOUNT		= URL_BASE +	NAMESPACE_ENGINEER +	 "GetTicketAmount?ticketId=";
	final String    URL_GET_JOB_CARD_AMOUNT		= URL_BASE +	NAMESPACE_ENGINEER +	 "GetJobCardDetails?ticketId=";
	final String    URL_GET_SPAREPART_TYPE		= URL_BASE +	NAMESPACE_ENGINEER +	 "GetSpareTypes";
	final String    URL_GET_SPAREPART_NAME		= URL_BASE +	NAMESPACE_ENGINEER +	 "GetSparepartsByType?TypeId=";  //"GetSparePartsName?SpareTypeId=";
	final String    URL_ADD_JOB_CARD			= URL_BASE +	NAMESPACE_ENGINEER +	 "AddJobCard";
	final String    URL_GET_COMPLETE_TICKET		= URL_BASE +	 						 "GetCompletedTickets?engineerId=";
	final String    URL_GET_TICKET_HISTORY		= URL_BASE +	 						 "GetTicketHistory?ticketId=";
	final String    URL_ENG_INFO				= URL_BASE +	 						 "EngineerByID?UserId=";
	final String    URL_EX_ENG_AVBL				= URL_BASE +	 NAMESPACE_STATUS +		 "UpdateEngineerStatus?UserId=";
	final String    URL_ADD_FEEDBACK			= URL_BASE +	 						 "AddFeedback";
	final String    URL_LEAVES_STATUS			= URL_BASE +	 						 "GetEngineerByEngineerID?EngineerID=";
	final String    URL_GET_COMPLETED_TICKET	= URL_BASE +	 NAMESPACE_ENGINEER	+	 "GetEngineerCompletedTickets?EngineerId=";
	final String    URL_ServiceTimeImageUpload	= URL_BASE +	           				 "ServiceTimeImageUpload";






















}
