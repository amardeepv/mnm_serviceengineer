package in.makenmake.serviceengineer.constants.enums;

/**
 * Created by Bucky on 03/06/2017.
 */

public class LeaveStatusString {
    public static final String ACCEPTED="Approved";
    public static final String REJECTED="Rejected";
    public static final String PENDING="Pending";
}
