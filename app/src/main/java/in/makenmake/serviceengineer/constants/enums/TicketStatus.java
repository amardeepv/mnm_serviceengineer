package in.makenmake.serviceengineer.constants.enums;

/**
 * Created by r.yadav on 09/01/2017.
 */

public final class TicketStatus {
   public static final int created=0;
   public static final int assigned=1;
   public static final int awaiting_engineer_confirmation=2;
   public static final int esclated=3;
   public static final int accepted=4;
   public static final int in_progress=5;
   public static final int completed=6;
   public static final int re_open=7;
   public static final int reject=8;





    /*--By Default Ticket Will be--
            --Created-0--
            --Assigned-1--
            --Awaiting Engineer Confirmation-2-
            --Esclated-3
            --Accepted-4--
            --In Progress-5--
            --Completed-6-
            --Re-open--7*/
}
