package in.makenmake.serviceengineer.constants.enums;

/**
 * Created by Bucky on 29/01/2017.
 */

public enum TicketType {
    Inspection,
    Repair
}
