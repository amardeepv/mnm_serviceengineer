package in.makenmake.serviceengineer.constants;

public interface AppConstants {

	public int					REQUEST_CODE_CAMERA					= 10001;
	public int					REQUEST_PICK_IMAGE					= 10002;

	public static final String	NIGHT_ADVISOR_PREFS					= "make_n_make";

	public String 				appLang_eng		 					= "en";
	public String 				appLang_hindi						= "hi";

	public String				EXTRA_LOOKS							= "extra_looks";

	public String				BUNDLE_USER							= "BUNDLE_USER";

	public String				LOCAL_BROADCAST_DELETE_EXPIRE_LOOK	= "local_broadcast_delete_expire_look";


	 interface LocalAttendanceType {
		String inAttendance="inAttendance";
		String outAttendance="outAttendance";
	}

}
