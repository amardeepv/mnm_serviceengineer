package in.makenmake.serviceengineer.constants.enums;

/**
 * Created by Bucky on 19/04/2017.
 */

public class TicketStatusString {
    public static final String created="created";
    public static final String assigned="assigned";
    public static final String awaiting_engineer_confirmation="Awaiting engineer confirmation";
    public static final String esclated="esclated";
    public static final String accepted="Accepted";
    public static final String in_progress="In Progress";
    public static final String completed="completed";
    public static final String re_open="re_open";
    public static final String reject="reject";

}
